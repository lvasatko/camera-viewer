#ifndef MODEDIALOG_H
#define MODEDIALOG_H

#include <QDialog>
#include <cuda_runtime.h>
namespace Ui {
class ModeDialog;
}

class ModeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ModeDialog(QWidget *parent = 0);
    ~ModeDialog();
    int exec();

    int getCudeDeviceNumber();
    QSize frameResolution();


private slots:
    void on_imageDebayerBtn_clicked();
    void on_dataDebayerBtn_clicked();

private:
    Ui::ModeDialog *ui;
    QString printDevProp(cudaDeviceProp devProp);
};

#endif // MODEDIALOG_H
