#-------------------------------------------------
#
# Project created by QtCreator 2015-03-20T15:45:23
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


LIBS += -lgpujpeg

TARGET = Viewer
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        viewerwindow.cpp \
    videoframe.cpp \
    debayer.cpp \
    cameradebayerwidget.cpp \
    modedialog.cpp \
    worker.cpp \
    glwidget.cpp \
    demoframegrabber.cpp \
    convertertobayer.cpp \
    include/Cuda/cuda_dfpd.cu \
    include/Cuda/cuda_code.cu \
    include/Cuda/cuda_hqli.cu \
    include/Methods/dfpd.cpp \
    include/Methods/dummy.cpp \
    include/Methods/hqli.cpp \
    include/Methods/bilinear.cpp \
    include/Methods/ahda_edge.cpp \
    include/Methods/ahda.cpp



SOURCES -=  include/Cuda/cuda_ahd_common.cu \
            include/Cuda/cuda_adha.cu \
            include/Cuda/cuda_hqli.cu \
            include/Cuda/cuda_code.cu \
            include/Cuda/cuda_dfpd.cu

HEADERS  += viewerwindow.h \
    videoframe.h \
    debayer.h \
    cameradebayerwidget.h \
    modedialog.h \
    worker.h \
    glwidget.h \
    demoframegrabber.h \
    convertertobayer.h \
    include/Methods/dfpd.h \
    include/Methods/dummy.h \
    include/Methods/hqli.h \
    include/Methods/bilinear.h \
    include/Methods/debayermethod.h \
    include/Cuda/cuda_code.cuh \
    include/Methods/ahda_edge.h \
    include/Methods/ahda.h

FORMS    += viewerwindow.ui \
    cameradebayerwidget.ui \
    modedialog.ui

CUDA_SOURCES += include/Cuda/cuda_hqli.cu
CUDA_SOURCES += include/Cuda/cuda_code.cu
CUDA_SOURCES += include/Cuda/cuda_dfpd.cu
CUDA_SOURCES += include/Cuda/cuda_ahda.cu


# c.c 5.0 (geforce GTX 960M)
# c.c 5.2 (geforce GTX 970)
CUDA_ARCH     = sm_50

NVCCFLAGS     = --compiler-options -use_fast_math --ptxas-options=-v #-fno-strict-aliasing
PROJECT_DIR = $$system(pwd)
OBJECTS_DIR = $$PROJECT_DIR/Obj

CUDA_INC = $$join(INCLUDEPATH,' -I','-I',' ')

cuda.commands = /usr/local/cuda-7.5/bin/nvcc -m64 -O3 -arch=$$CUDA_ARCH -c $$NVCCFLAGS \
                $$CUDA_INC $$LIBS  ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT} \
                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2

cuda.dependency_type = TYPE_C
cuda.depend_command = /usr/local/cuda-7.5/bin/nvcc -O3 -M $$CUDA_INC $$NVCCFLAGS   ${QMAKE_FILE_NAME}

cuda.input = CUDA_SOURCES
cuda.output = ${OBJECTS_DIR}${QMAKE_FILE_BASE}_cuda.o

QMAKE_EXTRA_COMPILERS += cuda

LIBS +=  -L/usr/local/cuda/lib64 -L/usr/local/cuda/lib64/ -lcudart -lcuda -ltiff

INCLUDEPATH += /usr/local/cuda/include
DEPENDPATH += /usr/local/cuda/include

DISTFILES += \
    include/Cuda/cuda_ahda.cu






