#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLTexture>
#include "videoframe.h"

class GLWidget : public QOpenGLWidget
{
public:
    GLWidget(QWidget * parent);
    ~GLWidget();

    void paintGL();

    void resizeGL();

    void initializeGL();

public slots:

    void newFrame(VideoFrame * newFrame);


private:
    int counter{0};
    QOpenGLTexture  * m_texture;
    GLuint texid;
    VideoFrame * m_frame;
};

#endif // GLWIDGET_H
