#include "worker.h"
#include "debayer.h"
#include <iostream>

Worker::Worker(Debayer *debayerManager, DebayerMethod *method, int cudaDevice) : QObject(0),
    m_debayerManager(debayerManager),m_cudaDevice(cudaDevice),m_method(method)

{
    m_workerThread = new QThread();
    m_workerThread->start();
    this->moveToThread(m_workerThread);

    setProcessing(true);
}

Worker::~Worker()
{
    m_workerThread->quit();
    if(!m_workerThread->wait(2000))
        m_workerThread->terminate();

    delete m_workerThread;

    m_method->destroy();
    delete m_method;
}

bool Worker::processing()
{
    m_processingMutex.lock();
    bool value = m_processing;

    // if not initialized -> no processing
    if(!m_initialized) {
        value = m_initialized;
    }

    m_processingMutex.unlock();
    return value;
}

void Worker::setProcessing(bool processing)
{
    m_processingMutex.lock();
    m_processing = processing;
    m_processingMutex.unlock();
}

void Worker::processFrame(VideoFrame *frame)
{
    if(m_method == nullptr) {
        delete frame;
        return;
    }

    setProcessing(true);
    do {
        // call virtual method - blocking this thread
        m_method->processFrame(frame);

        // send frame to storage
        emit storeProcessedFrame(frame);

        // take new frame to process
        frame = m_debayerManager->dequeueFrame();
    }
    while(frame != nullptr && !stopWorking());

    setProcessing(false);
}

void Worker::init(uint width, uint height)
{
    cudaError_t status;
    if((status = cudaSetDevice(m_cudaDevice)) != cudaSuccess) {
        std::cerr << "[ERROR]\t Worker::init() - cudaSetDevice: " <<  cudaGetErrorString(status) << std::endl;
        return;
    }

    if(!m_method->init(width, height)){
        std::cerr << "[ERROR]\t Worker::init() - m_method->init failed" << std::endl;
        return;
    }

    m_initialized = true;

    setProcessing(false);
}

bool Worker::stopWorking()
{
    m_stopMutex.lock();
    bool value = m_stopping;
    m_stopMutex.unlock();
    return value;
}

void Worker::stop()
{
    m_stopMutex.lock();
    m_stopping = true;
    m_stopMutex.unlock();
}
