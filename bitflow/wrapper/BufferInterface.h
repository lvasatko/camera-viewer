 #pragma once
 #include "BiApi.h"
 
 class BFDLL BufferInterface
 {
 public:
         BufferInterface(void);
         virtual ~BufferInterface(void);
 
         // Opens board for acquisition.
         void open(BFU32 brdNumber);
         
         // Closes board resources.
         void close(); 
         
         Bd getBoardHandle() { return m_hBoard; }
 
         // Returns a pointer to the array of pointers to the buffers.
         PBFU32* getBufferArrayPointers(); 
         
         // Returns information about the board and camera file.
         BFU32 getBrdInfo(BFU32 brdInqVar); 
         
         // Returns the number of frames that have been captured.
         BFU32 getNumFramesCaptured();
         
         // Returns the number of frames that where missed during acquisition.
         BFU32 getNumFramesMissed();
         
         // If returns TRUE <=> a start command has been issued.
         BFBOOL getStartAcqFlag();
         
         // If returns TRUE <=> a stop command has been issued.
         BFBOOL getStopAcqFlag();
         
         // If returns TRUE <=> an abort command has been issued.
         BFBOOL getAbortAcqFlag();
         
         // If returns TRUE <=> a pause command has been issued.
         BFBOOL getPauseAcqFlag();
         
         // If returns TRUE <=> instance cleanup has occured.
         BFBOOL getCleanupAcqFlag();
 
         // Returns the major version of the Bi API.
         BFU32 getMajorVersion();
         
         // Returns the minor version of the Bi API.
         BFU32 getMinorVersion();
 
         // Sets the hardware trigger mode
         void setTriggerMode(BFU32 triggerMode, BFU32 trigPolarity); 
         
         // Returns the current hardware trigger mode of the board.
         BFU32 getTriggerMode(); 
         
         // Returns the current hardware trigger polarity.
         BFU32 getTriggerPolarity(); 
         
         // Issues a software trigger.
         void issueSoftwareTrigger(BFU32 trigMode); 
         
         // Displays dialog describing error
         BFU32 showError(BFU32 errorNum); 
 
         // Write a single buffer to disk.
         void writeBuffer(PBFCHAR fileName, BFSIZET fileNameSize, BFU32 bufferNumber, BFU32 options);
 
         // Write a sequence of buffers to disk. 
         void writeSeqBuffer(PBFCHAR fileName, BFSIZET fileNameSize, BFU32 firstBufNumber, BFU32 numBufs, 
                 BFU32 Options);
 
         // read a sequence of files into memory.
         void readSeqFile(PBFCHAR fileName, BFU32 firstBufNumber, BFU32 numBufs); 
 
         // Clears all buffers memory
         void clearBuffers();
 
         // Returns the error test for a specific error number
         void getErrorText(BFU32 errorNumber, PBFCHAR errorText, PBFU32 errorTextSize);
 
         // Returns the number of buffers software is behind the hardware DMA engine.
         BFU32 getBufferQueueSize();
         
         // Set Buffer acquisition timeout.
         void setTimeout( BFU32 TimeoutValue );
 
 protected:
         Bd m_hBoard;
         
         BIBA m_BufferArray;     
 
         BFBOOL isMemAssigned;
         BFBOOL isMemAllocated;
         BFBOOL isSetup;
 
 };
 
 
 class BFDLL BFException
 {
 public:
         BFException(){errorNum = 0;}
         BFException(BFU32 error) {errorNum = error;}
         ~BFException(){};
 
         BFU32 showErrorMsg() { return(BiErrorShow(0, errorNum)); }
 
         // Returns an text based message of an error that has occured.
         BFU32 getErrorText(PBFCHAR ErrorText, PBFU32 ErrorTextSize);
 
         BFU32 getErrorNumber() {return errorNum;}
 
 private:
         BFU32 errorNum;
 
 };
