#include "convertertobayer.h"

extern "C"
void cuda_convertToBayer_init(unsigned char ** device_outputData,cudaArray ** inputTexArray,int width, int height,cudaStream_t * stream);
extern "C"
void cuda_convertToBayer(unsigned char * inputData,cudaArray * inputTexArray,unsigned char * device_outputData,unsigned char * host_outputData,int width, int height,cudaStream_t stream);
extern "C"
void cuda_convertToBayer_destroy(cudaArray * inputTexArray,unsigned char * device_outputData,cudaStream_t stream);


ConverterToBayer::ConverterToBayer(int width, int height, QObject *parent) : QObject(parent), m_width(width),m_height(height)
{

}

ConverterToBayer::~ConverterToBayer()
{
    cuda_convertToBayer_destroy(m_inputTexArray,m_device_outputData,m_stream);
}

void ConverterToBayer::init()
{
    cuda_convertToBayer_init(&m_device_outputData,&m_inputTexArray,m_width,m_height,&m_stream);
}

void ConverterToBayer::convertFrameToGRBG(unsigned char *input, unsigned char *output)
{
    cuda_convertToBayer(input,m_inputTexArray,m_device_outputData,output,m_width, m_height,m_stream);
}

