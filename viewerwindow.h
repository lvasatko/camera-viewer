#ifndef VIEWERWINDOW_H
#define VIEWERWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QImage>
#include <QMutex>

#include "modedialog.h"
#include "cameradebayerwidget.h"


namespace Ui {
class ViewerWindow;
}

class ViewerWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewerWindow(QWidget *parent = 0);

    bool parseArguments(int argc, char *argv[]);

    void createWidgets();
    void showDialog();

    ~ViewerWindow();

private:
    void printHelp();
    Ui::ViewerWindow *ui;
    CameraDebayerWidget * cameraWidget {nullptr};
};

#endif // VIEWERWINDOW_H
