#include <QApplication>
#include <QDebug>
#include <QByteArray>

#include "viewerwindow.h"
#include "lenscontroller.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ViewerWindow w;

    // run in GUI mode
    w.createWidgets();
    w.showMaximized();
    w.showDialog();

    return a.exec();
}
