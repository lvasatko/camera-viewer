#ifndef CAMERADEBAYERWIDGET_H
#define CAMERADEBAYERWIDGET_H

#include <QWidget>

#include "demoframegrabber.h"
#include "debayer.h"

namespace Ui {
class CameraDebayerWidget;
}

class CameraDebayerWidget : public QWidget
{
    Q_OBJECT

public:
    enum InputType{
        IMAGE = 0,
        MJPEG
    };


    explicit CameraDebayerWidget(QSize frameSize,QWidget *parent = 0);
    ~CameraDebayerWidget();
    void init();

public slots:

    void newDebayeredFrame(VideoFrame *frame);
    void showImage();

    void setCudaDevice(int cudaDevice);

private slots:

    void newFrameToDebayer(VideoFrame * frame);

    void on_startStopBtn_clicked();

    void on_fpsSpinBox_valueChanged(int arg1);

    void on_repeatSpinBox_valueChanged(int arg1);

    void on_methodComboBox_currentIndexChanged(int index);

    void on_streamCountSpinBox_valueChanged(int arg1);

    void on_loadBtn_clicked();

    void on_inputTypeComboBox_currentIndexChanged(int index);

private:
    void enableGUI(bool enable);
    Ui::CameraDebayerWidget *ui;


    bool running{false};

    QSize mSize;
    Debayer::Methods mMethodType{Debayer::Dummy};
    int mStreamCount{2};
    int mFps{0};
    int mFrameCount{0};

    InputType mCurrentInputType{IMAGE};

    bool storring{false};
    bool recording{false};

    int counter = 0;

    QString mImageFilename{""};
    QString mMJPEGdataFilename{""};
    QString mMJPEGindexFilename{""};


    bool mInit = {false};
    int mCudaDeviceNumber = {0};

    // grabbers
    DemoFrameGrabber * mDemoFrameGrabber    {nullptr};
    QThread mDemoFrameGrabberThread;

    // debayer
    Debayer * mDebayerUnit {nullptr};
    QThread mDebayerThread;

    QTimer mShowTimer;
    VideoFrame * mCurrentDebayeredFrame {nullptr};

    QMutex mutex;

    // stats
    QTimer fpsTimer;
    int mFrameToDebayerCounter{0};
    int mDebayeredFrameCounter{0};
};

#endif // CAMERADEBAYERWIDGET_H
