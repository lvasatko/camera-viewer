#include "debayer.h"
#include <QDebug>
#include <cuda.h>
#include <QElapsedTimer>


#include <cuda_profiler_api.h>
#include <cuda_runtime.h>
#include <QThread>

#include <fstream>

#include "include/Methods/dfpd.h"
#include "include/Methods/dummy.h"
#include "include/Methods/hqli.h"
#include "include/Methods/ahda.h"
#include "include/Methods/bilinear.h"
#include "include/Methods/ahda_edge.h"

Debayer::Debayer(int cudaDeviceNumber, QObject *parent): QObject(parent),
    cudaDevice(cudaDeviceNumber)
{
    qRegisterMetaType<VideoFrame *>("VideoFrame *");
}

Debayer::~Debayer()
{
    for (Worker * worker: m_workers) {
        worker->stop();

        while(worker->processing()){
            QThread::sleep(1);
        }
    }

    for (Worker * worker: m_workers) {
        delete worker;
    }

}

void Debayer::init(int method, int streamCount, uint width, uint height)
{

    size = QSize(width,height);

    for (int var = 0; var < streamCount; ++var)
    {
        DebayerMethod * m_method;

        switch(method)
        {
        case Dummy:
            m_method = new DUMMY();
            break;
        case Dfpd:
            m_method = new DFPD();
            break;
        case Hqli:
            m_method = new HQLI();
            break;
        case Ahda:
            m_method = new AHDA();
            break;
        case Bilinear:
            m_method = new BILINEAR();
            break;
        case Ahda_edge:
            m_method = new AHDA_EDGE();
            break;
        default:
            std::cerr << "[ERROR]\t " << Q_FUNC_INFO << method << " - method with this ID is not implemented" << std::endl;
            continue;
            break;
        }

        Worker * worker = new Worker(this ,m_method,cudaDevice);

        connect(worker,SIGNAL(storeProcessedFrame(VideoFrame*)),this,SIGNAL(newDebayeredFrame(VideoFrame*)));

        m_workers.append(worker);
    }

    for (Worker * worker : m_workers) {
        QMetaObject::invokeMethod(worker,"init",Qt::BlockingQueuedConnection,Q_ARG(uint,size.width()),Q_ARG(uint,size.height()));
    }

}

void Debayer::init(QList<DebayerMethod *> methodObjecs, uint width, uint height)
{
    size = QSize(width,height);

    for (DebayerMethod * methodObject : methodObjecs)
    {
        Worker * worker = new Worker(this ,methodObject,cudaDevice);

        connect(worker,SIGNAL(storeProcessedFrame(VideoFrame*)),this,SIGNAL(newDebayeredFrame(VideoFrame*)));

        m_workers.append(worker);
    }

    for (Worker * worker : m_workers) {
        QMetaObject::invokeMethod(worker,"init",Qt::BlockingQueuedConnection,Q_ARG(uint,size.width()),Q_ARG(uint,size.height()));
    }

}

void Debayer::enqueueFrame(VideoFrame *frame)
{
    m_framesToProcessMutex.lock();

    // try to find available worker
    Worker * m_freeWorker = nullptr;

    for (Worker * worker : m_workers) {
        if(!worker->processing()) {
            m_freeWorker = worker;
            break;
        }
    }

    // no frames in queue
    if(m_framesToProcess.empty()) {

        // no worker available -> enqueue frame
        if(m_freeWorker == nullptr) {
            m_framesToProcess.enqueue(frame);
        }
        // worker available -> start processing frame
        else {
            QMetaObject::invokeMethod(m_freeWorker,"processFrame",Q_ARG(VideoFrame *,frame));
        }
    }
    else {
        // store frame
        m_framesToProcess.enqueue(frame);

        // worker available -> start processing frame
        if(m_freeWorker != nullptr) {
            QMetaObject::invokeMethod(m_freeWorker,"processFrame",Q_ARG(VideoFrame *,m_framesToProcess.dequeue()));
        }
    }

    m_framesToProcessMutex.unlock();

}

VideoFrame *Debayer::dequeueFrame()
{
    VideoFrame *frame = nullptr;

    m_framesToProcessMutex.lock();

    if(!m_framesToProcess.empty()) {
        frame = m_framesToProcess.dequeue();
    }

    m_framesToProcessMutex.unlock();

    return frame;
}
