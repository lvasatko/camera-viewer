#ifndef HQLI_H
#define HQLI_H
#include "include/Methods/debayermethod.h"

class HQLI : public DebayerMethod
{
public:
    HQLI();

    bool init(uint width, uint height);

    bool processFrame(VideoFrame *frame);
    uint bytesPerFrame(uint width, uint height);

    void destroy();

private:

    uchar4 * device_outData;
    uchar3 * device_outData_final;
};

#endif // HQLI_H
