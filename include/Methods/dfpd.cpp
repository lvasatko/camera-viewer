#include "dfpd.h"

extern "C"
void cuda_DFPD_init(cudaArray_t *texArray,
                    uchar4 ** m_cuda_device_outData,
                    uchar3 ** m_cuda_device_outData_final,
                    float ** m_horizontalG,           float ** m_verticalG,
                    float ** m_horizontalGradientG,   float ** m_verticalGradientG,
                    int width, int height,
                    unsigned char ** m_cuda_host_inputData,
                    uchar3 ** m_cuda_host_outputData,
                    cudaEvent_t * start, cudaEvent_t * stop,
                    cudaArray ** texHorizontalGradientArray,
                    cudaArray ** texVerticalGradientArray);

extern "C"
void cuda_DFPD(cudaStream_t stream,
               cudaArray * texArray,
               uchar4 * m_cuda_outData,
               uchar3 * m_cuda_outData_final,
               unsigned char * inputData,
               uchar3 * outputData,
               float * horizontalG,           float * verticalG,
               float * horizontalGradientG,   float * verticalGradientG,
               int width, int height,
               cudaEvent_t start, cudaEvent_t stop,
               cudaArray * texHorizontalGradientArray,
               cudaArray * texVerticalGradientArray);

extern "C"
void cuda_DFPD_destroy(cudaArray * texArray,
                       uchar4 * m_cuda_outData,
                       uchar3 * m_cuda_outData_final,
                       unsigned char * inputData,
                       uchar3 * outputData,
                       float * horizontalG,float * verticalG,
                       float * horizontalGradientG,float * verticalGradientG,
                       cudaEvent_t start, cudaEvent_t stop,
                       cudaArray * texHorizontalGradientArray,
                       cudaArray * texVerticalGradientArray);

DFPD::DFPD()
{

}

bool DFPD::init(uint width, uint height)
{
    this->width = width;
    this->height = height;

    cudaStreamCreate(&stream);

    cuda_DFPD_init(&inputTexArray,
                   &m_cuda_device_outData,&m_cuda_device_outData_final,
                   &m_horizontalG,        &m_verticalG,
                   &m_horizontalGradientG,&m_verticalGradientG,
                   width,height,      &host_inputData,&host_outputData,
                   &start,&stop,
                   &texHorizontalGradientArray,&texVerticalGradientArray);
    return true;

}

bool DFPD::processFrame(VideoFrame * frame)
{
    if(width != frame->width() || height != frame->height())
        return false;

    void * newData = frame->data();

    // copy data into paged memory
    memcpy(host_inputData,newData,width*height);

    // run dfpd in stream
    cuda_DFPD(stream,inputTexArray,
              m_cuda_device_outData,
              m_cuda_device_outData_final,
              host_inputData,
              host_outputData,
              m_horizontalG, m_verticalG,
              m_horizontalGradientG,m_verticalGradientG,
              width, height,
              start,stop,
              texHorizontalGradientArray,
              texVerticalGradientArray);

    // on CPU delete data and allocate new memory
    free(newData); // delete previous data
    unsigned char * processedData = (unsigned char *)malloc(width*height*sizeof(uchar3));


    cudaStreamSynchronize(stream); // wait

    float  ms;
    cudaEventElapsedTime(&ms,start,stop);

    kernelAverage += ms;

    if(frameCounter == 500) {
        std::cout  << "[INFO]\tKernel average: " << kernelAverage/500 << std::endl;
        frameCounter = 0;
        kernelAverage = 0;
    }
    else {
        frameCounter++;
    }

    memcpy(processedData,host_outputData,width*height*sizeof(uchar3));
    frame->setData(processedData);

    return true;
}

uint DFPD::bytesPerFrame(uint width, uint height)
{
    uint bytes = 0;

    bytes += width*height*sizeof(uchar4);

    bytes += width*height*sizeof(uchar3);

    bytes += width*height;

    bytes += width*height*sizeof(float)*6;

    return bytes;
}

void DFPD::destroy()
{
    cuda_DFPD_destroy(inputTexArray,
                      m_cuda_device_outData,
                      m_cuda_device_outData_final,
                      host_inputData,
                      host_outputData,
                      m_horizontalG,m_verticalG,
                      m_horizontalGradientG,m_verticalGradientG,
                      start,stop,
                      texHorizontalGradientArray,texVerticalGradientArray);

    cudaStreamDestroy(stream);
}

