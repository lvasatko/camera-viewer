#include "ahda_edge.h"
#include <iomanip>

extern "C"
void cuda_ahda_init_new(cudaArray_t *inputTexArray,
                       cudaArray **texCielabHorizontalArray,
                       cudaArray **texCielabVerticalArray,

                       float4 ** cielabHorizontal,
                       float4 ** cielabVertical,


                       cudaArray **texHomogeneityHorizontalArray,
                       cudaArray **texHomogeneityVerticalArray,

                       unsigned char ** homogeneityHorizontal,
                       unsigned char ** homogeneityVertical,

                       uchar4 ** horizontal,
                       uchar4 ** vertical,

                       uchar4 ** output,
                       uchar3 ** output_final,
                       uchar3 ** host_output,

                       unsigned char ** host_inputData,

                       int width,
                       int height,
                       cudaStream_t * stream,
                       cudaEvent_t * start,
                       cudaEvent_t * stop,
                       unsigned char **mask,
                       float **grayscale);

extern "C"
void cuda_ahda_process_new(cudaArray_t inputTexArray,

                          cudaArray *texCielabHorizontalArray,
                          cudaArray *texCielabVerticalArray,

                          float4 * cielabHorizontal,
                          float4 * cielabVertical,

                          cudaArray *texHomogeneityHorizontalArray,
                          cudaArray *texHomogeneityVerticalArray,

                          unsigned char * homogeneityHorizontal,
                          unsigned char * homogeneityVertical,

                          uchar4 * horizontal,
                          uchar4 * vertical,

                          uchar4 * output,
                          uchar3 * output_final,
                          uchar3 * host_output,



                          int width,
                          int height,
                          cudaStream_t stream,
                          unsigned char * host_inputData,
                          cudaEvent_t start,
                          cudaEvent_t stop,
                          unsigned char *mask,
                          float *grayscale,
                          int * edgePixelCount);

extern "C"
void cuda_ahda_destroy_new(cudaArray_t inputTexArray,

                          cudaArray *texCielabHorizontalArray,
                          cudaArray *texCielabVerticalArray,

                          float4 * cielabHorizontal,
                          float4 * cielabVertical,


                          cudaArray *texHomogeneityHorizontalArray,
                          cudaArray *texHomogeneityVerticalArray,

                          unsigned char * homogeneityHorizontal,
                          unsigned char * homogeneityVertical,

                          uchar4 * horizontal,
                          uchar4 * vertical,

                          uchar4 * output,
                          uchar3 * output_final,
                          uchar3 * host_output,
                          unsigned char * host_inputData,
                          cudaStream_t  stream,
                          cudaEvent_t start,cudaEvent_t stop,
                          unsigned char *mask,
                          float *grayscale);


AHDA_EDGE::AHDA_EDGE()
{

}

bool AHDA_EDGE::init(uint width, uint height)
{
    this->width = width;
    this->height = height;

    cuda_ahda_init_new(&inputTexArray,
                      &texCielabHorizontalArray,
                      &texCielabVerticalArray,

                      &cielabHorizontal,
                      &cielabVertical,


                      &texHomogeneityHorizontalArray,
                      &texHomogeneityVerticalArray,

                      &homogeneityHorizontal,
                      &homogeneityVertical,

                      &horizontal,
                      &vertical,

                      &output,
                      &output_final,
                      &host_outputData,
                      &host_inputData,
                      width,
                      height,
                      &stream,
                      &start,
                      &stop,
                      &mask,
                      &grayscale);

    return true;
}

bool AHDA_EDGE::processFrame(VideoFrame * frame)
{
    if(width != frame->width() || height != frame->height())
        return false;

    unsigned char * newData = (unsigned char *)frame->data();


    // copy data into locked memory
    memcpy(host_inputData,newData,width*height);

    int edgePixelCount = 0;

    cuda_ahda_process_new(inputTexArray,

                         texCielabHorizontalArray,
                         texCielabVerticalArray,

                         cielabHorizontal,
                         cielabVertical,

                         texHomogeneityHorizontalArray,
                         texHomogeneityVerticalArray,

                         homogeneityHorizontal,
                         homogeneityVertical,

                         horizontal,
                         vertical,

                         output,
                         output_final,
                         host_outputData,
                         width,
                         height,
                         stream,
                         host_inputData,
                         start,
                         stop,
                         mask,
                         grayscale,
                         &edgePixelCount);

    free(newData); // delete previous data
    unsigned char * processedData;
    if((processedData = (unsigned char *)malloc(width*height*sizeof(uchar3))) == nullptr) {
        std::cout << "[ERROR]\t malloc error\n";
    }


    cudaStreamSynchronize(stream); // wait for finished

    float  ms;
    cudaEventElapsedTime(&ms,start,stop);

    kernelAverage += ms;

    if(ms < kernelMin)
        kernelMin = ms;

    if(ms > kernelMax)
        kernelMax = ms;

    edgePixelCountAverage += edgePixelCount;

    if(edgePixelCount < minEdgePixelCount)
        minEdgePixelCount = edgePixelCount;

    if(edgePixelCount > maxEdgePixelCount)
        maxEdgePixelCount = edgePixelCount;


    if(frameCounter == 1000) {
        std::cout  << "/////////////////////////////////////////////////" << std:: endl;
        std::cout  << "[INFO] TIME" << std:: endl;
        std::cout  << "[INFO]\tKernel average: " << std::fixed << std::setprecision(3) << (float)kernelAverage/1000.0 << std::endl;
        std::cout  << "[INFO]\t min:     " << kernelMin << std::endl;
        std::cout  << "[INFO]\t max:     " << kernelMax << std::endl;
        std::cout  << "[INFO] EDGES" << std:: endl;
        std::cout  << "[INFO]\t average: " << std::fixed << std::setprecision(3) << (float)edgePixelCountAverage/1000.0 << std::endl;
        std::cout  << "[INFO]\t min:     " << minEdgePixelCount << std::endl;
        std::cout  << "[INFO]\t max:     " << maxEdgePixelCount << std::endl;
        frameCounter = 0;
        edgePixelCountAverage = 0;
        kernelAverage = 0;
        kernelMax = -35635;
        kernelMin =  35635;
        minEdgePixelCount = INT_MAX;
        maxEdgePixelCount = INT_MIN;
    }
    else {
        frameCounter++;
    }


    // copy data from locked memory
    memcpy(processedData,host_outputData,width*height*sizeof(uchar3));

    frame->setData(processedData);

    return true;
}

uint AHDA_EDGE::bytesPerFrame(uint width, uint height)
{
    uint bytes = 0;

    bytes += width*height*sizeof(float);

    bytes += width*height;

    bytes += width*height*sizeof(float4)*4;

    bytes += width*height*sizeof(uchar3)*2;

    bytes += width*height*2;

    bytes += width*height*sizeof(uchar4)*3;

    bytes += width*height*sizeof(uchar3)*1;

    return bytes;
}

void AHDA_EDGE::destroy()
{
    cuda_ahda_destroy_new(inputTexArray,
                         texCielabHorizontalArray,
                         texCielabVerticalArray,

                         cielabHorizontal,
                         cielabVertical,


                         texHomogeneityHorizontalArray,
                         texHomogeneityVerticalArray,

                         homogeneityHorizontal,
                         homogeneityVertical,

                         horizontal,
                         vertical,

                         output,
                         output_final,
                         host_outputData,
                         host_inputData,
                         stream,
                         start,
                         stop,
                         mask,
                         grayscale);
}
