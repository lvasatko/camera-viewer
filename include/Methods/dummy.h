#ifndef DUMMY_H
#define DUMMY_H
#include "include/Methods/debayermethod.h"

class DUMMY : public DebayerMethod
{
public:
    explicit DUMMY();

    bool init(uint width, uint height);

    bool processFrame(VideoFrame * frame);
    uint bytesPerFrame(uint width, uint height);

    void destroy();

private:

    uchar3 * m_cuda_device_outputData3;
    uchar4* m_cuda_device_outputData {nullptr};

    size_t pitch;

    unsigned char * m_texArray;


    size_t m_pitch;

    cudaTextureObject_t m_tex;
};

#endif // DUMMY_H
