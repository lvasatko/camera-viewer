#ifndef DEBAYERMETHOD_H
#define DEBAYERMETHOD_H

#include <cuda_runtime.h>
#include <iostream>

#include <cstdlib>
#include <cstring>
#include "videoframe.h"


class DebayerMethod
{
public:
    virtual ~DebayerMethod(){}
    /////////////////
    /// \brief init
    /// \return true if initialization succeded
    /// this virtual function should contaion implemenation of initialization of given method
    virtual bool init(uint width, uint height) = 0;
    //////////
    /// \brief processFrame
    /// \param newData
    /// \param processedData
    /// \param width
    /// \param height
    /// \return
    ///
    virtual bool processFrame(VideoFrame * frame) = 0;
    ////////////////////////
    /// \brief bytesPerFrame
    /// \param width
    /// \param height
    /// \return
    ///
    virtual uint bytesPerFrame(uint width, uint height) = 0;

    /////////////////////////////
    /// \brief destroy
    ///
    virtual void destroy() = 0;

protected:

    // common
    uint width;
    uint height;

    cudaStream_t  stream;

    // MEMORY

    // input
    cudaArray_t inputTexArray;


    // output
    unsigned char * host_inputData;
    uchar3 * host_outputData;


    // statistics
    cudaEvent_t start;
    cudaEvent_t stop;

    int frameCounter {0};
    float kernelAverage {0.0};
    float kernelMax{-35635};
    float kernelMin{35635};


};

#endif // DEBAYERMETHOD_H
