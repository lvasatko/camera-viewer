#include "bilinear.h"

#include <QImage>

extern "C"
void cuda_bilinearDebayer_init(unsigned char ** host_inputData, uchar3 ** host_outputData,
                               uchar4 ** device_outputData, uchar3 ** device_outputData_final,
                               cudaArray_t * inputArray,
                               int width, int height, cudaStream_t * stream);

extern "C"
void cuda_bilinearDebayer_process(unsigned char * inputData, uchar3 * outputData,
                                  cudaArray_t  inputArray,
                                  uchar4 * device_outputData, uchar3 * device_outputData_final,
                                  int width, int height, cudaStream_t  stream);

extern "C"
void cuda_bilinearDebayer_destroy(unsigned char * host_inputData, uchar3 * host_outputData,
                                  uchar4 * device_outputData, uchar3 * device_outputData_final,
                                  cudaArray_t  inputArray, cudaStream_t  stream);

bool BILINEAR::init(uint width, uint height)
{
    this->width = width;
    this->height = height;

    cuda_bilinearDebayer_init(&host_inputData,&host_outputData,
                              &device_outputData, &device_outputData_final,
                              &inputArray,
                              width,  height,&stream);

    return true;
}

bool BILINEAR::processFrame(VideoFrame * frame)
{
    if(width != frame->width() || height != frame->height())
        return false;

    memcpy(host_inputData,frame->data(),width*height);


    cuda_bilinearDebayer_process(host_inputData, host_outputData,
                                 inputArray,
                                 device_outputData,device_outputData_final,
                                 width,  height,stream);


    free(frame->data()); // delete previous data
    unsigned char * processedData = (unsigned char *)malloc(width*height*sizeof(uchar3));

    cudaStreamSynchronize(stream);

    memcpy(processedData,host_outputData,width*height*sizeof(uchar3));

    frame->setData(processedData);

    return true;
}

uint BILINEAR::bytesPerFrame(uint width, uint height)
{
    uint bytes = 0;

    bytes += width*height*sizeof(uchar4);
    bytes += width*height*sizeof(uchar3);
    bytes += width*height;

    return bytes;
}

void BILINEAR::destroy()
{
    cuda_bilinearDebayer_destroy(host_inputData, host_outputData,
                                 device_outputData, device_outputData_final,
                                 inputArray,stream);
}

