#ifndef AHDA_EDGE_H
#define AHDA_EDGE_H
#include "include/Methods/debayermethod.h"

class AHDA_EDGE : public DebayerMethod
{
public:
    explicit AHDA_EDGE();

    bool init(uint width, uint height);

    bool processFrame(VideoFrame *frame);
    uint bytesPerFrame(uint width, uint height);

    void destroy();

private:

    cudaArray * texCielabHorizontalArray;
    cudaArray * texCielabVerticalArray;

    float4 * cielabHorizontal;
    float4 * cielabVertical;


    cudaArray * texHomogeneityHorizontalArray;
    cudaArray * texHomogeneityVerticalArray;

    unsigned char * homogeneityHorizontal;
    unsigned char * homogeneityVertical;

    uchar4 * horizontal;
    uchar4 * vertical;

    uchar4 * output;
    uchar3 * output_final;


    int edgePixelCountAverage{0};
    int minEdgePixelCount{INT_MAX};
    int maxEdgePixelCount{INT_MIN};

    unsigned char *mask;
    unsigned char *mask3x3;
    unsigned char *mask5x5;
    float *grayscale;

};



#endif // AHDA_EDGE_H
