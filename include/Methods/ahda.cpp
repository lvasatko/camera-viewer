#include "ahda.h"


extern "C"
void cuda_ahda_init(cudaArray_t *inputTexArray,
                    cudaArray **texCielabHorizontalArray,
                    cudaArray **texCielabVerticalArray,

                    float4 ** cielabHorizontal,
                    float4 ** cielabVertical,


                    cudaArray **texHomogeneityHorizontalArray,
                    cudaArray **texHomogeneityVerticalArray,

                    unsigned char ** homogeneityHorizontal,
                    unsigned char ** homogeneityVertical,

                    uchar4 ** horizontal,
                    uchar4 ** vertical,

                    uchar4 ** device_output,
                    uchar3 ** device_output_final,
                    uchar3 ** host_output,

                    unsigned char ** host_inputData,

                    int width,
                    int height,
                    cudaStream_t * stream,
                    cudaEvent_t * start,cudaEvent_t * stop);

extern "C"
void cuda_ahda_process(cudaArray_t inputTexArray,

                               cudaArray *texCielabHorizontalArray,
                               cudaArray *texCielabVerticalArray,

                               float4 * cielabHorizontal,
                               float4 * cielabVertical,

                               cudaArray *texHomogeneityHorizontalArray,
                               cudaArray *texHomogeneityVerticalArray,

                               unsigned char * homogeneityHorizontal,
                               unsigned char * homogeneityVertical,

                               uchar4 * horizontal,
                               uchar4 * vertical,

                               uchar4 * output,
                               uchar3 * device_output_final,
                               uchar3 * host_output,



                               int width,
                               int height,
                               cudaStream_t stream,
                               unsigned char * host_inputData,
                               cudaEvent_t start,cudaEvent_t stop);

extern "C"
void cuda_ahda_destroy(cudaArray_t inputTexArray,

                                cudaArray *texCielabHorizontalArray,
                                cudaArray *texCielabVerticalArray,

                                float4 * cielabHorizontal,
                                float4 * cielabVertical,


                                cudaArray *texHomogeneityHorizontalArray,
                                cudaArray *texHomogeneityVerticalArray,

                                unsigned char * homogeneityHorizontal,
                                unsigned char * homogeneityVertical,

                                uchar4 * horizontal,
                                uchar4 * vertical,

                                uchar4 * device_output,
                                uchar3 * device_output_final,
                                uchar3 * host_output,
                                unsigned char * host_inputData,
                                cudaStream_t  stream,
                                cudaEvent_t start,cudaEvent_t stop);


AHDA::AHDA()
{

}

bool AHDA::init(uint width, uint height)
{
    this->width = width;
    this->height = height;

    cuda_ahda_init(&inputTexArray,
                        &texCielabHorizontalArray,
                        &texCielabVerticalArray,

                        &cielabHorizontal,
                        &cielabVertical,


                        &texHomogeneityHorizontalArray,
                        &texHomogeneityVerticalArray,

                        &homogeneityHorizontal,
                        &homogeneityVertical,

                        &horizontal,
                        &vertical,

                        &device_output,
                        &device_output_final,
                        &host_output,
                        &host_inputData,
                        width,
                        height,
                        &stream,
                        &start,&stop);

    return true;
}

bool AHDA::processFrame(VideoFrame * frame)
{
    if(width != frame->width() || height != frame->height())
        return false;

    void * newData = frame->data();


    // copy data into locked memory
    memcpy(host_inputData,newData,width*height);

    cuda_ahda_process(inputTexArray,

                   texCielabHorizontalArray,
                   texCielabVerticalArray,

                   cielabHorizontal,
                   cielabVertical,

                   texHomogeneityHorizontalArray,
                   texHomogeneityVerticalArray,

                   homogeneityHorizontal,
                   homogeneityVertical,

                   horizontal,
                   vertical,

                   device_output,
                   device_output_final,
                   host_output,
                   width,
                   height,
                   stream,
                   host_inputData,
                   start,stop);

    free(newData); // delete previous data
    unsigned char *processedData = (unsigned char *)malloc(width*height*sizeof(uchar3));

    cudaStreamSynchronize(stream); // wait for finished

    if(processedData == nullptr) {
        std::cout << "[ERROR]\t malloc error\n";
        return false;
    }


    float  ms;

    cudaEventElapsedTime(&ms,start,stop);

    kernelAverage += ms;

    if(frameCounter == 1000) {
        std::cout  << "[INFO]\tKernel average: " << kernelAverage/1000 << std::endl;
        frameCounter = 0;
        kernelAverage = 0;
    }
    else {
        frameCounter++;
    }


    // copy data from locked memory
    memcpy(processedData,host_output,width*height*sizeof(uchar3));
    frame->setData(processedData);

    return true;
}

uint AHDA::bytesPerFrame(uint width, uint height)
{
    uint bytes = 0;

    bytes += width*height;

    bytes += width*height*sizeof(float4)*4;

    bytes += width*height*sizeof(uchar1)*2;

    bytes += width*height*sizeof(uchar4)*3;

    return bytes;
}

void AHDA::destroy()
{
    cuda_ahda_destroy(inputTexArray,
                     texCielabHorizontalArray,
                        texCielabVerticalArray,

                        cielabHorizontal,
                        cielabVertical,


                        texHomogeneityHorizontalArray,
                        texHomogeneityVerticalArray,

                        homogeneityHorizontal,
                        homogeneityVertical,

                        horizontal,
                        vertical,

                        device_output,
                        device_output_final,
                        host_output,
                        host_inputData,
                        stream,
                        start,stop);
}
