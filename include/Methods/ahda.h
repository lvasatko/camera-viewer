#ifndef AHDA_H
#define AHDA_H
#include "include/Methods/debayermethod.h"

class AHDA : public DebayerMethod
{
public:
    explicit AHDA();

    bool init(uint width, uint height);

    bool processFrame(VideoFrame *frame);
    uint bytesPerFrame(uint width, uint height);

    void destroy();

private:

    cudaArray * texCielabHorizontalArray;
    cudaArray * texCielabVerticalArray;

    float4 * cielabHorizontal;
    float4 * cielabVertical;

    cudaArray * texHomogeneityHorizontalArray;
    cudaArray * texHomogeneityVerticalArray;

    unsigned char * homogeneityHorizontal;
    unsigned char * homogeneityVertical;

    uchar4 * horizontal;
    uchar4 * vertical;

    uchar4 * device_output;
    uchar3 * device_output_final;
    uchar3 * host_output;
};

#endif // AHDA_H
