#ifndef DFPD_H
#define DFPD_H
#include "include/Methods/debayermethod.h"

class DFPD : public DebayerMethod
{
public:
    explicit DFPD();

    bool init(uint width, uint height);

    bool processFrame(VideoFrame * frame);
    uint bytesPerFrame(uint width, uint height);

    void destroy();

private:

    // texture
    cudaArray * texHorizontalGradientArray{0};
    cudaArray * texVerticalGradientArray{0};

    float * m_horizontalG;
    float * m_verticalG;
    float * m_horizontalGradientG;
    float * m_verticalGradientG;

    uchar4 * m_cuda_device_outData;
    uchar3 * m_cuda_device_outData_final;
};



#endif // DFPD_H
