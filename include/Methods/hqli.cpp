#include "hqli.h"

extern "C"
void highQualityLineraInterpolation(unsigned char * inputData,
                                    unsigned char ** outputData ,
                                    int width, int height,
                                    float alpha = 0, float beta = 0, float gama = 0);



extern "C"
void highQualityLineraInterpolation_init(uchar4 ** device_outData,
                                         uchar3 ** device_outData_final,
                                         uchar3 ** host_outData,
                                         unsigned char ** host_inData,
                                         cudaArray_t *texArray,
                                         int width,
                                         int height,
                                         cudaStream_t * stream,
                                         cudaEvent_t * start,
                                         cudaEvent_t * stop);

extern "C"
void highQualityLineraInterpolation_process(uchar4 * device_outData,
                                            uchar3 * device_outData_final,
                                            uchar3 * host_outData,
                                            unsigned char * host_inData,
                                            cudaArray_t texArray,
                                            int width,
                                            int height,
                                            cudaStream_t stream,
                                            cudaEvent_t start,
                                            cudaEvent_t stop);


extern "C"
void highQualityLineraInterpolation_destroy(uchar4 * device_outData,
                                            uchar3 * device_outData_final,
                                            uchar3 * host_outData,
                                            unsigned char * host_inData,
                                            cudaArray_t texArray,
                                            cudaStream_t stream,
                                            cudaEvent_t start,
                                            cudaEvent_t stop);

HQLI::HQLI()
{

}

bool HQLI::init(uint width, uint height)
{
    this->width = width;
    this->height = height;

    highQualityLineraInterpolation_init(&device_outData,
                                        &device_outData_final,
                                        &host_outputData,
                                        &host_inputData,
                                        &inputTexArray,
                                        width,
                                        height,
                                        &stream,
                                        &start,&stop);


    return true;
}

bool HQLI::processFrame(VideoFrame * frame)
{
    if(width != frame->width() || height != frame->height())
        return false;

    void * newData = frame->data();


    // copy data into paged memory
    memcpy(host_inputData,newData,width*height);



    highQualityLineraInterpolation_process(device_outData,
                                           device_outData_final,
                                           host_outputData,
                                           host_inputData,
                                           inputTexArray,
                                           width,
                                           height,
                                           stream,
                                           start,stop);


    unsigned char *processedData = (unsigned char *)malloc(width*height*sizeof(uchar3));

    free(newData); // delete previous data

    cudaStreamSynchronize(stream); // wait for finished


    float  ms;
    cudaEventElapsedTime(&ms,start,stop);
    kernelAverage += ms;

    if(frameCounter == 1000) {
        std::cout  << "[INFO]\tKernel average: " << kernelAverage/1000 << std::endl;
        frameCounter = 0;
        kernelAverage = 0;
    }
    else {
        frameCounter++;
    }

    memcpy(processedData,host_outputData,width*height*sizeof(uchar3));

    frame->setData(processedData);

    return true;
}

uint HQLI::bytesPerFrame(uint width, uint height)
{
    uint bytes = 0;

    bytes += width*height*sizeof(uchar4);
    bytes += width*height*sizeof(uchar3);
    bytes += width*height;

    return bytes;
}

void HQLI::destroy()
{
    highQualityLineraInterpolation_destroy(device_outData,
                                           device_outData_final,
                                           host_outputData,
                                           host_inputData,
                                           inputTexArray,
                                           stream,
                                           start,stop);
}

