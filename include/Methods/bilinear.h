#ifndef BILINEAR_H
#define BILINEAR_H
#include "include/Methods/debayermethod.h"

class BILINEAR : public DebayerMethod
{
public:
    bool init(uint width, uint height);

    bool processFrame(VideoFrame *frame);
    uint bytesPerFrame(uint width, uint height);

    void destroy();

private:

    unsigned char * host_inputData;
    uchar3 * host_outputData;
    uchar4 * device_outputData;
    uchar3 * device_outputData_final;
    cudaArray_t inputArray;
};

#endif // BILINEAR_H
