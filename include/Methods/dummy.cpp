#include "dummy.h"

extern "C"
void cuda_basicDebayer_init(unsigned char ** texArray,uchar4 **outData,uchar3 ** device_outputData3,size_t *m_outDataPitch,size_t * m_pitch,cudaTextureObject_t * m_tex,int width, int height,cudaEvent_t * start,cudaEvent_t * stop);

extern "C"
void cuda_basicDebayer(cudaStream_t stream,unsigned char * texArray,uchar4 * cuda_outData,uchar3 * device_outputData3,size_t m_outDataPitch,unsigned char * inputData, uchar3 * outputData,cudaTextureObject_t m_tex,size_t m_pitch, int width, int height,cudaEvent_t start,cudaEvent_t stop);

extern "C"
void cuda_basicDebayer_destroy(unsigned char * texArray,uchar4 * cuda_outData,uchar3 * device_outputData3,cudaEvent_t start,cudaEvent_t stop);


extern "C"
void cuda_convertUchar4to3(uchar4 * source,uchar3 * destination, int width, int height);

DUMMY::DUMMY()
{

}

bool DUMMY::init(uint width, uint height)
{
    this->width = width;
    this->height = height;

    cudaStreamCreate(&stream);



    cudaMallocHost(&host_outputData,width*height*sizeof(uchar4));
    cudaMallocHost(&host_inputData,width*height);

    cuda_basicDebayer_init(&m_texArray,&m_cuda_device_outputData,&m_cuda_device_outputData3,&pitch,&m_pitch,&m_tex,width,height,&start,&stop);

    return true;

}

bool DUMMY::processFrame(VideoFrame * frame)// unsigned char *newData, unsigned char **processedData, uint width, uint height)
{

    if(width != frame->width() || height != frame->height())
        return false;

    void * newData = frame->data();


    // copy data into paged memory
    memcpy(host_inputData,newData,width*height);




    cuda_basicDebayer(stream,
                      m_texArray,
                      m_cuda_device_outputData,
                      m_cuda_device_outputData3,
                      pitch,
                      host_inputData,
                      host_outputData,
                      m_tex,m_pitch,
                      width,height,
                      start,stop);

    ////////////////////
    /// overlaped by GPU
    free(newData); // delete previous data
    void  * processedData = (unsigned char *)malloc(width*height*sizeof(uchar3));
    ////////////////////

    cudaStreamSynchronize(stream); // wait for finished

    float  ms;

    cudaEventElapsedTime(&ms,start,stop);

    kernelAverage += ms;

    if(frameCounter == 1000) {
        std::cout  << "[INFO]\tKernel average: " << kernelAverage/1000 << std::endl;
        frameCounter = 0;
        kernelAverage = 0;
    }
    else {
        frameCounter++;
    }

    memcpy(processedData,host_outputData,width*height*sizeof(uchar3));

    frame->setData(processedData);

    return true;
}

uint DUMMY::bytesPerFrame(uint width, uint height)
{
    uint bytes = 0;

    bytes += width*height*sizeof(uchar4);
    bytes += width*height*sizeof(uchar3);
    bytes += width*height;

    return bytes;
}

void DUMMY::destroy()
{
    cuda_basicDebayer_destroy(m_texArray,m_cuda_device_outputData,m_cuda_device_outputData3,start,stop);

    cudaFreeHost(host_inputData);
    cudaFreeHost(host_outputData);

    cudaStreamDestroy(stream);
}
