
#include <cuda_runtime.h>
#include <cuda_texture_types.h>
#include <math.h>
#include <stdio.h>

#include <iostream>

#include "cuda_code.cuh"

texture<unsigned char, 2, cudaReadModeElementType> texInputDataRef;
texture<uchar4, 2, cudaReadModeElementType> texInputDataRef4;

__device__ float colorMax = 255.0;

__global__ void convertUchar4to3(uchar4 * source,uchar3 * destination, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x > width || y > height)
        return;

    // G R
    // B G

    uint inX = x*2;
    uint inY = y*2;

    if(inX >= width || inY >= height)
        return;

    // G [0,0]
    uchar4 sourceData = source[inX +inY*width];
    uchar3 destinationData = {sourceData.x,sourceData.y,sourceData.z};

    destination[inX +inY*width] = destinationData;


    inX +=1;

    if(inX >= width || inY >= height)
        return;

    // R [1,0]
    sourceData = source[inX +inY*width];
    destinationData.x = sourceData.x;
    destinationData.y = sourceData.y;
    destinationData.z = sourceData.z;

    destination[inX +inY*width] = destinationData;

    inX -=1;
    inY +=1;

    if(inX >= width || inY >= height)
        return;

    // B [0,1]
    sourceData = source[inX +inY*width];
    destinationData.x = sourceData.x;
    destinationData.y = sourceData.y;
    destinationData.z = sourceData.z;

    destination[inX +inY*width] = destinationData;

    inX +=1;

    if(inX >= width || inY >= height)
        return;

    // G [1,1]
    sourceData = source[inX +inY*width];
    destinationData.x = sourceData.x;
    destinationData.y = sourceData.y;
    destinationData.z = sourceData.z;

    destination[inX +inY*width] = destinationData;
}


//_____  ______ _______   ______ _____   _____ ______  _____
//|  __ \|  ____|__   __| |  ____|  __ \ / ____|  ____|/ ____|
//| |  | | |__     | |    | |__  | |  | | |  __| |__  | (___
//| |  | |  __|    | |    |  __| | |  | | | |_ |  __|  \___ \
//| |__| | |____   | | _  | |____| |__| | |__| | |____ ____) |
//|_____/|______|  |_|(_) |______|_____/ \_____|______|_____/
// detection of edges in image using Sobel filter

__device__ int d_PixelCount = 0;

__device__ void  sobel_count_edge(
        unsigned char ul, // upper left
        unsigned char um, // upper middle
        unsigned char ur, // upper right
        unsigned char ml, // middle left
        unsigned char mr, // middle right
        unsigned char ll, // lower left
        unsigned char lm, // lower middle
        unsigned char lr  // lower right
        )
{
    int horizontal = -ur - 2*mr - lr + ul + 2*ml + ll;
    int vertical = -ul - 2*um - ur + ll + 2*lm + lr;

    if((abs(horizontal)+abs(vertical)) > 40) {
        atomicAdd(&d_PixelCount,1);
    }
}


__global__ void detectEdges(float * grayscale,int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x < 1 || y < 1 || x >= width -1 || y >= height -1)
        return;

    sobel_count_edge(grayscale[x -1 + (y-1)*width], grayscale[x + (y-1)*width],
                     grayscale[x +1 + (y-1)*width], grayscale[x -1 + (y)*width],
                     grayscale[x +1 + (y)*width],   grayscale[x -1 + (y+1)*width],
                     grayscale[x + (y+1)*width],    grayscale[x +1 + (y+1)*width]);
}


__global__ void convertToGrayscale(float * grayscale, int width)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    d_PixelCount = 0;

    float r  = ((uchar4)tex2D(texInputDataRef4,x,y)).x;    //R
    float g  = ((uchar4)tex2D(texInputDataRef4,x,y)).y;    //G
    float b  = ((uchar4)tex2D(texInputDataRef4,x,y)).z;    //B


    grayscale[x + y*width] =  0.59f*g + 0.3f*r +0.11f*b;

}

extern "C"
__host__ void cuda_detectEdges(unsigned char * inputData,int width, int height)
{
    dim3 threadsPerBlock(32, 32);
    dim3 numBlocks(width/threadsPerBlock.x,height/threadsPerBlock.y);

    texInputDataRef.addressMode[0] = cudaAddressModeClamp;
    texInputDataRef.addressMode[1] = cudaAddressModeClamp;
    texInputDataRef.filterMode = cudaFilterModePoint;
    texInputDataRef.normalized = false;

    cudaArray *texArray = 0;
    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar4>();

    int count = 0;
    float * grayscale;


    CUDA_SAFE_CALL(cudaMemcpyToSymbol(d_PixelCount,&count,sizeof(int),0,cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL(cudaMalloc(&grayscale, width*height*sizeof(float)));

    CUDA_SAFE_CALL(cudaMallocArray( &texArray, &channelDesc, width, height));
    CUDA_SAFE_CALL(cudaMemcpyToArray(texArray, 0, 0, inputData, width * height*sizeof(uchar4), cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL(cudaBindTextureToArray( texInputDataRef4, (cudaArray*)texArray, channelDesc));

    convertToGrayscale<<<numBlocks,threadsPerBlock>>>(grayscale,width);
    detectEdges<<<numBlocks,threadsPerBlock>>>(grayscale,width,height);

    CUDA_SAFE_CALL(cudaMemcpyFromSymbol(&count,d_PixelCount,sizeof(int),0,cudaMemcpyDeviceToHost));

    // Unbind the image and projection matrix textures
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRef));

    // Cleanup
    CUDA_SAFE_CALL(cudaFreeArray((cudaArray*)texArray));
    CUDA_SAFE_CALL(cudaFree(grayscale));
}


//   _____ _____  ________     _______  _____          _      ______
//  / ____|  __ \|  ____\ \   / / ____|/ ____|   /\   | |    |  ____|
// | |  __| |__) | |__   \ \_/ / (___ | |       /  \  | |    | |__
// | | |_ |  _  /|  __|   \   / \___ \| |      / /\ \ | |    |  __|
// | |__| | | \ \| |____   | |  ____) | |____ / ____ \| |____| |____
//  \_____|_|  \_\______|  |_| |_____/ \_____/_/    \_\______|______|
// convert image to grayscale

__global__ void convertToGreyscale(uchar4 * outData, int width)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    outData[x + y*width].x  = tex2D(texInputDataRef,x,y);    //R
    outData[x + y*width].y  = tex2D(texInputDataRef,x,y);    //G
    outData[x + y*width].z  = tex2D(texInputDataRef,x,y);    //B
    outData[x + y*width].w  = 255;

}

extern "C"
__host__ void cuda_convertToGreyscale(unsigned char * inputData,unsigned char * outputData, int width, int height)
{
    dim3 threadsPerBlock(32, 32);
    dim3 numBlocks(width/threadsPerBlock.x,height/threadsPerBlock.y);

    texInputDataRef.addressMode[0] = cudaAddressModeClamp;
    texInputDataRef.addressMode[1] = cudaAddressModeClamp;
    texInputDataRef.filterMode = cudaFilterModePoint;
    texInputDataRef.normalized = false;

    cudaArray *texArray = 0;
    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();

    uchar4 * outData;
    CUDA_SAFE_CALL(cudaMalloc(&outData, width*height*sizeof(uchar4)));
    CUDA_SAFE_CALL(cudaMallocArray( &texArray, &channelDesc, width, height));
    CUDA_SAFE_CALL(cudaMemcpyToArray( texArray, 0, 0, inputData, width * height, cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL(cudaBindTextureToArray( texInputDataRef, (cudaArray*)texArray, channelDesc));

    // kernels
    convertToGreyscale<<<numBlocks,threadsPerBlock>>>(outData,width);

    CUDA_SAFE_CALL(cudaMemcpy(outputData, outData, width*height*sizeof(uchar4), cudaMemcpyDeviceToHost ));

    // Unbind the image and projection matrix textures
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRef));

    // Cleanup
    CUDA_SAFE_CALL(cudaFreeArray((cudaArray*)texArray));
    CUDA_SAFE_CALL(cudaFree(outData));
}


// _   _ ______ _____ _____ _    _ ____   ____  _    _ _____
//| \ | |  ____|_   _/ ____| |  | |  _ \ / __ \| |  | |  __ \
//|  \| | |__    | || |  __| |__| | |_) | |  | | |  | | |__) |
//| . ` |  __|   | || | |_ |  __  |  _ <| |  | | |  | |  _  /
//| |\  | |____ _| || |__| | |  | | |_) | |__| | |__| | | \ \
//|_| \_|______|_____\_____|_|  |_|____/ \____/ \____/|_|  \_\
// neighbour method


__global__ void basicDebayer(cudaTextureObject_t m_tex,uchar4 * outData,size_t m_outDataPitch, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;


    // take every four pixels

    // G R
    // B G

    uint inX = x*2;
    uint inY = y*2;

    if(inX >= width || inY >= height)
        return;
    uchar4 pixel;

    // G [0,0]
    pixel.x = (float)tex2D<unsigned char>(m_tex,inX+1,inY);      //R
    pixel.y = (float)tex2D<unsigned char>(m_tex,inX,inY);          //G
    pixel.z = (float)tex2D<unsigned char>(m_tex,inX,inY+1);    //B
    pixel.w = 255;
    outData[inX + inY*width] = pixel;


    inX +=1;

    if(inX >= width || inY >= height)
        return;
    // R [1,0]
    pixel.x = (float)tex2D<unsigned char>(m_tex,inX,inY);       //R
    pixel.y = (float)tex2D<unsigned char>(m_tex,inX-1,inY);     //G
    pixel.z = (float)tex2D<unsigned char>(m_tex,inX-1,inY+1);   //B
    outData[inX + inY*width] = pixel;


    inX -=1;
    inY +=1;


    if(inX >= width || inY >= height)
        return;

    // B [0,1]
    pixel.x = (float)tex2D<unsigned char>(m_tex,inX+1,inY-1);//R
    pixel.y = (float)tex2D<unsigned char>(m_tex,inX+1,inY);      //G
    pixel.z = (float)tex2D<unsigned char>(m_tex,inX,inY);          //B
    outData[inX + inY*width] = pixel;

    inX +=1;

    if(inX >= width || inY >= height)
        return;


    // G [1,1]
    pixel.x = (float)tex2D<unsigned char>(m_tex,inX,inY-1);       //R
    pixel.y = (float)tex2D<unsigned char>(m_tex,inX,inY);          //G
    pixel.z = (float)tex2D<unsigned char>(m_tex,inX-1,inY) ;      //B
    outData[inX + inY*width] = pixel;
}

extern "C"
void cuda_basicDebayer_init(unsigned char **texArray, uchar4 ** device_outputData,uchar3 ** device_outputData3, size_t *m_outDataPitch, size_t *m_pitch, cudaTextureObject_t *m_tex, int width, int height, cudaEvent_t *start, cudaEvent_t *stop)
{
    CUDA_SAFE_CALL(cudaMalloc(device_outputData, width*height*sizeof(uchar4)));
    CUDA_SAFE_CALL(cudaMalloc(device_outputData3, width*height*sizeof(uchar3)));

    cudaEventCreate(start);
    cudaEventCreate(stop);


    cudaMallocPitch(texArray, m_pitch, width, height);


    cudaResourceDesc resDesc;
    memset(&resDesc, 0, sizeof(resDesc));
    resDesc.resType = cudaResourceTypePitch2D;
    resDesc.res.pitch2D.devPtr = *texArray;
    resDesc.res.pitch2D.pitchInBytes = *m_pitch;
    resDesc.res.pitch2D.width = width;
    resDesc.res.pitch2D.height = height;
    resDesc.res.pitch2D.desc = cudaCreateChannelDesc<unsigned char>();


    cudaTextureDesc texDesc;
    memset(&texDesc, 0, sizeof(texDesc));
    texDesc.readMode =  cudaReadModeElementType;
    texDesc.normalizedCoords=false;
    texDesc.addressMode[0]=cudaAddressModeWrap;
    texDesc.addressMode[1]=cudaAddressModeWrap;

    cudaCreateTextureObject(m_tex, &resDesc, &texDesc, NULL);
}


extern "C"
void cuda_basicDebayer(cudaStream_t stream, unsigned char * texArray, uchar4 * device_outputData, uchar3 * device_outputData3, size_t m_outDataPitch, unsigned char * host_inputData, uchar3 *host_outputData,
                       cudaTextureObject_t m_tex, size_t m_pitch, int width, int height, cudaEvent_t start, cudaEvent_t stop)
{
    dim3 threadsPerBlock(32,32);

    dim3 numBlocks_half(ceil((double)width/threadsPerBlock.x/2.0),ceil((double)height/threadsPerBlock.y/2.0));

    CUDA_SAFE_CALL(cudaMemcpy2DAsync(texArray,m_pitch,host_inputData, width, width, height,cudaMemcpyHostToDevice,stream));


    cudaEventRecord(start,stream);
    basicDebayer<<<numBlocks_half,threadsPerBlock,0,stream>>>(m_tex,device_outputData,m_outDataPitch,width,height);

    convertUchar4to3<<<numBlocks_half,threadsPerBlock,0,stream>>>(device_outputData,device_outputData3, width, height);
    cudaEventRecord(stop,stream);

    cudaMemcpyAsync(host_outputData, device_outputData3, width*height*sizeof(uchar3),cudaMemcpyDeviceToHost,stream);
}

extern "C"
void cuda_basicDebayer_destroy(unsigned char *texArray, uchar4 * cuda_outData,uchar3 * device_outputData3,cudaEvent_t start,cudaEvent_t stop)
{
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    // Unbind the image and projection matrix textures
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRef));

    // Cleanup
    CUDA_SAFE_CALL(cudaFree(texArray));
    CUDA_SAFE_CALL(cudaFree(cuda_outData));
    CUDA_SAFE_CALL(cudaFree(device_outputData3));
}

// ____ _____ _      _____ _   _ ______          _____
//|  _ \_   _| |    |_   _| \ | |  ____|   /\   |  __ \
//| |_) || | | |      | | |  \| | |__     /  \  | |__) |
//|  _ < | | | |      | | | . ` |  __|   / /\ \ |  _  /
//| |_) || |_| |____ _| |_| |\  | |____ / ____ \| | \ \
//|____/_____|______|_____|_| \_|______/_/    \_\_|  \_\
// bilinear interpolation method

__global__ void bilinearInterpolation(uchar4 * outData, int width, int height)
{


    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x *=2;
    y *=2;

    if(x >= width || y >= height)
        return;



    // take every four pixels

    // G R
    // B G


    // G
    float r = (tex2D(texInputDataRef,x-1,y) + tex2D(texInputDataRef,x+1,y))/2.0;
    float g = tex2D(texInputDataRef,x,y);
    float b = (tex2D(texInputDataRef,x,y-1) + tex2D(texInputDataRef,x,y+1))/2.0;

    uchar4 pixel;
    pixel.x = r;
    pixel.y = g;
    pixel.z = b;
    pixel.w = 255;

    outData[(x + y*width)] = pixel;

    // R
    x+=1;

    if(x >= width || y >= height)
        return;


    r = tex2D(texInputDataRef,x,y);
    g = (tex2D(texInputDataRef,x+1,y) + tex2D(texInputDataRef,x-1,y) + tex2D(texInputDataRef,x,y+1) + tex2D(texInputDataRef,x,y-1))/4.0;
    b = (tex2D(texInputDataRef,x+1,y+1) + tex2D(texInputDataRef,x-1,y-1) + tex2D(texInputDataRef,x+1,y-1) + tex2D(texInputDataRef,x-1,y+1))/4.0;

    pixel.x = r;
    pixel.y = g;
    pixel.z = b;

    outData[(x + y*width)] = pixel;

    // B
    x-=1;
    y+=1;

    if(x >= width || y >= height)
        return;


    r = (tex2D(texInputDataRef,x+1,y+1) + tex2D(texInputDataRef,x-1,y-1) + tex2D(texInputDataRef,x+1,y-1) + tex2D(texInputDataRef,x-1,y+1))/4.0;
    g = (tex2D(texInputDataRef,x+1,y) + tex2D(texInputDataRef,x-1,y) + tex2D(texInputDataRef,x,y+1) + tex2D(texInputDataRef,x,y-1))/4.0;
    b = tex2D(texInputDataRef,x,y);

    pixel.x = r;
    pixel.y = g;
    pixel.z = b;

    outData[(x + y*width)] = pixel;

    // G
    x+=1;

    if(x >= width || y >= height)
        return;


    r = (tex2D(texInputDataRef,x,y-1) + tex2D(texInputDataRef,x,y+1))/2.0;
    g = tex2D(texInputDataRef,x,y);
    b = (tex2D(texInputDataRef,x-1,y) + tex2D(texInputDataRef,x+1,y))/2.0;

    pixel.x = r;
    pixel.y = g;
    pixel.z = b;

    outData[(x + y*width)] = pixel;

}

extern "C"
void cuda_bilinearDebayer_init(unsigned char ** host_inputData, uchar3 ** host_outputData,
                               uchar4 ** device_outputData, uchar3 ** device_outputData_final,
                               cudaArray_t * inputArray,
                               int width, int height, cudaStream_t *stream)
{

    cudaStreamCreate(stream);

    cudaMallocHost(host_inputData,width*height);
    cudaMallocHost(host_outputData,width*height*sizeof(uchar3));

    cudaMalloc(device_outputData,width*height*sizeof(uchar4));
    cudaMalloc(device_outputData_final,width*height*sizeof(uchar3));

    texInputDataRef.addressMode[0] = cudaAddressModeClamp;
    texInputDataRef.addressMode[1] = cudaAddressModeClamp;
    texInputDataRef.filterMode = cudaFilterModePoint;    // souradnice jsou brany primo (neni potreba udavat stred pixelu)
    texInputDataRef.normalized = false;

    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();
    CUDA_SAFE_CALL(cudaMallocArray(inputArray, &channelDesc, width, height ));
    CUDA_SAFE_CALL(cudaBindTextureToArray( texInputDataRef, *inputArray, channelDesc)); // pro pristup do linearni pameti

}


extern "C"
void cuda_bilinearDebayer_process(unsigned char * inputData, uchar3 *outputData, cudaArray_t  inputArray, uchar4 * device_outputData, uchar3 * device_outputData_final, int width, int height, cudaStream_t stream)
{
    dim3 threadsPerBlock(32, 32);
    dim3 numBlocks(ceil((double)width/(double)threadsPerBlock.x), ceil((double)height/(double)threadsPerBlock.y));
    dim3 numBlocks_by4(ceil((double)width/(double)threadsPerBlock.x/2.0), ceil((double)height/(double)threadsPerBlock.y/2.0));

    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(inputArray, 0, 0, inputData, width*height*sizeof(unsigned char), cudaMemcpyHostToDevice,stream));

    bilinearInterpolation<<<numBlocks_by4,threadsPerBlock,0,stream>>>(device_outputData,width,height);

    convertUchar4to3<<<numBlocks,threadsPerBlock,0,stream>>>(device_outputData,device_outputData_final,width,height);

    CUDA_SAFE_CALL(cudaMemcpyAsync(outputData, device_outputData_final, width*height*sizeof(uchar3), cudaMemcpyDeviceToHost,stream ));
}


extern "C"
void cuda_bilinearDebayer(unsigned char * inputData, unsigned char * outputData, int width, int height)
{

    dim3 threadsPerBlock(32, 32);
    dim3 numBlocks(ceil((double)width/(double)threadsPerBlock.x), ceil((double)height/(double)threadsPerBlock.y));
    dim3 numBlocks_by4(ceil((double)width/(double)threadsPerBlock.x/2.0), ceil((double)height/(double)threadsPerBlock.y/2.0));


    uchar4 * outData;
    CUDA_SAFE_CALL(cudaMalloc( &outData, width*height*sizeof(uchar4)));

    uchar3 * outData_final;
    CUDA_SAFE_CALL(cudaMalloc( &outData_final, width*height*sizeof(uchar3)));


    texInputDataRef.addressMode[0] = cudaAddressModeClamp;
    texInputDataRef.addressMode[1] = cudaAddressModeClamp;
    texInputDataRef.filterMode = cudaFilterModePoint;    // souradnice jsou brany primo (neni potreba udavat stred pixelu)
    texInputDataRef.normalized = false;

    cudaArray *texArray = 0;
    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();

    CUDA_SAFE_CALL(cudaMallocArray( &texArray, &channelDesc, width, height ));
    CUDA_SAFE_CALL(cudaMemcpyToArray( texArray, 0, 0, inputData, width*height*sizeof(unsigned char), cudaMemcpyHostToDevice));
    CUDA_SAFE_CALL(cudaBindTextureToArray( texInputDataRef, (cudaArray*)texArray, channelDesc));

    bilinearInterpolation<<<numBlocks_by4,threadsPerBlock>>>(outData,width,height);

    convertUchar4to3<<<numBlocks,threadsPerBlock>>>(outData,outData_final,width,height);

    //bilinearDebayer_secondStep<<<numBlocks,threadsPerBlock>>>(outData,width,height);
                                                               // copy result
    CUDA_SAFE_CALL(cudaMemcpy(outputData, outData_final, width*height*sizeof(uchar3), cudaMemcpyDeviceToHost ));

    // Unbind the image and projection matrix textures
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRef));

    // Cleanup
    CUDA_SAFE_CALL(cudaFreeArray ((cudaArray*)texArray));
    CUDA_SAFE_CALL(cudaFree(outData));
    CUDA_SAFE_CALL(cudaFree(outData_final));
}


extern "C"
void cuda_bilinearDebayer_destroy(unsigned char * host_inputData, uchar3 * host_outputData,
                               uchar4 * device_outputData, uchar3 * device_outputData_final,
                               cudaArray_t  inputArray, cudaStream_t  stream)
{
    cudaFreeHost(host_inputData);
    cudaFreeHost(host_outputData);


    cudaFree(device_outputData);
    cudaFree(device_outputData_final);

    cudaFreeArray(inputArray);

    cudaStreamDestroy(stream);
}



//   _____ _____  ____   _____
//  / ____|  __ \|  _ \ / ____|
// | |  __| |__) | |_) | |  __
// | | |_ |  _  /|  _ <| | |_ |
// | |__| | | \ \| |_) | |__| |
//  \_____|_|  \_\____/ \_____|
// conversion of RGBA image to GRBG CFA

__global__ void convertToBayerGRBG(unsigned char * outData,int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;

    if(x >= width || y >= height)
        return;

    // G

    outData[x + y*width] = ((uchar4)tex2D(texInputDataRef4,x,y)).y;
    x+=1;

    if(x >= width || y >= height)
        return;

    // R
    outData[x + y*width] = ((uchar4)tex2D(texInputDataRef4,x,y)).z;
    x-=1;
    y+=1;

    if(x >= width || y >= height)
        return;

    // B
    outData[x + y*width] = ((uchar4)tex2D(texInputDataRef4,x,y)).x;

    x+=1;
    if(x >= width || y >= height)
        return;

    // G
    outData[x + y*width] = ((uchar4)tex2D(texInputDataRef4,x,y)).y;
}


extern "C"
void cuda_convertToBayer_init(unsigned char ** device_outputData,cudaArray ** inputTexArray,int width, int height,cudaStream_t * stream)
{
    CUDA_SAFE_CALL(cudaStreamCreate(stream));
    CUDA_SAFE_CALL(cudaMalloc(device_outputData, width*height));

    texInputDataRef4.addressMode[0] = cudaAddressModeClamp;
    texInputDataRef4.addressMode[1] = cudaAddressModeClamp;
    texInputDataRef4.filterMode = cudaFilterModePoint;
    texInputDataRef4.normalized = false;

    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar4>();

    CUDA_SAFE_CALL(cudaMallocArray( inputTexArray, &channelDesc, width, height ));
    CUDA_SAFE_CALL(cudaBindTextureToArray( texInputDataRef4, *inputTexArray, channelDesc));
}


extern "C"
void cuda_convertToBayer(unsigned char * inputData,cudaArray * inputTexArray,unsigned char * device_outputData,unsigned char * host_outputData,int width, int height,cudaStream_t stream)
{
    dim3 threadsPerBlock(32, 32);
    dim3 numBlocks_by4(ceil((double)width/(double)threadsPerBlock.x/2.0),ceil((double)height/(double)threadsPerBlock.y/2.0));

    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(inputTexArray, 0, 0, inputData, width*height*sizeof(uchar4), cudaMemcpyHostToDevice,stream));

    convertToBayerGRBG<<<numBlocks_by4,threadsPerBlock,0,stream>>>(device_outputData,width,height);

    CUDA_SAFE_CALL(cudaMemcpyAsync(host_outputData, device_outputData, width*height, cudaMemcpyDeviceToHost,stream));

    cudaStreamSynchronize(stream);
}


extern "C"
void cuda_convertToBayer_destroy(cudaArray * inputTexArray,unsigned char * device_outputData,cudaStream_t stream)
{
    CUDA_SAFE_CALL(cudaFree(device_outputData));

    // Unbind the image and projection matrix textures
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRef4));

    // Cleanup
    CUDA_SAFE_CALL(cudaFreeArray(inputTexArray));
    CUDA_SAFE_CALL(cudaStreamDestroy(stream));
}


//   _____  _____          _      ______      _____  ______ ____      __     ________ _____
//  / ____|/ ____|   /\   | |    |  ____|    |  __ \|  ____|  _ \   /\\ \   / /  ____|  __ \
// | (___ | |       /  \  | |    | |__       | |  | | |__  | |_) | /  \\ \_/ /| |__  | |__) |
//  \___ \| |      / /\ \ | |    |  __|      | |  | |  __| |  _ < / /\ \\   / |  __| |  _  /
//  ____) | |____ / ____ \| |____| |____     | |__| | |____| |_) / ____ \| |  | |____| | \ \
// |_____/ \_____/_/    \_\______|______|    |_____/|______|____/_/    \_\_|  |______|_|  \_\
// demosaicing by scaling 4 pixels into 1 output pixel


__global__ void scaleDebayer(uchar4 * outData, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;


    if(x >= width || y >= height)
        return;

    // take every four pixels

    // G R
    // B G

    uint inX = x*2;
    uint inY = y*2;

    outData[x + y*width].x    = fminf((float)tex2D(texInputDataRef,inX+1,inY),colorMax);
    outData[x + y*width].y    = fminf((float)tex2D(texInputDataRef,inX,inY),colorMax);
    outData[x + y*width].z    = fminf((float)tex2D(texInputDataRef,inX,inY+1),colorMax);
}

extern "C"
void cuda_scaleDebayer(unsigned char * inputData,unsigned char * outputData, int width, int height)
{

    dim3 threadsPerBlock(32, 32);

    dim3 numBlocks(ceil((double)width/(double)threadsPerBlock.x/2.0), ceil((double)height/(double)threadsPerBlock.y/2.0));


    uchar4 * outData;
    CUDA_SAFE_CALL(cudaMalloc( &outData, width*height*sizeof(uchar4)));


    texInputDataRef.addressMode[0] = cudaAddressModeClamp;
    texInputDataRef.addressMode[1] = cudaAddressModeClamp;
    texInputDataRef.filterMode = cudaFilterModePoint;    // souradnice jsou brany primo (neni potreba udavat stred pixelu)
    texInputDataRef.normalized = false;


    cudaArray *texArray = 0;
    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();

    CUDA_SAFE_CALL(cudaMallocArray( &texArray, &channelDesc, width, height ));
    CUDA_SAFE_CALL(cudaMemcpyToArray( texArray, 0, 0, inputData, width * height * sizeof(unsigned char), cudaMemcpyHostToDevice));

    scaleDebayer<<<numBlocks,threadsPerBlock>>>(outData,width,height);

    // copy result
    CUDA_SAFE_CALL(cudaMemcpy(outputData, outData, width*height*sizeof(uchar4), cudaMemcpyDeviceToHost ));

    // Unbind the image and projection matrix textures
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRef));

    // Cleanup
    CUDA_SAFE_CALL(cudaFreeArray ((cudaArray*)texArray));
    CUDA_SAFE_CALL(cudaFree(outData));
}
