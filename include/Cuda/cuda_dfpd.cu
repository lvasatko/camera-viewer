#include "cuda_code.cuh"
#include <math.h>

texture<unsigned char, 2, cudaReadModeElementType> texInputDataRef;

texture<float, 2, cudaReadModeElementType> texHorizontalGrandientRef;
texture<float, 2, cudaReadModeElementType> texVerticalGrandientRef;


__device__ float colorMax = 255.0;


//        _____  ______ _____  _____
//        |  __ \|  ____|  __ \|  __ \
//        | |  | | |__  | |__) | |  | |
//        | |  | |  __| |  ___/| |  | |
//        | |__| | |    | |    | |__| |
//        |_____/|_|    |_|    |_____/


__global__ void DFPD_greenHandV(float * horizontalG,float * verticalG,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;

    // R [1,0]
    x+=1;

    if(x >= width || y >= height) {
        return;
    }

    // horizontal
    horizontalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x,y)
                                       + ((float)tex2D(texInputDataRef,x + 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x + 2,y))/2.0f
                                          + ((float)tex2D(texInputDataRef,x - 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x - 2,y))/2.0f))/2.0f,colorMax);

    // vertical
    verticalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x,y)
                                     + ((float)tex2D(texInputDataRef,x,y + 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y + 2))/2.0f
                                        + ((float)tex2D(texInputDataRef,x,y - 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y - 2))/2.0f))/2.0f,colorMax);

    // B [0,1]
    x-=1;
    y+=1;


    if(x >= width || y >= height) {
        return;
    }

    // horizontal
    horizontalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x ,y)
                                       + ((float)tex2D(texInputDataRef,x + 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x + 2,y))/2.0f
                                          + ((float)tex2D(texInputDataRef,x - 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x - 2,y))/2.0f))/2.0f,colorMax);

    // vertical
    verticalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x,y)
                                     + ((float)tex2D(texInputDataRef,x,y + 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y + 2))/2.0f
                                        + ((float)tex2D(texInputDataRef,x,y - 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y - 2))/2.0f))/2.0f,colorMax);
}

__global__ void DFPD_chrominance(float * horizontalG,float * verticalG,float * horizontalGradientG,float * verticalGradientG,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;


    x*=2;
    y*=2;


    // R
    x+=1;

    if(x >= width || y >= height) {
        return;
    }

    float orig = tex2D(texInputDataRef,x,y);

    // horizontal
    horizontalGradientG[(x + y*width)]  = orig - horizontalG[(x + y*width)];

    // vertical
    verticalGradientG[(x + y*width)]    = orig - verticalG[(x + y*width)];

    // B
    x-=1;
    y+=1;

    if(x >= width || y >= height) {
        return;
    }

    orig = tex2D(texInputDataRef,x,y);

    // horizontal
    horizontalGradientG[(x + y*width)]  =  orig - horizontalG[(x + y*width)];

    // vertical
    verticalGradientG[(x + y*width)]    = orig - verticalG[(x + y*width)];

}

__global__ void DFPD_chrominanceGradient(float * horizontalGradientG,float * verticalGradientG,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;


    // R
    x+=1;

    if(x >= width || y >= height)
        return;

    // horizontal
    if(y+2 < height)
        horizontalGradientG[(x + y*width)]  = fabs(horizontalGradientG[(x + y*width)] - horizontalGradientG[(x + (y+2)*width)]);


    // vertical
    if(x+2 < width)
        verticalGradientG[(x + y*width)]    = fabs(verticalGradientG[(x + y*width)] - verticalGradientG[(x + 2 + y*width)]);

    // B
    x-=1;
    y+=1;

    if(x >= width || y >= height)
        return;

    // horizontal
    if(y+2 < height)
        horizontalGradientG[(x + y*width)]  = fabs(horizontalGradientG[(x + y*width)] - horizontalGradientG[(x + (y+2)*width)]);

    // vertical
    if(x+2 < width)
        verticalGradientG[(x + y*width)]    = fabs(verticalGradientG[(x + y*width)] - verticalGradientG[(x + 2 + y*width)]);
}


__global__ void DFPD_chrominanceCombined(float * horizontalG,float * verticalG,float * horizontalGradientG,float * verticalGradientG,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;

    // R [1,0]
    x+=1;

    if(x >= width || y >= height) {
        return;
    }

    // horizontal
    horizontalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x,y)
                                       + ((float)tex2D(texInputDataRef,x + 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x + 2,y))/2.0f
                                          + ((float)tex2D(texInputDataRef,x - 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x - 2,y))/2.0f))/2.0f,colorMax);

    // vertical
    verticalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x,y)
                                     + ((float)tex2D(texInputDataRef,x,y + 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y + 2))/2.0f
                                        + ((float)tex2D(texInputDataRef,x,y - 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y - 2))/2.0f))/2.0f,colorMax);

    // B [0,1]
    x-=1;
    y+=1;


    if(x >= width || y >= height) {
        return;
    }

    // horizontal
    horizontalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x ,y)
                                       + ((float)tex2D(texInputDataRef,x + 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x + 2,y))/2.0f
                                          + ((float)tex2D(texInputDataRef,x - 1,y) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x - 2,y))/2.0f))/2.0f,colorMax);

    // vertical
    verticalG[(x + y*width)] = fminf((float)tex2D(texInputDataRef,x,y)
                                     + ((float)tex2D(texInputDataRef,x,y + 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y + 2))/2.0f
                                        + ((float)tex2D(texInputDataRef,x,y - 1) - ((float)tex2D(texInputDataRef,x,y) + (float)tex2D(texInputDataRef,x,y - 2))/2.0f))/2.0f,colorMax);





    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    __syncthreads();

    x+=1;
    y-=1;


    // R

    float orig = tex2D(texInputDataRef,x,y);

    // horizontal
    horizontalGradientG[(x + y*width)]  = orig - horizontalG[(x + y*width)];

    // vertical
    verticalGradientG[(x + y*width)]    = orig - verticalG[(x + y*width)];

    // B
    x-=1;
    y+=1;

    orig = tex2D(texInputDataRef,x,y);

    // horizontal
    horizontalGradientG[(x + y*width)]  =  orig - horizontalG[(x + y*width)];

    // vertical
    verticalGradientG[(x + y*width)]    = orig - verticalG[(x + y*width)];

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    __syncthreads();

    y-=1;

    // R

    // horizontal
    if(y+2 < height)
        horizontalGradientG[(x + y*width)]  = fabs(horizontalGradientG[(x + y*width)] - horizontalGradientG[(x + (y+2)*width)]);


    // vertical
    if(x+2 < width)
        verticalGradientG[(x + y*width)]    = fabs(verticalGradientG[(x + y*width)] - verticalGradientG[(x + 2 + y*width)]);

    // B
    x-=1;
    y+=1;

    // horizontal
    if(y+2 < height)
        horizontalGradientG[(x + y*width)]  = fabs(horizontalGradientG[(x + y*width)] - horizontalGradientG[(x + (y+2)*width)]);

    // vertical
    if(x+2 < width)
        verticalGradientG[(x + y*width)]    = fabs(verticalGradientG[(x + y*width)] - verticalGradientG[(x + 2 + y*width)]);
}


__device__ float sumGradients_vertical(int x, int y, int width, int height, float *partSum)
{
    float sum = 0;

    *partSum += tex2D(texVerticalGrandientRef,x,y) + tex2D(texVerticalGrandientRef,x+1,y+1) + tex2D(texVerticalGrandientRef,x-1,y+1) + tex2D(texVerticalGrandientRef,x+1,y-1) +
            tex2D(texVerticalGrandientRef,x-1,y-1) + tex2D(texVerticalGrandientRef,x,y+2) + tex2D(texVerticalGrandientRef,x-2,y+2) + tex2D(texVerticalGrandientRef,x-2,y);


    sum += *partSum = tex2D(texVerticalGrandientRef,x+2,y) + tex2D(texVerticalGrandientRef,x+2,y+2) + tex2D(texVerticalGrandientRef,x+2,y-2) + tex2D(texVerticalGrandientRef,x,y-2) + tex2D(texVerticalGrandientRef,x-2,y-2);


    return sum;
}

__device__ float sumGradients_vertical_second_step(int x, int y, int width, int height,float partSum)
{
    float sum = 0;
    sum += partSum + tex2D(texVerticalGrandientRef,x-2,y-2) + tex2D(texVerticalGrandientRef,x+2,y+2) + tex2D(texVerticalGrandientRef,x-2,y) + tex2D(texVerticalGrandientRef,x-2,y-2) + tex2D(texVerticalGrandientRef,x,y+2);


    return sum;
}


__device__ float sumGradients_horizontal(int x, int y, int width, int height, float *partSum)
{
    float sum = 0;
    *partSum += tex2D(texHorizontalGrandientRef,x,y) + tex2D(texHorizontalGrandientRef,x+1,y+1) + tex2D(texHorizontalGrandientRef,x-1,y+1) + tex2D(texHorizontalGrandientRef,x+1,y-1) +
            tex2D(texHorizontalGrandientRef,x-1,y-1) + tex2D(texHorizontalGrandientRef,x,y+2) + tex2D(texHorizontalGrandientRef,x-2,y+2) + tex2D(texHorizontalGrandientRef,x-2,y);


    sum += *partSum = tex2D(texHorizontalGrandientRef,x+2,y) + tex2D(texHorizontalGrandientRef,x+2,y+2) + tex2D(texHorizontalGrandientRef,x+2,y-2) + tex2D(texHorizontalGrandientRef,x,y-2) + tex2D(texHorizontalGrandientRef,x-2,y-2);


    return sum;
}

__device__ float sumGradients_horizontal_second_step(int x, int y, int width, int height, float partSum)
{
    float sum = 0;

    sum += partSum + tex2D(texHorizontalGrandientRef,x-2,y-2) + tex2D(texHorizontalGrandientRef,x+2,y+2) + tex2D(texHorizontalGrandientRef,x-2,y) + tex2D(texHorizontalGrandientRef,x-2,y-2) + tex2D(texHorizontalGrandientRef,x,y+2);

    return sum;
}

__global__ void DFPD_green(uchar4 * device_outputData,float * horizontalG,float * verticalG,float * horizontalDeltaG,float * verticalDeltaG,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;


    x*=2;
    y*=2;

    // R
    x+=1;

    if(x >= width || y >= height)
        return;

    float partHorizontalSum = 0;
    float deltaH = sumGradients_horizontal(x,y, width,height,&partHorizontalSum);
    horizontalDeltaG[x + y*width] = deltaH;

    float partVerticalSum = 0;
    float deltaV = sumGradients_vertical(x,y, width,height,&partVerticalSum);
    verticalDeltaG[x + y*width] = deltaV;

    device_outputData[x + y*width].y = (deltaV < deltaH) ? verticalG[(x + y*width)] : horizontalG[(x + y*width)];


    // B
    x-=1;
    y+=1;

    if(x >= width || y >= height)
        return;

    deltaH = sumGradients_horizontal_second_step(x,y, width,height,partHorizontalSum);
    horizontalDeltaG[x + y*width] = deltaH;

    deltaV = sumGradients_vertical_second_step(x,y, width,height,partVerticalSum);
    verticalDeltaG[x + y*width] = deltaV;

    device_outputData[x + y*width].y = (deltaV < deltaH) ? verticalG[x + y*width] : horizontalG[x + y*width];
}


__global__ void DFPD_green_R_and_B_at_G(uchar4 * device_outputData,int width)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    //     |G|R|
    //     |B|G|

    x*=2;
    y*=2;


    // G

    uchar4 pixel = device_outputData[x + y*width];
    pixel.x = fminf((float)(((uint)tex2D(texInputDataRef,x - 1,y) + (uint)tex2D(texInputDataRef,x + 1,y)) >> 1),colorMax);
    pixel.y = fminf((float)tex2D(texInputDataRef,x,y),colorMax);
    pixel.z = fminf((float)(((uint)tex2D(texInputDataRef,x,y - 1) + (uint)tex2D(texInputDataRef,x,y + 1)) >> 1),colorMax);
    device_outputData[x + y*width] = pixel;

    // R
    x+=1;

    device_outputData[x + y*width].x   = fminf((float)tex2D(texInputDataRef,x,y),colorMax);

    // B
    x-=1;
    y+=1;

    device_outputData[x + y*width].z   = fminf((float)tex2D(texInputDataRef,x,y),colorMax);

    // G
    x+=1;

    pixel = device_outputData[x + y*width];
    pixel.x = fminf((float)(((uint)tex2D(texInputDataRef,x,y - 1) + (uint)tex2D(texInputDataRef,x,y + 1)) >> 1),colorMax);
    pixel.y = fminf((float)tex2D(texInputDataRef,x,y),colorMax);
    pixel.z = fminf((float)(((uint)tex2D(texInputDataRef,x - 1,y) + (uint)tex2D(texInputDataRef,x + 1,y)) >> 1),colorMax);
    device_outputData[x + y*width] = pixel;
}

__global__ void DFPD_green_R_and_B_at_B_and_R(uchar4 * device_outputData,float * horizontalDeltaG,float * verticalDeltaG,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;


    x*=2;
    y*=2;

    // [1,0] R
    x+=1;

    if(x >= width - 1 || y >= height - 1 || x == 0 || y == 0)
        return;

    uchar4 pixel1;
    uchar4 pixel2;
    float blue;
    if(verticalDeltaG[x + y*width] < horizontalDeltaG[x + y*width]) {

        pixel1 = device_outputData[x - 1 + y*width];
        pixel2 = device_outputData[x + 1 + y*width];

        blue = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.z - (float)pixel1.x + (float)pixel2.z - (float)pixel2.x)/2.0f;
    }
    else {
        pixel1 = device_outputData[x + (y - 1)*width];
        pixel2 = device_outputData[x + (y + 1)*width];

        blue = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.z - (float)pixel1.x + (float)pixel2.z - (float)pixel2.x)/2.0f;
    }
    device_outputData[x + y*width].z  =  fminf(blue,colorMax);



    // [0,1] B
    x-=1;
    y+=1;

    if(x >= width - 1 || y >= height - 1 || x == 0 || y == 0)
        return;

    float red;
    if(verticalDeltaG[x + y*width] < horizontalDeltaG[x + y*width]) {
        pixel1 = device_outputData[x - 1 + y*width];
        pixel2 = device_outputData[x + 1 + y*width];

        red = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.x - (float)pixel1.z + (float)pixel2.x - (float)pixel2.z)/2.0f;
    }
    else {
        pixel1 = device_outputData[x + (y - 1)*width];
        pixel2 = device_outputData[x + (y + 1)*width];

        red = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.x - (float)pixel1.z + (float)pixel2.x - (float)pixel2.z)/2.0f;
    }

    device_outputData[x + y*width].x  = fminf(red,colorMax);
}


__global__ void  DFPD_green_Combined(uchar4 * device_outputData,float * horizontalG,float * verticalG,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;


    x*=2;
    y*=2;

    // R
    x+=1;

    if(x >= width || y >= height)
        return;

    float partHorizontalSum = 0;
    float deltaH_atR = sumGradients_horizontal(x,y, width,height,&partHorizontalSum);

    float partVerticalSum = 0;
    float deltaV_atR = sumGradients_vertical(x,y, width,height,&partVerticalSum);

    device_outputData[x + y*width].y = (deltaV_atR < deltaH_atR) ? verticalG[(x + y*width)] : horizontalG[(x + y*width)];


    // B
    x-=1;
    y+=1;

    if(x >= width || y >= height)
        return;

    float deltaH_atB = sumGradients_horizontal_second_step(x,y, width,height,partHorizontalSum);
    float deltaV_atB = sumGradients_vertical_second_step(x,y, width,height,partVerticalSum);

    device_outputData[x + y*width].y = (deltaV_atB < deltaH_atB) ? verticalG[x + y*width] : horizontalG[x + y*width];


    ////////////////////////////////////////////////////////////////////////////////

    __syncthreads();

    y-=1;

    // G

    uchar4 pixel = device_outputData[x + y*width];
    pixel.x = fminf((float)(((uint)tex2D(texInputDataRef,x - 1,y) + (uint)tex2D(texInputDataRef,x + 1,y)) >> 1),colorMax);
    pixel.y = fminf((float)tex2D(texInputDataRef,x,y),colorMax);
    pixel.z = fminf((float)(((uint)tex2D(texInputDataRef,x,y - 1) + (uint)tex2D(texInputDataRef,x,y + 1)) >> 1),colorMax);
    device_outputData[x + y*width] = pixel;

    // R
    x+=1;

    device_outputData[x + y*width].x   = fminf((float)tex2D(texInputDataRef,x,y),colorMax);

    // B
    x-=1;
    y+=1;

    device_outputData[x + y*width].z   = fminf((float)tex2D(texInputDataRef,x,y),colorMax);

    // G
    x+=1;

    pixel = device_outputData[x + y*width];
    pixel.x = fminf((float)(((uint)tex2D(texInputDataRef,x,y - 1) + (uint)tex2D(texInputDataRef,x,y + 1)) >> 1),colorMax);
    pixel.y = fminf((float)tex2D(texInputDataRef,x,y),colorMax);
    pixel.z = fminf((float)(((uint)tex2D(texInputDataRef,x - 1,y) + (uint)tex2D(texInputDataRef,x + 1,y)) >> 1),colorMax);
    device_outputData[x + y*width] = pixel;


    //////////////////////////////////////////////////////////////////////////////////////

    __syncthreads();


    y-=1;

    // [1,0] R
    if(x >= width - 1 || y >= height - 1 || x == 0 || y == 0)
        return;

    uchar4 pixel1;
    uchar4 pixel2;
    float blue;

    if(deltaV_atR < deltaH_atR) {

        pixel1 = device_outputData[x - 1 + y*width];
        pixel2 = device_outputData[x + 1 + y*width];

        blue = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.z - (float)pixel1.x + (float)pixel2.z - (float)pixel2.x)/2.0f;
    }
    else {
        pixel1 = device_outputData[x + (y - 1)*width];
        pixel2 = device_outputData[x + (y + 1)*width];

        blue = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.z - (float)pixel1.x + (float)pixel2.z - (float)pixel2.x)/2.0f;
    }
    device_outputData[x + y*width].z  =  fminf(blue,colorMax);



    // [0,1] B
    x-=1;
    y+=1;

    if(x >= width - 1 || y >= height - 1 || x == 0 || y == 0)
        return;

    float red;
    if(deltaV_atB < deltaH_atB) {
        pixel1 = device_outputData[x - 1 + y*width];
        pixel2 = device_outputData[x + 1 + y*width];

        red = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.x - (float)pixel1.z + (float)pixel2.x - (float)pixel2.z)/2.0f;
    }
    else {
        pixel1 = device_outputData[x + (y - 1)*width];
        pixel2 = device_outputData[x + (y + 1)*width];

        red = (float)tex2D(texInputDataRef,x,y) + ((float)pixel1.x - (float)pixel1.z + (float)pixel2.x - (float)pixel2.z)/2.0f;
    }

    device_outputData[x + y*width].x  = fminf(red,colorMax);

}


__global__ void DFPD_convertUchar4to3(uchar4 * source,uchar3 * destination, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x > width || y > height)
        return;

    // G R
    // B G

    uint inX = x*2;
    uint inY = y*2;

    if(inX >= width || inY >= height)
        return;

    // G [0,0]
    uchar4 sourceData = source[inX +inY*width];
    uchar3 destinationData = {sourceData.x,sourceData.y,sourceData.z};

    destination[inX +inY*width] = destinationData;


    inX +=1;

    if(inX >= width || inY >= height)
        return;

    // R [1,0]
    sourceData = source[inX +inY*width];
    destinationData.x = sourceData.x;
    destinationData.y = sourceData.y;
    destinationData.z = sourceData.z;

    destination[inX +inY*width] = destinationData;

    inX -=1;
    inY +=1;

    if(inX >= width || inY >= height)
        return;

    // B [0,1]
    sourceData = source[inX +inY*width];
    destinationData.x = sourceData.x;
    destinationData.y = sourceData.y;
    destinationData.z = sourceData.z;

    destination[inX +inY*width] = destinationData;

    inX +=1;

    if(inX >= width || inY >= height)
        return;

    // G [1,1]
    sourceData = source[inX +inY*width];
    destinationData.x = sourceData.x;
    destinationData.y = sourceData.y;
    destinationData.z = sourceData.z;

    destination[inX +inY*width] = destinationData;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





extern "C"
__host__ void cuda_DFPD_init(cudaArray_t *texInputArray,
                             uchar4 ** m_cuda_device_outData,
                             uchar3 ** m_cuda_device_outData_final,
                             float ** m_horizontalG,         float ** m_verticalG,
                             float ** m_horizontalGradientG, float ** m_verticalGradientG,
                             int width, int height,
                             unsigned char ** m_cuda_host_inputData,
                             uchar3 **        m_cuda_host_outputData,
                             cudaEvent_t * start, cudaEvent_t * stop,
                             cudaArray ** texHorizontalGradientArray,
                             cudaArray ** texVerticalGradientArray)
{


    ////////////////////////////////////////////////////////////////////////////////////////

    cudaEventCreate(start);
    cudaEventCreate(stop);

    CUDA_SAFE_CALL(cudaMalloc(m_cuda_device_outData,width*height*sizeof(uchar4)));
    CUDA_SAFE_CALL(cudaMalloc(m_cuda_device_outData_final,width*height*sizeof(uchar3)));

    CUDA_SAFE_CALL(cudaMallocHost(m_cuda_host_inputData,width*height));
    CUDA_SAFE_CALL(cudaMallocHost(m_cuda_host_outputData,width*height*sizeof(uchar3)));

    ////////////////////////////////////////////////////////////////////////////////////////

    // INPUT
    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();

    texInputDataRef.addressMode[0] = cudaAddressModeClamp;
    texInputDataRef.addressMode[1] = cudaAddressModeClamp;
    texInputDataRef.filterMode = cudaFilterModePoint;
    texInputDataRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texInputArray, &channelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texInputDataRef, *texInputArray, channelDesc));


    // GRADIENTS
    static cudaChannelFormatDesc gradientDesc = cudaCreateChannelDesc<float>();
    // horizontal
    texHorizontalGrandientRef.addressMode[0] = cudaAddressModeClamp;
    texHorizontalGrandientRef.addressMode[1] = cudaAddressModeClamp;
    texHorizontalGrandientRef.filterMode = cudaFilterModePoint;
    texHorizontalGrandientRef.normalized = false;


    CUDA_SAFE_CALL(cudaMallocArray(texHorizontalGradientArray, &gradientDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texHorizontalGrandientRef, *texHorizontalGradientArray, gradientDesc));

    // vertical
    texVerticalGrandientRef.addressMode[0] = cudaAddressModeClamp;
    texVerticalGrandientRef.addressMode[1] = cudaAddressModeClamp;
    texVerticalGrandientRef.filterMode = cudaFilterModePoint;
    texVerticalGrandientRef.normalized = false;


    CUDA_SAFE_CALL(cudaMallocArray(texVerticalGradientArray, &gradientDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texVerticalGrandientRef, *texVerticalGradientArray, gradientDesc));

    ////////////////////////////////////////////////////////////////////////////////////////


    CUDA_SAFE_CALL(cudaMalloc(m_horizontalG, width*height * sizeof(float)));
    CUDA_SAFE_CALL(cudaMalloc(m_verticalG, width*height * sizeof(float)));

    CUDA_SAFE_CALL(cudaMalloc(m_horizontalGradientG, width*height*sizeof(float)));
    CUDA_SAFE_CALL(cudaMalloc(m_verticalGradientG, width*height*sizeof(float)));

}


extern "C"
__host__ void cuda_DFPD(cudaStream_t stream,
                        cudaArray * texArray,
                        uchar4 * device_outputData,
                        uchar3 * m_cuda_outData_final,
                        unsigned char * inputData,
                        uchar3 * host_outputData,
                        float * horizontalG,           float * verticalG,
                        float * horizontalGradientG,   float * verticalGradientG,
                        int width, int height,
                        cudaEvent_t start, cudaEvent_t stop,
                        cudaArray * texHorizontalGradientArray,
                        cudaArray * texVerticalGradientArray)
{

    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texArray,0,0,inputData,width*height,cudaMemcpyHostToDevice,stream));

    dim3 threadsPerBlock(32, 32);

    dim3 numBlocksBy4(ceil((double)width/(double)threadsPerBlock.x/2.0f),ceil((double)height/(double)threadsPerBlock.y/2.0f));
    cudaEventRecord(start,stream);

    DFPD_chrominanceCombined<<<numBlocksBy4,threadsPerBlock,0,stream>>>(horizontalG,verticalG,horizontalGradientG,verticalGradientG,width,height);

    // copy cielab data into arrays accessed via texture
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texHorizontalGradientArray, 0,0,horizontalGradientG, width*height*sizeof(float), cudaMemcpyDeviceToDevice,stream));
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texVerticalGradientArray,   0,0,verticalGradientG,   width*height*sizeof(float), cudaMemcpyDeviceToDevice,stream));

    DFPD_green_Combined<<<numBlocksBy4,threadsPerBlock,0,stream>>>(device_outputData,horizontalG,verticalG,width,height);

    DFPD_convertUchar4to3<<<numBlocksBy4,threadsPerBlock,0,stream>>>(device_outputData,m_cuda_outData_final,width,height);
    cudaEventRecord(stop,stream);

    // copy result
    CUDA_SAFE_CALL(cudaMemcpyAsync(host_outputData, m_cuda_outData_final, width*height*sizeof(uchar3), cudaMemcpyDeviceToHost,stream));
}

extern "C"
__host__ void cuda_DFPD_destroy(cudaArray * texArray,
                                uchar4 * m_cuda_outData,
                                uchar3 *m_cuda_outData_final,
                                unsigned char * inputData,
                                uchar3 * outputData,
                                float * horizontalG, float * verticalG,
                                float * horizontalGradientG, float * verticalGradientG,
                                cudaEvent_t start, cudaEvent_t stop,
                                cudaArray * texHorizontalGradientArray,
                                cudaArray * texVerticalGradientArray)
{


    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    // Unbind the image and projection matrix textures
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRef));

    // Cleanup
    CUDA_SAFE_CALL(cudaFreeArray(texArray));

    CUDA_SAFE_CALL(cudaFreeArray(texHorizontalGradientArray));
    CUDA_SAFE_CALL(cudaFreeArray(texVerticalGradientArray));


    CUDA_SAFE_CALL(cudaFreeHost(inputData));
    CUDA_SAFE_CALL(cudaFreeHost(outputData));



    CUDA_SAFE_CALL(cudaFree(horizontalG));
    CUDA_SAFE_CALL(cudaFree(verticalG));
    CUDA_SAFE_CALL(cudaFree(horizontalGradientG));
    CUDA_SAFE_CALL(cudaFree(verticalGradientG));

    CUDA_SAFE_CALL(cudaFree(m_cuda_outData));
    CUDA_SAFE_CALL(cudaFree(m_cuda_outData_final));
}

