#include "cuda_code.cuh"
#include <math.h>

texture<unsigned char, 2, cudaReadModeElementType> texInputDataRefHqli;

__device__ float colorMax = 255.0;


//          _    _  ____  _      _____
//         | |  | |/ __ \| |    |_   _|
//         | |__| | |  | | |      | |
//         |  __  | |  | | |      | |
//         | |  | | |__| | |____ _| |_
//         |_|  |_|\___\_\______|_____|
// high quality linear interpolation method

__global__ void highQualityLineraInterpolationBy4_R_B_at_B_R_by4(uchar4 * outData, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;

    // take every four pixels

    // G R
    // B G

    x++;
    // R [1,0]
    float blue = (2.0f*((float)tex2D(texInputDataRefHqli,x-1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y-1) + (float)tex2D(texInputDataRefHqli,x-1,y+1) + (float)tex2D(texInputDataRefHqli,x+1,y+1))
            + 6.0f*(float)tex2D(texInputDataRefHqli,x,y)
            - 3.0f*((float)tex2D(texInputDataRefHqli,x-2,y) + (float)tex2D(texInputDataRefHqli,x+2,y) + (float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y+2))/2.0f)/8.0f;

    outData[x + y*width].z  = fmin(blue,255.0);

    x--;
    y++;

    // B [0,1]
    float red = (2.0f*((float)tex2D(texInputDataRefHqli,x-1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y-1) + (float)tex2D(texInputDataRefHqli,x-1,y+1) + (float)tex2D(texInputDataRefHqli,x+1,y+1))
            + 6.0f*(float)tex2D(texInputDataRefHqli,x,y)
            - 3.0f*((float)tex2D(texInputDataRefHqli,x-2,y) + (float)tex2D(texInputDataRefHqli,x+2,y) + (float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y+2))/2.0f)/8.0f;

    outData[x + y*width].x  = fmin(red,255.0);
}

__global__ void highQualityLineraInterpolation_by4(uchar4 * outData, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;

    // take every four pixels

    // G R
    // B G

    if(x >= width - 1 || y >= height - 1 || x < 1 || y < 1) {
        return;
    }

    // G [0,0]
    float red = (((float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y + 2))/2.0f
            - 1.0f*((float)tex2D(texInputDataRefHqli,x-2,y) + (float)tex2D(texInputDataRefHqli,x+2,y) + (float)tex2D(texInputDataRefHqli,x-1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y-1) + (float)tex2D(texInputDataRefHqli,x-1,y+1) + (float)tex2D(texInputDataRefHqli,x+1,y+1))
            + 5.0f*(float)tex2D(texInputDataRefHqli,x,y)
            + 4.0f*((float)tex2D(texInputDataRefHqli,x-1,y) + (float)tex2D(texInputDataRefHqli,x+1,y)))/8.0f;

    float blue = (((float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y + 2))/2.0f
            - 1.0f*((float)tex2D(texInputDataRefHqli,x-2,y) + (float)tex2D(texInputDataRefHqli,x+2,y) + (float)tex2D(texInputDataRefHqli,x-1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y-1) + (float)tex2D(texInputDataRefHqli,x-1,y+1) + (float)tex2D(texInputDataRefHqli,x+1,y+1))
            + 5.0f*(float)tex2D(texInputDataRefHqli,x,y)
            + 4.0f*( outData[x - 1 + y*width].z+ outData[x + 1 + y*width].z)) / 8.0f;

    float green = tex2D(texInputDataRefHqli,x,y);

    uchar4 pixel;

    pixel.x = fminf(red,colorMax);
    pixel.y = fminf(green,colorMax);
    pixel.z = blue;

    outData[x + y*width] = pixel;

    x++;

    // R [1,0]
    red = tex2D(texInputDataRefHqli,x,y);

    green = (-1.0f*((float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y + 2) + (float)tex2D(texInputDataRefHqli,x + 2,y) + (float)tex2D(texInputDataRefHqli,x - 2,y))
             + 2.0f*((float)tex2D(texInputDataRefHqli,x-1,y) + (float)tex2D(texInputDataRefHqli,x+1,y) + (float)tex2D(texInputDataRefHqli,x,y+1) + (float)tex2D(texInputDataRefHqli,x,y-1)) + 4.0f*(float)tex2D(texInputDataRefHqli,x,y) ) / 8.0f;

    pixel.x = red;
    pixel.y = fminf(green,colorMax);
    pixel.z = outData[x + y*width].z;

    outData[x + y*width] = pixel;

    x--;
    y++;

    // B [0,1]

    blue = tex2D(texInputDataRefHqli,x,y);

    green = (-1.0f*((float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y + 2) + (float)tex2D(texInputDataRefHqli,x + 2,y) + (float)tex2D(texInputDataRefHqli,x - 2,y))
             + 2.0f*((float)tex2D(texInputDataRefHqli,x-1,y) + (float)tex2D(texInputDataRefHqli,x+1,y) + (float)tex2D(texInputDataRefHqli,x,y-1) + (float)tex2D(texInputDataRefHqli,x,y+1))
             + 4.0f*(float)tex2D(texInputDataRefHqli,x,y))/8.0f;


    pixel.x = outData[x + y*width].x;
    pixel.y = green;
    pixel.z = blue;

    outData[x + y*width] = pixel;

    x++;

    // G [1,1]
    red = (((float)tex2D(texInputDataRefHqli,x-2,y) + (float)tex2D(texInputDataRefHqli,x+2,y))/2.0f
           - 1.0f*((float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y+2) + (float)tex2D(texInputDataRefHqli,x-1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y+1) + (float)tex2D(texInputDataRefHqli,x-1,y+1))
           + 5.0f*(float)tex2D(texInputDataRefHqli,x,y)
           + 4.0f*((float)tex2D(texInputDataRefHqli,x,y+1) + (float)tex2D(texInputDataRefHqli,x,y-1))) / 8.0f;

    blue = (((float)tex2D(texInputDataRefHqli,x-2,y) + (float)tex2D(texInputDataRefHqli,x+2,y))/2.0f
           - 1.0f*((float)tex2D(texInputDataRefHqli,x,y-2) + (float)tex2D(texInputDataRefHqli,x,y+2) + (float)tex2D(texInputDataRefHqli,x-1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y-1) + (float)tex2D(texInputDataRefHqli,x+1,y+1) + (float)tex2D(texInputDataRefHqli,x-1,y+1))
           + 5.0f*(float)tex2D(texInputDataRefHqli,x,y)
           + 4.0f*(outData[x + (y-1)*width].z + outData[x + (y+1)*width].z)) / 8.0f;

    green = tex2D(texInputDataRefHqli,x,y);


    pixel.x = fminf(red,colorMax);
    pixel.y = fminf(green,colorMax);
    pixel.z = blue;

    outData[x + y*width] = pixel;
}



extern "C"
__host__ void highQualityLineraInterpolation_init(uchar4 **device_outData,
                                                  uchar3 **device_outData_final,
                                                  uchar3 **host_outData,
                                                  unsigned char ** host_inData,
                                                  cudaArray_t * texArray,
                                                  int width,
                                                  int height,
                                                  cudaStream_t * stream,
                                                  cudaEvent_t * start,
                                                  cudaEvent_t * stop)
{

    CUDA_SAFE_CALL(cudaStreamCreate(stream));

    cudaEventCreate(start);
    cudaEventCreate(stop);

    CUDA_SAFE_CALL(cudaMallocHost(host_outData,width*height*sizeof(uchar3)));
    CUDA_SAFE_CALL(cudaMallocHost(host_inData,width*height));

    CUDA_SAFE_CALL(cudaMalloc(device_outData, width*height*sizeof(uchar4)));
    CUDA_SAFE_CALL(cudaMalloc(device_outData_final, width*height*sizeof(uchar3)));



    texInputDataRefHqli.addressMode[0] = cudaAddressModeClamp;
    texInputDataRefHqli.addressMode[1] = cudaAddressModeClamp;
    texInputDataRefHqli.filterMode = cudaFilterModePoint;    // souradnice jsou brany primo (neni potreba udavat stred pixelu)
    texInputDataRefHqli.normalized = false;

    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();

    CUDA_SAFE_CALL(cudaMallocArray(texArray, &channelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texInputDataRefHqli, *texArray, channelDesc));

}


extern "C"
__host__ void highQualityLineraInterpolation_process(uchar4 * device_outData,
                                                     uchar3 * device_outData_final,
                                                     uchar3 *host_outData,
                                                     unsigned char * host_inData,
                                                     cudaArray_t texArray,
                                                     int width,
                                                     int height,
                                                     cudaStream_t stream,
                                                     cudaEvent_t start, cudaEvent_t stop)
{
    dim3 threadsPerBlock(32, 8);

    dim3 numBlocks(ceil((double)width/(double)threadsPerBlock.x/2.0), ceil((double)height/(double)threadsPerBlock.y/2.0));


    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texArray, 0, 0, host_inData, width*height, cudaMemcpyHostToDevice,stream));

    cudaEventRecord(start,stream);
    highQualityLineraInterpolationBy4_R_B_at_B_R_by4<<<numBlocks,threadsPerBlock,0,stream>>>(device_outData,width,height);

    highQualityLineraInterpolation_by4<<<numBlocks,threadsPerBlock,0,stream>>>(device_outData,width,height);

    convertUchar4to3<<<numBlocks,threadsPerBlock,0,stream>>>(device_outData,device_outData_final,width,height);
    cudaEventRecord(stop,stream);


    CUDA_SAFE_CALL(cudaMemcpyAsync(host_outData, device_outData_final, width*height*sizeof(uchar3), cudaMemcpyDeviceToHost,stream));
}


extern "C"
__host__ void highQualityLineraInterpolation_destroy(uchar4 *device_outData,
                                                     uchar3 *device_outData_final,
                                                     uchar3 *host_outData,
                                                     unsigned char * host_inData,
                                                     cudaArray_t texArray,
                                                     cudaStream_t stream, cudaEvent_t start, cudaEvent_t stop)
{
    CUDA_SAFE_CALL(cudaUnbindTexture(texInputDataRefHqli));
    CUDA_SAFE_CALL(cudaFreeArray (texArray));

    CUDA_SAFE_CALL(cudaFree(device_outData));
    CUDA_SAFE_CALL(cudaFree(device_outData_final));

    CUDA_SAFE_CALL(cudaFreeHost(host_outData));
    CUDA_SAFE_CALL(cudaFreeHost(host_inData));

    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    cudaStreamDestroy(stream);
}
