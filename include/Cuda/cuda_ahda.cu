﻿#include "cuda_code.cuh"
#include <math.h>


texture<unsigned char, 2, cudaReadModeElementType> texRefInputNew;

texture<float4, 2, cudaReadModeElementType> texCielabHorizontalRef;
texture<float4, 2, cudaReadModeElementType> texCielabVerticalRef;

texture<unsigned char, 2, cudaReadModeElementType> texHomogeneityVerticalRef;
texture<unsigned char, 2, cudaReadModeElementType> texHomogeneityHorizontalRef;

__device__ int d_PixelCount = 0;

__device__ float colorMax = 255.0f;


#define MASK    2
#define MASK3x3 4
#define MASK5x5 8


#define CMP_SWAP(a,b) { if ((a) > (b)){float temp = (a); (a)=(b);(b)=temp;}}

__device__ float medianFrom9items(float * diffs)
{
    // FPGA Implementation of Median Filter using an Improved Algorithm for Image Processing - figure 4
    // http://www.ijetae.com/files/Volume2Issue8/IJETAE_0812_38.pdf

    CMP_SWAP(diffs[0],diffs[1]);CMP_SWAP(diffs[3],diffs[4]);CMP_SWAP(diffs[6],diffs[7]);

    CMP_SWAP(diffs[1],diffs[2]);CMP_SWAP(diffs[4],diffs[5]);CMP_SWAP(diffs[7],diffs[8]);

    CMP_SWAP(diffs[0],diffs[1]);CMP_SWAP(diffs[3],diffs[4]);CMP_SWAP(diffs[6],diffs[7]);

    CMP_SWAP(diffs[0],diffs[3]);CMP_SWAP(diffs[1],diffs[4]);CMP_SWAP(diffs[5],diffs[8]);

    CMP_SWAP(diffs[3],diffs[6]);CMP_SWAP(diffs[4],diffs[7]);CMP_SWAP(diffs[2],diffs[5]);

    CMP_SWAP(diffs[1],diffs[4]);

    CMP_SWAP(diffs[6],diffs[4]);

    CMP_SWAP(diffs[4],diffs[2]);

    CMP_SWAP(diffs[6],diffs[4]);

    // median in diffs[4]
    return diffs[4];
}

__device__ float mediaDiff_RED_GREEN(int x, int y,int width, uchar4 * result)
{
    float diffs[9];
    int i = 0;

#pragma unroll
    for (int dy = -1; dy <= 1; dy++) {
        for (int dx = -1; dx <= 1; dx++) {
            diffs[i++] = ((float)result[(x + dx + (y+dy)*width)].x - (float)result[(x + dx + (y+dy)*width)].y);
        }
    }
    return medianFrom9items(diffs);
}

__device__ float mediaDiff_BLUE_GREEN(int x, int y,int width, uchar4 * result)
{
    float diffs[9];
    int i = 0;

#pragma unroll
    for (int dy = -1; dy <= 1; dy++) {
        for (int dx = -1; dx <= 1; dx++) {
            diffs[i++] = ((float)result[(x + dx + (y+dy)*width)].z - (float)result[(x + dx + (y+dy)*width)].y);
        }
    }
    return medianFrom9items(diffs);
}

__device__ float mediaDiff_GREEN_RED(int x, int y,int width, uchar4 * result)
{
    float diffs[9];
    int i = 0;

#pragma unroll
    for (int dy = -1; dy <= 1; dy++) {
        for (int dx = -1; dx <= 1; dx++) {
            diffs[i++] = ((float)result[(x + dx + (y+dy)*width)].y - (float)result[(x + dx + (y+dy)*width)].x);
        }
    }
    return medianFrom9items(diffs);
}

__device__ float mediaDiff_GREEN_BLUE(int x, int y,int width, uchar4 * result)
{
    float diffs[9];
    int i = 0;

#pragma unroll
    for (int dy = -1; dy <= 1; dy++) {
        for (int dx = -1; dx <= 1; dx++) {
            diffs[i++] = ((float)result[(x + dx + (y+dy)*width)].y - (float)result[(x + dx + (y+dy)*width)].z);
        }
    }
    return medianFrom9items(diffs);
}





//           _    _ _____                     ______ _____   _____ ______
//     /\   | |  | |  __ \   /\              |  ____|  __ \ / ____|  ____|
//    /  \  | |__| | |  | | /  \     ______  | |__  | |  | | |  __| |__
//   / /\ \ |  __  | |  | |/ /\ \   |______| |  __| | |  | | | |_ |  __|
//  / ____ \| |  | | |__| / ____ \           | |____| |__| | |__| | |____
// /_/    \_\_|  |_|_____/_/    \_\          |______|_____/ \_____|______|
// adaptive homogenity demosaicing algorithm with edge modification

__global__ void bilinearDebayer_firstStep_new(uchar4 * device_outputData, float * grayscale, int width)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x *=2;
    y *=2;


    d_PixelCount = 0;


    // take every four pixels

    // G R
    // B G


    //    uchar3 pixel;
    //    pixel.x = 255;
    //    pixel.y = 255;
    //    pixel.z = 255;

    // G
    float r = (tex2D(texRefInputNew,x-1,y) + tex2D(texRefInputNew,x+1,y))/2.0f;
    float g = tex2D(texRefInputNew,x,y);
    float b = (tex2D(texRefInputNew,x,y-1) + tex2D(texRefInputNew,x,y+1))/2.0f;

    uchar4 pixel;
    pixel.x = r;
    pixel.y = g;
    pixel.z = b;
    pixel.w = 255;

    device_outputData[(x + y*width)] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;



    // R
    x+=1;

    r = tex2D(texRefInputNew,x,y);
    g = (tex2D(texRefInputNew,x+1,y) + tex2D(texRefInputNew,x-1,y) + tex2D(texRefInputNew,x,y+1) + tex2D(texRefInputNew,x,y-1))/4.0f;
    b = (tex2D(texRefInputNew,x+1,y+1) + tex2D(texRefInputNew,x-1,y-1) + tex2D(texRefInputNew,x+1,y-1) + tex2D(texRefInputNew,x-1,y+1))/4.0f;

    pixel.x = r;
    pixel.y = g;
    pixel.z = b;

    device_outputData[(x + y*width)] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;

    // B
    x-=1;
    y+=1;

    r = (tex2D(texRefInputNew,x+1,y+1) + tex2D(texRefInputNew,x-1,y-1) + tex2D(texRefInputNew,x+1,y-1) + tex2D(texRefInputNew,x-1,y+1))/4.0f;
    g = (tex2D(texRefInputNew,x+1,y) + tex2D(texRefInputNew,x-1,y) + tex2D(texRefInputNew,x,y+1) + tex2D(texRefInputNew,x,y-1))/4.0f;
    b = tex2D(texRefInputNew,x,y);

    pixel.x = r;
    pixel.y = g;
    pixel.z = b;

    device_outputData[(x + y*width)] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;

    // G
    x+=1;

    r = (tex2D(texRefInputNew,x,y-1) + tex2D(texRefInputNew,x,y+1))/2.0f;
    g = tex2D(texRefInputNew,x,y);
    b = (tex2D(texRefInputNew,x-1,y) + tex2D(texRefInputNew,x+1,y))/2.0f;

    pixel.x = r;
    pixel.y = g;
    pixel.z = b;

    device_outputData[(x + y*width)] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;
}



__global__ void ahda_interpolateCombined_mask(uchar4 * vertical,uchar4 * horizontal, int width, int height, unsigned char * mask)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;


    if(x <= 2 || y <= 2 || x >= width -2 || y>= height - 2)
        return;

    uchar4 pixel;
    pixel.w = 255;

    bool G1pixel = mask[x + width*y] & MASK3x3;
    bool Rpixel  = mask[x + 1 + width*y] & MASK3x3;
    bool Bpixel  = mask[x + width*(y + 1)] & MASK3x3;
    bool G2pixel = mask[x + 1 + width*(y + 1)] & MASK3x3;



    if(G1pixel)
    {
        // [0,0] G
        vertical  [x + y*width].y = tex2D(texRefInputNew,x,y);
        horizontal[x + y*width].y = tex2D(texRefInputNew,x,y);
    }
    x+=1;

    if(Rpixel)
    {
        // [1,0] R

        pixel.x = tex2D(texRefInputNew,x,y);
        pixel.y = fminf(((float)tex2D(texRefInputNew,x,y - 1) + (float)tex2D(texRefInputNew,x,y + 1))/2.0f + ((float)2.0f*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x,y - 2) - (float)tex2D(texRefInputNew,x,y + 2))/4.0f,colorMax);
        pixel.z = 0;
        horizontal[x + y*width] = pixel;

        pixel.x = tex2D(texRefInputNew,x,y);
        pixel.y = fminf(((float)tex2D(texRefInputNew,x - 1,y) + (float)tex2D(texRefInputNew,x + 1,y))/2.0f + ((float)2.0f*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x - 2,y) - (float)tex2D(texRefInputNew,x + 2,y))/4.0f,colorMax);
        vertical[x + y*width] = pixel;
    }

    // [0,1] B
    x-=1;
    y+=1;
    if(Bpixel)
    {
        pixel.x = 0;
        pixel.y = fminf(((float)tex2D(texRefInputNew,x,y - 1) + (float)tex2D(texRefInputNew,x,y + 1))/2.0f + ((float)2.0*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x,y - 2) - (float)tex2D(texRefInputNew,x,y + 2))/4.0f,colorMax);
        pixel.z = tex2D(texRefInputNew,x,y);
        horizontal[x + y*width]  = pixel;

        pixel.x = 0;
        pixel.y = fminf(((float)tex2D(texRefInputNew,x - 1,y) + (float)tex2D(texRefInputNew,x + 1,y))/2.0f + ((float)2.0*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x - 2,y) - (float)tex2D(texRefInputNew,x + 2,y))/4.0f,colorMax);
        pixel.z = tex2D(texRefInputNew,x,y);
        vertical[x + y*width]  = pixel;
    }
    // [1,1] G
    x+=1;
    if(G2pixel)
    {
        vertical[x + y*width].y   = tex2D(texRefInputNew,x,y);
        horizontal[x + y*width].y = tex2D(texRefInputNew,x,y);
    }

    //////////////////////////////////////////////////////////////////////
    __syncthreads();

    y-=1;


    // [1,0] R - calculate B

    if(Rpixel)
    {
        horizontal[x + y*width].z = fminf(
                    ((float)horizontal[x+1 + (y+1)*width].z + (float)horizontal[x-1 + (y+1)*width].z + (float)horizontal[x+1 + (y-1)*width].z +(float)horizontal[x-1 + (y-1)*width].z)/4.0f,colorMax);

        vertical[x + y*width].z = fminf(
                    ((float)vertical[x+1 + (y+1)*width].z + (float)vertical[x-1 + (y+1)*width].z + (float)vertical[x+1 + (y-1)*width].z + (float)vertical[x-1 + (y-1)*width].z)/4.0f,colorMax);
    }
    // [0,1] B - calculate R
    x-=1;
    y+=1;

    if(Bpixel)
    {
        horizontal[x + y*width].x = fminf(
                    ((float)horizontal[x+1  + (y+1)*width].x +(float)horizontal[x-1  + (y+1)*width].x +(float)horizontal[x+1  + (y-1)*width].x +(float)horizontal[x-1  + (y-1)*width].x)/4.0f,colorMax);

        vertical[x + y*width].x = fminf(
                    ((float)vertical[x+1 + (y+1)*width].x +(float)vertical[x-1 + (y+1)*width].x +(float)vertical[x+1 + (y-1)*width].x +(float)vertical[x-1 + (y-1)*width].x)/4.0f,colorMax);
    }

    //////////////////////////////////////////////////////////////////////
    __syncthreads();
    y-=1;



    // [0,0] - G

    if(G1pixel)
    {
        // R
        horizontal[x + y*width].x = fminf(
                    ((float)horizontal[x+1 + (y)*width].x +
                    (float)horizontal[x + (y+1)*width].x +
                (float)horizontal[x-1 + (y)*width].x +
                (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

        vertical[x + y*width].x = fminf(
                    ((float)vertical[x+1 + (y)*width].x +
                    (float)vertical[x + (y+1)*width].x +
                (float)vertical[x-1 + (y)*width].x +
                (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);
    }
    // B
    if(Bpixel)
    {
        horizontal[x + y*width].z = fminf(
                    ((float)horizontal[x+1 + (y)*width].z +
                    (float)horizontal[x + (y+1)*width].z +
                (float)horizontal[x-1 + (y)*width].z +
                (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

        vertical[x + y*width].z = fminf(
                    ((float)vertical[x+1 + (y)*width].z +
                    (float)vertical[x + (y+1)*width].z +
                (float)vertical[x-1 + (y)*width].z +
                (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);
    }

    // [1,1] - G
    x+=1;
    y+=1;

    if(G2pixel)
    {
        // R
        horizontal[x + y*width].x = fminf(
                    ((float)horizontal[x+1 + (y)*width].x +
                    (float)horizontal[x + (y+1)*width].x +
                (float)horizontal[x-1 + (y)*width].x +
                (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

        vertical[x + y*width].x = fminf(
                    ((float)vertical[x+1 + (y)*width].x +
                    (float)vertical[x + (y+1)*width].x +
                (float)vertical[x-1 + (y)*width].x +
                (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);
        // B
        horizontal[x + y*width].z = fminf(
                    ((float)horizontal[x+1 + (y)*width].z +
                    (float)horizontal[x + (y+1)*width].z +
                (float)horizontal[x-1 + (y)*width].z +
                (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

        vertical[x + y*width].z = fminf(
                    ((float)vertical[x+1 + (y)*width].z +
                    (float)vertical[x + (y+1)*width].z +
                (float)vertical[x-1 + (y)*width].z +
                (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);
    }
    //////////////////////////////////////////////////////////////////////
    __syncthreads();

    x-=1;
    y-=1;


    // [0,0] - G

    if(G1pixel)
    {
        // R
        horizontal[x + y*width].x = fminf(
                    ((float)horizontal[x+1 + (y)*width].x +
                    (float)horizontal[x + (y+1)*width].x +
                (float)horizontal[x-1 + (y)*width].x +
                (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

        vertical[x + y*width].x = fminf(
                    ((float)vertical[x+1 + (y)*width].x +
                    (float)vertical[x + (y+1)*width].x +
                (float)vertical[x-1 + (y)*width].x +
                (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);

        // B
        horizontal[x + y*width].z = fminf(
                    ((float)horizontal[x+1 + (y)*width].z +
                    (float)horizontal[x + (y+1)*width].z +
                (float)horizontal[x-1 + (y)*width].z +
                (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

        vertical[x + y*width].z = fminf(
                    ((float)vertical[x+1 + (y)*width].z +
                    (float)vertical[x + (y+1)*width].z +
                (float)vertical[x-1 + (y)*width].z +
                (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);
    }

    x+=1;
    y+=1;

    // [1,1] - G
    if(G2pixel)
    {
        // R
        horizontal[x + y*width].x = fminf(
                    ((float)horizontal[x+1 + (y)*width].x +
                    (float)horizontal[x + (y+1)*width].x +
                (float)horizontal[x-1 + (y)*width].x +
                (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

        vertical[x + y*width].x = fminf(
                    ((float)vertical[x+1 + (y)*width].x +
                    (float)vertical[x + (y+1)*width].x +
                (float)vertical[x-1 + (y)*width].x +
                (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);
        // B
        horizontal[x + y*width].z = fminf(
                    ((float)horizontal[x+1 + (y)*width].z +
                    (float)horizontal[x + (y+1)*width].z +
                (float)horizontal[x-1 + (y)*width].z +
                (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

        vertical[x + y*width].z = fminf(
                    ((float)vertical[x+1 + (y)*width].z +
                    (float)vertical[x + (y+1)*width].z +
                (float)vertical[x-1 + (y)*width].z +
                (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);
    }
}



__device__ unsigned char  sobelOperator(
        unsigned char ul, // upper left
        unsigned char um, // upper middle
        unsigned char ur, // upper right
        unsigned char ml, // middle left
        unsigned char mr, // middle right
        unsigned char ll, // lower left
        unsigned char lm, // lower middle
        unsigned char lr  // lower right
        )
{
    int horizontal = (int)ul + 2*(int)ml + (int)ll - (int)ur - 2*(int)mr - (int)lr;
    int vertical =   (int)ll + 2*(int)lm + (int)lr -(int)ul - 2*(int)um - (int)ur;
    unsigned char sum = 0;

    if((abs(horizontal)+abs(vertical)) > 40) {
        sum = MASK;
        atomicAdd(&d_PixelCount,1);
    }

    return sum;
}

__global__ void bilinearDebayer_edgeDetection_combined(float * grayscale,unsigned char * maskORIG, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;


    if(x < 2 || y < 2 || x >= width -2 || y >= height -2) {
        return;
    }

    maskORIG[x + y*width] = sobelOperator(grayscale[x -1 + (y-1)*width],grayscale[x + (y-1)*width],grayscale[x + 1 + (y-1)*width],
            grayscale[x - 1 + (y)*width],grayscale[x +1 + (y)*width],
            grayscale[x - 1 + (y+1)*width],grayscale[x + (y+1)*width],grayscale[x + 1 + (y+1)*width]);


    //    if(x >= width || y >= height)
    //        return;


    //    unsigned char flag = 0;
    //    if(!(x < 2 || y < 2 || x >= width -2 || y >= height -2)) {
    //        flag = sobel_new(grayscale[x -1 + (y-1)*width],grayscale[x + (y-1)*width],grayscale[x + 1 + (y-1)*width],
    //                grayscale[x - 1 + (y)*width],grayscale[x +1 + (y)*width],
    //                grayscale[x - 1 + (y+1)*width],grayscale[x + (y+1)*width],grayscale[x + 1 + (y+1)*width]);
    //    }

    //    maskORIG[x + y*width] = flag;

    //    if(x < 2 || y < 2 || x >= width -2 || y >= height -2)
    //        return;


    //    sobel_new(grayscale[x -1 + (y-1)*width],grayscale[x + (y-1)*width],grayscale[x +1 + (y-1)*width],
    //            grayscale[x -1 + (y)*width],grayscale[x +1 + (y)*width],
    //            grayscale[x -1 + (y+1)*width],grayscale[x + (y+1)*width],grayscale[x +1 + (y+1)*width]);

    ///////////////////////////////////////////////////////////
    __syncthreads();


    bool founded = false;

    unsigned char newValue = 0;

#pragma unroll
    for (int dX = -2; dX <= 2; dX++)
        for (int dY = -2; dY <= 2; dY++)
        {
            founded = false;

            if(maskORIG[x + dX + (y+dY)*width] & MASK) {
                newValue |= MASK5x5;
                founded = true;
            }

            if(founded && dX >= -1 && dX <= 1 &&  dY >= -1 && dY <= 1) {
                newValue |= MASK3x3;
            }
        }

    maskORIG[x + y*width] = newValue;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__device__ float fX(float input)
{   
    return input > 0.008856f ? input : 7.787f*input + 0.13793f;   //  0.13793 = 16.0/116.0
}

__device__ void convertOnePixelToCielab(uint x, uint y, uchar4 * rgbSource, float4 * targetCielab, int width)
{
    uchar4 pixel = rgbSource[x + y*width];

    float red   = pixel.x;
    float green = pixel.y;
    float blue  = pixel.z;

    float X = (red*0.49f + green*0.31f + blue*0.2f)/0.17697f;
    float Y = (red*0.17697f + green*0.8124f + blue*0.01063f)/0.17697f;
    float Z = (green*0.01f + blue*0.99f)/0.17697f;

    float4 cielabPixel;
    cielabPixel.x = Y/100.0f > 0.008856f ? 116.0f*powf(Y/100.0f,1/3) - 16.0f : 903.3f*(Y/100.0f);  // L
    cielabPixel.y = 500.0f*(powf(fX(X/95.047f),1/3) - powf(fX(Y/100.0f),1/3));       // a
    cielabPixel.z = 200.0f*(powf(fX(Y/100.0f),1/3) - powf(fX(Z/108.883f),1/3));      // b

    targetCielab[x + y*width] = cielabPixel;
}



__global__ void convertToCielab(uchar4 * vertical,uchar4 * horizontal,float4 * cielabHorizontal,float4 * cielabVertical,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x >= width || y >= height)
        return;

    convertOnePixelToCielab(x, y, vertical,   cielabVertical,   width);
    convertOnePixelToCielab(x, y, horizontal, cielabHorizontal, width);
}


__global__ void convertToCielab_mask(uchar4 * vertical,uchar4 * horizontal,float4 * cielabHorizontal,float4 * cielabVertical,int width, int height, unsigned char * mask)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x >= width || y >= height)
        return;

    if(!(mask[x + width*y] & MASK5x5))
        return;

    convertOnePixelToCielab(x, y, vertical,   cielabVertical,   width);
    convertOnePixelToCielab(x, y, horizontal, cielabHorizontal, width);
}


__device__ float d_L(float x_L, float y_L)
{
    return fabsf(x_L - y_L);
}

__device__ float d_ab(float x_a, float y_a,float x_b, float y_b)
{
    return rsqrtf( __powf(x_a - y_a,2) + __powf(x_b - y_b,2));
}

__device__ void calculateHomogenity_private(uint x, uint y, unsigned char * homogeneityVertical,unsigned char * homogeneityHorizontal,int width)
{
    float4 horizontalX_Y   = tex2D(texCielabHorizontalRef,x,y);
    float4 horizontalX_Y1  = tex2D(texCielabHorizontalRef,x,y+1);
    float4 horizontalX_1Y  = tex2D(texCielabHorizontalRef,x,y-1);


    float4 verticalX_Y  = tex2D(texCielabVerticalRef,x,y);
    float4 verticalX1_Y = tex2D(texCielabVerticalRef,x+1,y);
    float4 vertical1X_Y  = tex2D(texCielabVerticalRef,x-1,y);



    float treshold_L = fmin(fmax(d_L(horizontalX_Y.x,horizontalX_1Y.x),
                                 d_L(horizontalX_Y.x,horizontalX_Y1.x)),
                            fmax(d_L(verticalX_Y.x,vertical1X_Y.x),
                                 d_L(verticalX_Y.x,verticalX1_Y.x)));


    float treshold_ab = fmin(fmax(d_ab(horizontalX_Y.y,horizontalX_1Y.y,horizontalX_Y.z,horizontalX_1Y.z),
                                  d_ab(horizontalX_Y.y,horizontalX_Y1.y,horizontalX_Y.z,horizontalX_Y1.z)),
                             fmax(d_ab(verticalX_Y.y,vertical1X_Y.y,verticalX1_Y.z,vertical1X_Y.z),
                                  d_ab(verticalX_Y.y,verticalX1_Y.y,verticalX1_Y.z,verticalX1_Y.z)));

    int homogeneityTempH = 0;
    int homogeneityTempV = 0;

#pragma unroll

    for (int x_H = -1; x_H < 2; ++x_H)
    {
        for (int y_H = -1; y_H < 2; ++y_H)
        {
            // except [0,0]
            if(x_H != 0 && y_H != 0)
            {
                if(d_L(horizontalX_Y.x,((float4)tex2D(texCielabHorizontalRef,x + x_H,y + y_H)).x) <= treshold_L

                        &&

                        d_ab(horizontalX_Y.y,((float4)tex2D(texCielabHorizontalRef,x+x_H+1,y+y_H)).y,
                                 horizontalX_Y.z,((float4)tex2D(texCielabHorizontalRef,x+x_H+2,y+y_H)).z) <= treshold_ab )
                {
                    homogeneityTempH++;
                }

                if(d_L(verticalX_Y.x,((float4)tex2D(texCielabVerticalRef,x + x_H,y + y_H)).x) <= treshold_L

                        &&

                        d_ab(verticalX1_Y.y,((float4)tex2D(texCielabVerticalRef,x+x_H+1,y+y_H)).y,
                                 verticalX_Y.z,((float4)tex2D(texCielabVerticalRef,x+x_H+2,y+y_H)).z) <= treshold_ab )
                {
                    homogeneityTempV++;
                }
            }
        }
    }

    homogeneityHorizontal[(x + y*width)*sizeof(uchar3)] = homogeneityTempH;
    homogeneityVertical[(x + y*width)*sizeof(uchar3)]   = homogeneityTempV;
}




__global__ void calculateHomogenity(unsigned char * homogeneityVertical,unsigned char * homogeneityHorizontal,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x < 1 || y < 1 || x >= width - 1  || y >= height - 1)
        return;

    calculateHomogenity_private(x,y,homogeneityVertical, homogeneityHorizontal,width);
}

__global__ void calculateHomogenity_mask(unsigned char * homogeneityVertical,unsigned char * homogeneityHorizontal,int width,int height, unsigned char * mask)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x < 1 || y < 1 || x >= width - 1  || y >= height - 1)
        return;

    if(!(mask[x + width*y] & MASK3x3))
        return;

    calculateHomogenity_private(x,y,homogeneityVertical, homogeneityHorizontal,width);
}

__device__ void chooseDirection_private(uint x, uint y ,uchar4 * result,uchar4 * vertical,uchar4 * horizontal,int width)
{
    float horizontalAverage = 0;
    float verticalAverage = 0;

    // 3x3 neighbourhood - manual
    verticalAverage += tex2D(texHomogeneityVerticalRef,x-1,y-1) + tex2D(texHomogeneityVerticalRef,x,y-1) +
            tex2D(texHomogeneityVerticalRef,x+1,y-1) + tex2D(texHomogeneityVerticalRef,x-1,y) + tex2D(texHomogeneityVerticalRef,x,y) +
            tex2D(texHomogeneityVerticalRef,x+1,y) + tex2D(texHomogeneityVerticalRef,x-1,y+1) + tex2D(texHomogeneityVerticalRef,x,y+1) +
            tex2D(texHomogeneityVerticalRef,x+1,y+1);


    horizontalAverage += tex2D(texHomogeneityHorizontalRef,x-1,y-1) + tex2D(texHomogeneityHorizontalRef,x,y-1) +
            tex2D(texHomogeneityHorizontalRef,x+1,y-1) + tex2D(texHomogeneityHorizontalRef,x-1,y) + tex2D(texHomogeneityHorizontalRef,x,y) +
            tex2D(texHomogeneityHorizontalRef,x+1,y) + tex2D(texHomogeneityHorizontalRef,x-1,y+1) + tex2D(texHomogeneityHorizontalRef,x,y+1) +
            tex2D(texHomogeneityHorizontalRef,x+1,y+1);


    // use horizontal
    uchar4 pixelV = vertical[x + y*width];
    uchar4 pixelH = horizontal[x + y*width];

    result[x + y*width] = (horizontalAverage/9.0f > verticalAverage/9.0f) ? pixelV : pixelH;


}

__global__ void chooseDirection(uchar4 * result,uchar4 * vertical,uchar4 * horizontal,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x < 2 || y < 2 || x >= width - 2 || y >= height - 2)
        return;

    chooseDirection_private( x,  y , result, vertical, horizontal,width);
}

__global__ void chooseDirection_mask(uchar4 * result,uchar4 * vertical,uchar4 * horizontal,int width,int height, unsigned char * mask)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x < 2 || y < 2 || x >= width - 2 || y >= height - 2)
        return;

    if(!(mask[x + width*y] & MASK))
        return;

    chooseDirection_private( x,  y , result, vertical, horizontal,width);
}

__device__ void removeArtifacts_private(uint x, uint y, uchar4 * result, int width)
{
    // http://elynxsdk.free.fr/ext-docs/Demosaicing/more/news0/Color%20Filter%20Array%20Demosaicking-New%20Method%20and%20Performance%20Measures.pdf


    float medianRED_GREEN = mediaDiff_RED_GREEN(x,y,width,result);
    float medianBLUE_GREEN = mediaDiff_BLUE_GREEN(x,y,width,result);

    float newGreen = fminf(((float)result[(x + y*width)].x - medianRED_GREEN +  (float)result[(x + y*width)].z - medianBLUE_GREEN)/2.0f,colorMax);
    float newRed   = fminf(medianRED_GREEN + newGreen,colorMax);
    float newBlue  = fminf(medianBLUE_GREEN + newGreen,colorMax);

    uchar4 pixel;
    pixel.x = newRed;
    pixel.y = newGreen;
    pixel.z = newBlue;
    pixel.w = 255;

    result[(x + y*width)] = pixel;
}



__global__ void removeArtifacts(uchar4 * result, int width,int height)
{

    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(x <= 1 || y <= 1 || x >= width - 1  || y >= height - 1)
        return;

    removeArtifacts_private(x,y,result, width);
}

__global__ void removeArtifacts_mask(uchar4 * result, unsigned char * mask,int width,int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    if(!(mask[x + y*width] & MASK3x3) && !(x <= 1 || y <= 1 || x >= width - 1  || y >= height - 1))
        return;

    removeArtifacts_private(x,y,result, width);
}


extern "C"
__host__ void cuda_ahda_init_new(cudaArray_t *inputTexArray,

                                cudaArray **texCielabHorizontalArray,
                                cudaArray **texCielabVerticalArray,

                                float4 ** cielabHorizontal,
                                float4 ** cielabVertical,


                                cudaArray **texHomogeneityHorizontalArray,
                                cudaArray **texHomogeneityVerticalArray,

                                unsigned char ** homogeneityHorizontal,
                                unsigned char ** homogeneityVertical,

                                uchar4 ** horizontal,
                                uchar4 ** vertical,

                                uchar4 ** device_output,
                                uchar3 ** device_output_final,
                                uchar3 ** host_output,
                                unsigned char **host_input,
                                int width,
                                int height,
                                cudaStream_t * stream, cudaEvent_t *start, cudaEvent_t *stop,
                                unsigned char **mask,
                                float ** grayscale)
{

    CUDA_SAFE_CALL(cudaStreamCreate(stream));

    CUDA_SAFE_CALL(cudaEventCreate(start));
    CUDA_SAFE_CALL(cudaEventCreate(stop));


    CUDA_SAFE_CALL(cudaMalloc(mask, width*height*sizeof(unsigned char)));
    CUDA_SAFE_CALL(cudaMalloc(grayscale, width*height*sizeof(float)));



    // INPUT DATA

    CUDA_SAFE_CALL(cudaMallocHost(host_input, width*height));

    texRefInputNew.addressMode[0] = cudaAddressModeClamp;
    texRefInputNew.addressMode[1] = cudaAddressModeClamp;
    texRefInputNew.filterMode = cudaFilterModePoint;
    texRefInputNew.normalized = false;

    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<unsigned char>();
    CUDA_SAFE_CALL(cudaMallocArray(inputTexArray, &channelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texRefInputNew, *inputTexArray, channelDesc));


    // CIELAB
    CUDA_SAFE_CALL(cudaMalloc(cielabHorizontal, width*height*sizeof(float4)));
    CUDA_SAFE_CALL(cudaMalloc(cielabVertical,   width*height*sizeof(float4)));

    static cudaChannelFormatDesc cielabChannelDesc = cudaCreateChannelDesc<float4>();

    // horizontal
    texCielabHorizontalRef.addressMode[0] = cudaAddressModeClamp;
    texCielabHorizontalRef.addressMode[1] = cudaAddressModeClamp;
    texCielabHorizontalRef.filterMode = cudaFilterModePoint;
    texCielabHorizontalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texCielabHorizontalArray, &cielabChannelDesc, width*sizeof(float4), height));
    CUDA_SAFE_CALL(cudaBindTextureToArray( texCielabHorizontalRef,*texCielabHorizontalArray, cielabChannelDesc));

    // vertical
    texCielabVerticalRef.addressMode[0] = cudaAddressModeClamp;
    texCielabVerticalRef.addressMode[1] = cudaAddressModeClamp;
    texCielabVerticalRef.filterMode = cudaFilterModePoint;
    texCielabVerticalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texCielabVerticalArray, &cielabChannelDesc, width*sizeof(float4), height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texCielabVerticalRef, *texCielabVerticalArray, cielabChannelDesc));

    // HOMOGENEITY SOURCE BUFFERS
    CUDA_SAFE_CALL(cudaMalloc(homogeneityVertical, width*height*sizeof(uchar3)));
    CUDA_SAFE_CALL(cudaMalloc(homogeneityHorizontal, width*height*sizeof(uchar3)));

    // HOMOGENEITY TARGET TEXTURES
    static cudaChannelFormatDesc homogeneityChannelDesc = cudaCreateChannelDesc<unsigned char>();

    // horizontal
    texHomogeneityHorizontalRef.addressMode[0] = cudaAddressModeClamp;
    texHomogeneityHorizontalRef.addressMode[1] = cudaAddressModeClamp;
    texHomogeneityHorizontalRef.filterMode = cudaFilterModePoint;
    texHomogeneityHorizontalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texHomogeneityHorizontalArray, &homogeneityChannelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texHomogeneityHorizontalRef, *texHomogeneityHorizontalArray, homogeneityChannelDesc));

    // vertical
    texHomogeneityVerticalRef.addressMode[0] = cudaAddressModeClamp;
    texHomogeneityVerticalRef.addressMode[1] = cudaAddressModeClamp;
    texHomogeneityVerticalRef.filterMode = cudaFilterModePoint;
    texHomogeneityVerticalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texHomogeneityVerticalArray, &homogeneityChannelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texHomogeneityVerticalRef, *texHomogeneityVerticalArray, homogeneityChannelDesc));


    // INTERPOLATED IMAGES
    // horizontal
    CUDA_SAFE_CALL(cudaMalloc(horizontal, width*height*sizeof(uchar4)));
    // vertical
    CUDA_SAFE_CALL(cudaMalloc(vertical,   width*height*sizeof(uchar4)));


    // OUTPUT IMAGE
    CUDA_SAFE_CALL(cudaMalloc(device_output, width*height*sizeof(uchar4)));
    CUDA_SAFE_CALL(cudaMalloc(device_output_final, width*height*sizeof(uchar3)));
    CUDA_SAFE_CALL(cudaMallocHost(host_output, width*height*sizeof(uchar3)));
}


extern "C"
__host__ void cuda_ahda_destroy_new(cudaArray_t inputTexArray,

                                   cudaArray *texCielabHorizontalArray,
                                   cudaArray *texCielabVerticalArray,

                                   float4 * cielabHorizontal,
                                   float4 * cielabVertical,


                                   cudaArray *texHomogeneityHorizontalArray,
                                   cudaArray *texHomogeneityVerticalArray,

                                   unsigned char * homogeneityHorizontal,
                                   unsigned char * homogeneityVertical,

                                   uchar4 * horizontal,
                                   uchar4 * vertical,

                                   uchar4 * device_output,
                                   uchar3 * device_output_final,
                                   uchar3 * host_output,
                                   unsigned char * host_input,

                                   cudaStream_t  stream,
                                   cudaEvent_t start,
                                   cudaEvent_t stop,
                                   unsigned char *mask,
                                   float *grayscale)
{

    CUDA_SAFE_CALL(cudaEventDestroy(start));
    CUDA_SAFE_CALL(cudaEventDestroy(stop));

    CUDA_SAFE_CALL(cudaFree(mask));
    CUDA_SAFE_CALL(cudaFree(grayscale));

    // INPUT
    CUDA_SAFE_CALL(cudaFreeHost(host_input));

    CUDA_SAFE_CALL(cudaUnbindTexture(texRefInputNew));
    CUDA_SAFE_CALL(cudaFreeArray((cudaArray*)inputTexArray));

    // CIELAB SOURCE
    CUDA_SAFE_CALL(cudaFree(cielabHorizontal));
    CUDA_SAFE_CALL(cudaFree(cielabVertical));

    // CIELAB TARGET TEXTURES
    CUDA_SAFE_CALL(cudaUnbindTexture(texCielabHorizontalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texCielabHorizontalArray));

    CUDA_SAFE_CALL(cudaUnbindTexture(texCielabVerticalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texCielabVerticalArray));

    // HOMOGENEITY SOURCE
    CUDA_SAFE_CALL(cudaFree(homogeneityVertical));
    CUDA_SAFE_CALL(cudaFree(homogeneityHorizontal));

    // HOMOGENEITY TARGET TEXTURES
    CUDA_SAFE_CALL(cudaUnbindTexture(texHomogeneityHorizontalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texHomogeneityHorizontalArray));

    CUDA_SAFE_CALL(cudaUnbindTexture(texHomogeneityVerticalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texHomogeneityVerticalArray));

    // INTERPOLATED IMAGES
    CUDA_SAFE_CALL(cudaFree(horizontal));
    CUDA_SAFE_CALL(cudaFree(vertical));

    // OUTPUT
    CUDA_SAFE_CALL(cudaFree(device_output));
    CUDA_SAFE_CALL(cudaFree(device_output_final));
    CUDA_SAFE_CALL(cudaFreeHost(host_output));

    CUDA_SAFE_CALL(cudaStreamDestroy(stream));
}



__global__ void basicDebayer_ahda(uchar4 * outData,float * grayscale, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;
    x = x*2;
    y = y*2;


    d_PixelCount = 0;
    uchar4 pixel;
    // take every four pixels

    // G R
    // B G

    if(x >= width || y >= height)
        return;

    float r = tex2D(texRefInputNew,x+1,y);
    float g = tex2D(texRefInputNew,x,y);
    float b = tex2D(texRefInputNew,x,y+1);

    // G [0,0]
    pixel.x = r;
    pixel.y = g;
    pixel.z = b;
    pixel.w = 255;
    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;

    x +=1;
    if(x >= width || y >= height)
        return;

    r = tex2D(texRefInputNew,x,y);
    g = tex2D(texRefInputNew,x-1,y);
    b = tex2D(texRefInputNew,x-1,y+1);

    // R [1,0]
    pixel.x = r;
    pixel.y = g;
    pixel.z = b;
    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;


    x -=1;
    y +=1;
    if(x >= width || y >= height)
        return;

    r = tex2D(texRefInputNew,x+1,y-1);
    g = tex2D(texRefInputNew,x+1,y);
    b = tex2D(texRefInputNew,x,y);

    // B [0,1]
    pixel.x = r;
    pixel.y = g;
    pixel.z = b;
    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;

    x +=1;
    if(x >= width || y >= height)
        return;

    r = tex2D(texRefInputNew,x,y-1);
    g = tex2D(texRefInputNew,x,y);
    b = tex2D(texRefInputNew,x-1,y);

    // G [1,1]
    pixel.x = r;
    pixel.y = g;
    pixel.z = b;
    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*g + 0.3f*r +0.11f*b;
}



__global__ void highQualityLineraInterpolationBy4_R_B_at_B_R_by4_ahda(uchar4 * outData, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    d_PixelCount = 0;

    x*=2;
    y*=2;

    // take every four pixels

    // G R
    // B G

    x++;

    if(x >= width || y >= height)
        return;


    // R [1,0]
    float blue = (2.0f*((float)tex2D(texRefInputNew,x-1,y-1) + (float)tex2D(texRefInputNew,x+1,y-1) + (float)tex2D(texRefInputNew,x-1,y+1) + (float)tex2D(texRefInputNew,x+1,y+1))
                  + 6.0f*(float)tex2D(texRefInputNew,x,y)
                  - 3.0f*((float)tex2D(texRefInputNew,x-2,y) + (float)tex2D(texRefInputNew,x+2,y) + (float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y+2))/2.0f)/8.0f;

    outData[x + y*width].z  = fmin(blue,255.0);

    x--;
    y++;

    if(x >= width || y >= height)
        return;


    // B [0,1]
    float red = (2.0f*((float)tex2D(texRefInputNew,x-1,y-1) + (float)tex2D(texRefInputNew,x+1,y-1) + (float)tex2D(texRefInputNew,x-1,y+1) + (float)tex2D(texRefInputNew,x+1,y+1))
                 + 6.0f*(float)tex2D(texRefInputNew,x,y)
                 - 3.0f*((float)tex2D(texRefInputNew,x-2,y) + (float)tex2D(texRefInputNew,x+2,y) + (float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y+2))/2.0f)/8.0f;

    outData[x + y*width].x  = fmin(red,255.0);
}

__global__ void highQualityLineraInterpolation_by4_ahda(uchar4 * outData,float * grayscale, int width, int height)
{
    uint x = (blockIdx.x * blockDim.x) + threadIdx.x;
    uint y = (blockIdx.y * blockDim.y) + threadIdx.y;

    x*=2;
    y*=2;

    if(x >= width || y >= height)
        return;


    // take every four pixels

    // G R
    // B G

    //    if(x >= width - 1 || y >= height - 1 || x < 1 || y < 1) {
    //        return;
    //    }

    // G [0,0]
    float red = (((float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y + 2))/2.0f
                 - 1.0f*((float)tex2D(texRefInputNew,x-2,y) + (float)tex2D(texRefInputNew,x+2,y) + (float)tex2D(texRefInputNew,x-1,y-1) + (float)tex2D(texRefInputNew,x+1,y-1) + (float)tex2D(texRefInputNew,x-1,y+1) + (float)tex2D(texRefInputNew,x+1,y+1))
                 + 5.0f*(float)tex2D(texRefInputNew,x,y)
                 + 4.0f*((float)tex2D(texRefInputNew,x-1,y) + (float)tex2D(texRefInputNew,x+1,y)))/8.0f;


    float temp = 0;
    if(x == 0)
        temp = 2.0f*(outData[x + 1 + y*width].z);

    if(x == width - 1)
        temp = 2.0f*(outData[x - 1 + y*width].z);

    if(temp == 0)
        temp = 4.0f*( outData[x - 1 + y*width].z+ outData[x + 1 + y*width].z);



    float blue = (((float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y + 2))/2.0f
                  - 1.0f*((float)tex2D(texRefInputNew,x-2,y) + (float)tex2D(texRefInputNew,x+2,y) + (float)tex2D(texRefInputNew,x-1,y-1) + (float)tex2D(texRefInputNew,x+1,y-1) + (float)tex2D(texRefInputNew,x-1,y+1) + (float)tex2D(texRefInputNew,x+1,y+1))
                  + 5.0f*(float)tex2D(texRefInputNew,x,y)
                  + temp /*4.0f*( outData[x - 1 + y*width].z+ outData[x + 1 + y*width].z)*/) / 8.0f;

    float green = tex2D(texRefInputNew,x,y);

    uchar4 pixel;

    pixel.x = fminf(red,colorMax);
    pixel.y = fminf(green,colorMax);
    pixel.z = blue;

    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*green + 0.3f*red +0.11f*blue;

    x++;

    if(x >= width || y >= height)
        return;


    // R [1,0]
    red = tex2D(texRefInputNew,x,y);

    green = (-1.0f*((float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y + 2) + (float)tex2D(texRefInputNew,x + 2,y) + (float)tex2D(texRefInputNew,x - 2,y))
             + 2.0f*((float)tex2D(texRefInputNew,x-1,y) + (float)tex2D(texRefInputNew,x+1,y) + (float)tex2D(texRefInputNew,x,y+1) + (float)tex2D(texRefInputNew,x,y-1)) + 4.0f*(float)tex2D(texRefInputNew,x,y) ) / 8.0f;

    pixel.x = red;
    pixel.y = fminf(green,colorMax);
    pixel.z = outData[x + y*width].z;

    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*green + 0.3f*red +0.11f*((float)pixel.z);

    x--;
    y++;

    if(x >= width || y >= height)
        return;


    // B [0,1]

    blue = tex2D(texRefInputNew,x,y);

    green = (-1.0f*((float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y + 2) + (float)tex2D(texRefInputNew,x + 2,y) + (float)tex2D(texRefInputNew,x - 2,y))
             + 2.0f*((float)tex2D(texRefInputNew,x-1,y) + (float)tex2D(texRefInputNew,x+1,y) + (float)tex2D(texRefInputNew,x,y-1) + (float)tex2D(texRefInputNew,x,y+1))
             + 4.0f*(float)tex2D(texRefInputNew,x,y))/8.0f;


    pixel.x = outData[x + y*width].x;
    pixel.y = green;
    pixel.z = blue;

    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*green + 0.3f*((float)pixel.x) +0.11f*blue;

    x++;

    if(x >= width || y >= height)
        return;


    // G [1,1]
    red = (((float)tex2D(texRefInputNew,x-2,y) + (float)tex2D(texRefInputNew,x+2,y))/2.0f
           - 1.0f*((float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y+2) + (float)tex2D(texRefInputNew,x-1,y-1) + (float)tex2D(texRefInputNew,x+1,y-1) + (float)tex2D(texRefInputNew,x+1,y+1) + (float)tex2D(texRefInputNew,x-1,y+1))
           + 5.0f*(float)tex2D(texRefInputNew,x,y)
           + 4.0f*((float)tex2D(texRefInputNew,x,y+1) + (float)tex2D(texRefInputNew,x,y-1))) / 8.0f;

    temp = 0;
    if(y == 0)
        temp = 2.0f*(outData[x + (y+1)*width].z);

    if(y == height - 1)
        temp = 2.0f*(outData[x + (y-1)*width].z);

    if(temp == 0)
        temp = 4.0f*(outData[x + (y-1)*width].z + outData[x + (y+1)*width].z);


    blue = (((float)tex2D(texRefInputNew,x-2,y) + (float)tex2D(texRefInputNew,x+2,y))/2.0f
            - 1.0f*((float)tex2D(texRefInputNew,x,y-2) + (float)tex2D(texRefInputNew,x,y+2) + (float)tex2D(texRefInputNew,x-1,y-1) + (float)tex2D(texRefInputNew,x+1,y-1) + (float)tex2D(texRefInputNew,x+1,y+1) + (float)tex2D(texRefInputNew,x-1,y+1))
            + 5.0f*(float)tex2D(texRefInputNew,x,y)
            + temp /*4.0f*(outData[x + (y-1)*width].z + outData[x + (y+1)*width].z)*/) / 8.0f;

    green = tex2D(texRefInputNew,x,y);


    pixel.x = fminf(red,colorMax);
    pixel.y = fminf(green,colorMax);
    pixel.z = blue;

    outData[x + y*width] = pixel;
    grayscale[(x + y*width)] = 0.59f*green + 0.3f*red +0.11f*blue;
}




extern "C"
__host__ void cuda_ahda_process_new(cudaArray_t inputTexArray,

                                   cudaArray *texCielabHorizontalArray,
                                   cudaArray *texCielabVerticalArray,

                                   float4 * cielabHorizontal,
                                   float4 * cielabVertical,

                                   cudaArray *texHomogeneityHorizontalArray,
                                   cudaArray *texHomogeneityVerticalArray,

                                   unsigned char * homogeneityHorizontal,
                                   unsigned char * homogeneityVertical,

                                   uchar4 * horizontal,
                                   uchar4 * vertical,

                                   uchar4 * device_output,
                                   uchar3 * device_output_final,
                                   uchar3 * host_output,
                                   int width,
                                   int height,
                                   cudaStream_t stream,
                                   unsigned char * host_input,
                                   cudaEvent_t start,

                                   cudaEvent_t stop,
                                   unsigned char *mask,
                                   float *grayscale,
                                   int *edgePixelCount)
{
    dim3 threadsPerBlock(32, 8);
    dim3 numBlocks(ceil((double)width/(double)threadsPerBlock.x), ceil((double)height/(double)threadsPerBlock.y));
    dim3 numBlocks_half(ceil((double)width/(double)threadsPerBlock.x/2.0), ceil((double)height/(double)threadsPerBlock.y/2.0));


    //CUDA_SAFE_CALL(cudaMemcpyToSymbolAsync(d_AHDPixelCount,edgePixelCount,sizeof(int),0,cudaMemcpyHostToDevice,stream));

    // copy input data into array accessed via texture
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(inputTexArray, 0, 0, host_input, width * height, cudaMemcpyHostToDevice,stream));


    // kernels
    cudaEventRecord(start,stream);



    int step = 0;

    switch(step)
    {
    case 0: // BILINEAR
        bilinearDebayer_firstStep_new<<<numBlocks_half,threadsPerBlock,0,stream>>>(device_output,grayscale,width);
        break;
    case 1: // NEIGHBOUR
        basicDebayer_ahda<<<numBlocks_half,threadsPerBlock,0,stream>>>(device_output, grayscale, width, height);
        break;
    case 2:     // HQLI
        highQualityLineraInterpolationBy4_R_B_at_B_R_by4_ahda<<<numBlocks_half,threadsPerBlock,0,stream>>>(device_output,  width, height);
        highQualityLineraInterpolation_by4_ahda<<<numBlocks_half,threadsPerBlock,0,stream>>>(device_output, grayscale, width, height);
        break;
    }

    bilinearDebayer_edgeDetection_combined<<<numBlocks,threadsPerBlock,0,stream>>>(grayscale,mask, width, height);

    ahda_interpolateCombined_mask<<<numBlocks_half,threadsPerBlock,0,stream>>>(vertical,horizontal,width,height,mask);

    // convert to cielab
    convertToCielab_mask<<<numBlocks,threadsPerBlock,0,stream>>>(vertical,horizontal,cielabHorizontal,cielabVertical,width,height,mask);

    // copy cielab data into arrays accessed via texture
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texCielabHorizontalArray, 0,0,cielabHorizontal, width*height*sizeof(float4), cudaMemcpyDeviceToDevice,stream));
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texCielabVerticalArray,   0,0,cielabVertical,   width*height*sizeof(float4), cudaMemcpyDeviceToDevice,stream));

    calculateHomogenity_mask<<<numBlocks,threadsPerBlock,0,stream>>>(homogeneityVertical,homogeneityHorizontal,width,height,mask);

    // copy homogeneity data into arrays accessed via texture
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texHomogeneityVerticalArray,  0,0,homogeneityVertical,  width*height, cudaMemcpyDeviceToDevice,stream));
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texHomogeneityHorizontalArray,0,0,homogeneityHorizontal,width*height, cudaMemcpyDeviceToDevice,stream));

    // in original data is from CIELAB not RGB
    chooseDirection_mask<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,vertical,horizontal,width,height,mask);


    // remove artifacts (optional)
    removeArtifacts_mask<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,mask,width,height);
    removeArtifacts_mask<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,mask,width,height);
    removeArtifacts_mask<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,mask,width,height);

    convertUchar4to3<<<numBlocks_half,threadsPerBlock,0,stream>>>(device_output,device_output_final,width,height);

    cudaEventRecord(stop,stream);

    CUDA_SAFE_CALL(cudaMemcpyFromSymbolAsync(edgePixelCount,d_PixelCount,sizeof(int),0,cudaMemcpyDeviceToHost,stream));
    CUDA_SAFE_CALL(cudaMemcpyAsync(host_output, device_output_final, width*height*sizeof(uchar3), cudaMemcpyDeviceToHost,stream));
}





//           _    _ _____             ____  _____  _____ _____
//     /\   | |  | |  __ \   /\      / __ \|  __ \|_   _/ ____|
//    /  \  | |__| | |  | | /  \    | |  | | |__) | | || |  __
//   / /\ \ |  __  | |  | |/ /\ \   | |  | |  _  /  | || | |_ |
//  / ____ \| |  | | |__| / ____ \  | |__| | | \ \ _| || |__| |
// /_/    \_\_|  |_|_____/_/    \_\  \____/|_|  \_\_____\_____|





__global__ void ahda_interpolateCombined(uchar4 * vertical,uchar4 * horizontal, int width, int height)
{
    uint x = ((blockIdx.x * blockDim.x) + threadIdx.x)*2;
    uint y = ((blockIdx.y * blockDim.y) + threadIdx.y)*2;

    if(x >= width || y >= height)
        return;



    uchar4 pixel;
    pixel.w = 255;


    // [0,0] G
    vertical  [x + y*width].y = tex2D(texRefInputNew,x,y);
    horizontal[x + y*width].y = tex2D(texRefInputNew,x,y);

    x+=1;
    // [1,0] R
    pixel.x = tex2D(texRefInputNew,x,y);
    pixel.y = fminf(((float)tex2D(texRefInputNew,x,y - 1) + (float)tex2D(texRefInputNew,x,y + 1))/2.0f + ((float)2.0f*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x,y - 2) - (float)tex2D(texRefInputNew,x,y + 2))/4.0f,colorMax);
    pixel.z = 0;
    horizontal[x + y*width] = pixel;

    pixel.x = tex2D(texRefInputNew,x,y);
    pixel.y = fminf(((float)tex2D(texRefInputNew,x - 1,y) + (float)tex2D(texRefInputNew,x + 1,y))/2.0f + ((float)2.0f*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x - 2,y) - (float)tex2D(texRefInputNew,x + 2,y))/4.0f,colorMax);
    vertical[x + y*width] = pixel;


    x-=1;
    y+=1;
    // [0,1] B
    pixel.x = 0;
    pixel.y = fminf(((float)tex2D(texRefInputNew,x,y - 1) + (float)tex2D(texRefInputNew,x,y + 1))/2.0f + ((float)2.0*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x,y - 2) - (float)tex2D(texRefInputNew,x,y + 2))/4.0f,colorMax);
    pixel.z = tex2D(texRefInputNew,x,y);
    horizontal[x + y*width]  = pixel;

    pixel.x = 0;
    pixel.y = fminf(((float)tex2D(texRefInputNew,x - 1,y) + (float)tex2D(texRefInputNew,x + 1,y))/2.0f + ((float)2.0*tex2D(texRefInputNew,x,y) - (float)tex2D(texRefInputNew,x - 2,y) - (float)tex2D(texRefInputNew,x + 2,y))/4.0f,colorMax);
    pixel.z = tex2D(texRefInputNew,x,y);
    vertical[x + y*width]  = pixel;

    x+=1;
    // [1,1] G
    vertical[x + y*width].y   = tex2D(texRefInputNew,x,y);
    horizontal[x + y*width].y = tex2D(texRefInputNew,x,y);


    //////////////////////////////////////////////////////////////////////
    __syncthreads();



    y-=1;

    if(x < 1 || y < 1 || x > width -1 || y> height - 1)
        return;

    // [1,0] R - calculate B
    horizontal[x + y*width].z = fminf(
                ((float)horizontal[x+1 + (y+1)*width].z + (float)horizontal[x-1 + (y+1)*width].z + (float)horizontal[x+1 + (y-1)*width].z +(float)horizontal[x-1 + (y-1)*width].z)/4.0f,colorMax);

    vertical[x + y*width].z = fminf(
                ((float)vertical[x+1 + (y+1)*width].z + (float)vertical[x-1 + (y+1)*width].z + (float)vertical[x+1 + (y-1)*width].z + (float)vertical[x-1 + (y-1)*width].z)/4.0f,colorMax);

    x-=1;
    y+=1;
    // [0,1] B - calculate R
    horizontal[x + y*width].x = fminf(
                ((float)horizontal[x+1  + (y+1)*width].x +(float)horizontal[x-1  + (y+1)*width].x +(float)horizontal[x+1  + (y-1)*width].x +(float)horizontal[x-1  + (y-1)*width].x)/4.0f,colorMax);

    vertical[x + y*width].x = fminf(
                ((float)vertical[x+1 + (y+1)*width].x +(float)vertical[x-1 + (y+1)*width].x +(float)vertical[x+1 + (y-1)*width].x +(float)vertical[x-1 + (y-1)*width].x)/4.0f,colorMax);


    //////////////////////////////////////////////////////////////////////
    __syncthreads();

    y-=1;
    // [0,0] - G
    // R
    horizontal[x + y*width].x = fminf(
                ((float)horizontal[x+1 + (y)*width].x +
                (float)horizontal[x + (y+1)*width].x +
            (float)horizontal[x-1 + (y)*width].x +
            (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

    vertical[x + y*width].x = fminf(
                ((float)vertical[x+1 + (y)*width].x +
                (float)vertical[x + (y+1)*width].x +
            (float)vertical[x-1 + (y)*width].x +
            (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);

    // B
    horizontal[x + y*width].z = fminf(
                ((float)horizontal[x+1 + (y)*width].z +
                (float)horizontal[x + (y+1)*width].z +
            (float)horizontal[x-1 + (y)*width].z +
            (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

    vertical[x + y*width].z = fminf(
                ((float)vertical[x+1 + (y)*width].z +
                (float)vertical[x + (y+1)*width].z +
            (float)vertical[x-1 + (y)*width].z +
            (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);


    x+=1;
    y+=1;
    // [1,1] - G
    // R
    horizontal[x + y*width].x = fminf(
                ((float)horizontal[x+1 + (y)*width].x +
                (float)horizontal[x + (y+1)*width].x +
            (float)horizontal[x-1 + (y)*width].x +
            (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

    vertical[x + y*width].x = fminf(
                ((float)vertical[x+1 + (y)*width].x +
                (float)vertical[x + (y+1)*width].x +
            (float)vertical[x-1 + (y)*width].x +
            (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);
    // B
    horizontal[x + y*width].z = fminf(
                ((float)horizontal[x+1 + (y)*width].z +
                (float)horizontal[x + (y+1)*width].z +
            (float)horizontal[x-1 + (y)*width].z +
            (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

    vertical[x + y*width].z = fminf(
                ((float)vertical[x+1 + (y)*width].z +
                (float)vertical[x + (y+1)*width].z +
            (float)vertical[x-1 + (y)*width].z +
            (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);

    //////////////////////////////////////////////////////////////////////
    __syncthreads();

    x-=1;
    y-=1;
    // [0,0] - G
    // R
    horizontal[x + y*width].x = fminf(
                ((float)horizontal[x+1 + (y)*width].x +
                (float)horizontal[x + (y+1)*width].x +
            (float)horizontal[x-1 + (y)*width].x +
            (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

    vertical[x + y*width].x = fminf(
                ((float)vertical[x+1 + (y)*width].x +
                (float)vertical[x + (y+1)*width].x +
            (float)vertical[x-1 + (y)*width].x +
            (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);

    // B
    horizontal[x + y*width].z = fminf(
                ((float)horizontal[x+1 + (y)*width].z +
                (float)horizontal[x + (y+1)*width].z +
            (float)horizontal[x-1 + (y)*width].z +
            (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

    vertical[x + y*width].z = fminf(
                ((float)vertical[x+1 + (y)*width].z +
                (float)vertical[x + (y+1)*width].z +
            (float)vertical[x-1 + (y)*width].z +
            (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);


    x+=1;
    y+=1;
    // [1,1] - G
    // R
    horizontal[x + y*width].x = fminf(
                ((float)horizontal[x+1 + (y)*width].x +
                (float)horizontal[x + (y+1)*width].x +
            (float)horizontal[x-1 + (y)*width].x +
            (float)horizontal[x + (y-1)*width].x)/4.0f,colorMax);

    vertical[x + y*width].x = fminf(
                ((float)vertical[x+1 + (y)*width].x +
                (float)vertical[x + (y+1)*width].x +
            (float)vertical[x-1 + (y)*width].x +
            (float)vertical[x + (y-1)*width].x)/4.0f,colorMax);
    // B
    horizontal[x + y*width].z = fminf(
                ((float)horizontal[x+1 + (y)*width].z +
                (float)horizontal[x + (y+1)*width].z +
            (float)horizontal[x-1 + (y)*width].z +
            (float)horizontal[x + (y-1)*width].z)/4.0f,colorMax);

    vertical[x + y*width].z = fminf(
                ((float)vertical[x+1 + (y)*width].z +
                (float)vertical[x + (y+1)*width].z +
            (float)vertical[x-1 + (y)*width].z +
            (float)vertical[x + (y-1)*width].z)/4.0f,colorMax);
}



extern "C"
__host__ void cuda_ahda_init(cudaArray_t *inputTexArray,

                            cudaArray **texCielabHorizontalArray,
                            cudaArray **texCielabVerticalArray,

                            float4 ** cielabHorizontal,
                            float4 ** cielabVertical,


                            cudaArray **texHomogeneityHorizontalArray,
                            cudaArray **texHomogeneityVerticalArray,

                            unsigned char ** homogeneityHorizontal,
                            unsigned char ** homogeneityVertical,

                            uchar4 ** horizontal,
                            uchar4 ** vertical,

                            uchar4 **device_output,
                            uchar3 **device_output_final,
                            uchar3 **host_output,
                            unsigned char **host_inputData,
                            int width,
                            int height,
                            cudaStream_t * stream, cudaEvent_t *start, cudaEvent_t *stop)
{

    cudaStreamCreate(stream);


    cudaEventCreate(start);
    cudaEventCreate(stop);

    // INPUT DATA

    CUDA_SAFE_CALL(cudaMallocHost(host_inputData, width*height));

    texRefInputNew.addressMode[0] = cudaAddressModeClamp;
    texRefInputNew.addressMode[1] = cudaAddressModeClamp;
    texRefInputNew.filterMode = cudaFilterModePoint;
    texRefInputNew.normalized = false;

    static cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar1>();
    CUDA_SAFE_CALL(cudaMallocArray(inputTexArray, &channelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texRefInputNew, *inputTexArray, channelDesc));


    // CIELAB
    CUDA_SAFE_CALL(cudaMalloc(cielabHorizontal, width*height*sizeof(float4)));
    CUDA_SAFE_CALL(cudaMalloc(cielabVertical,   width*height*sizeof(float4)));

    static cudaChannelFormatDesc cielabChannelDesc = cudaCreateChannelDesc<float4>();

    // horizontal
    texCielabHorizontalRef.addressMode[0] = cudaAddressModeClamp;
    texCielabHorizontalRef.addressMode[1] = cudaAddressModeClamp;
    texCielabHorizontalRef.filterMode = cudaFilterModePoint;
    texCielabHorizontalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texCielabHorizontalArray, &cielabChannelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray( texCielabHorizontalRef,*texCielabHorizontalArray, cielabChannelDesc));

    // vertical
    texCielabVerticalRef.addressMode[0] = cudaAddressModeClamp;
    texCielabVerticalRef.addressMode[1] = cudaAddressModeClamp;
    texCielabVerticalRef.filterMode = cudaFilterModePoint;
    texCielabVerticalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texCielabVerticalArray, &cielabChannelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texCielabVerticalRef, *texCielabVerticalArray, cielabChannelDesc));

    // HOMOGENEITY SOURCE BUFFERS
    CUDA_SAFE_CALL(cudaMalloc(homogeneityVertical, width*height*sizeof(uchar4)));
    CUDA_SAFE_CALL(cudaMalloc(homogeneityHorizontal, width*height*sizeof(uchar4)));

    // HOMOGENEITY TARGET TEXTURES
    static cudaChannelFormatDesc homogeneityChannelDesc = cudaCreateChannelDesc<unsigned char>();

    // horizontal
    texHomogeneityHorizontalRef.addressMode[0] = cudaAddressModeClamp;
    texHomogeneityHorizontalRef.addressMode[1] = cudaAddressModeClamp;
    texHomogeneityHorizontalRef.filterMode = cudaFilterModePoint;
    texHomogeneityHorizontalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texHomogeneityHorizontalArray, &homogeneityChannelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texHomogeneityHorizontalRef, *texHomogeneityHorizontalArray, homogeneityChannelDesc));

    // vertical
    texHomogeneityVerticalRef.addressMode[0] = cudaAddressModeClamp;
    texHomogeneityVerticalRef.addressMode[1] = cudaAddressModeClamp;
    texHomogeneityVerticalRef.filterMode = cudaFilterModePoint;
    texHomogeneityVerticalRef.normalized = false;

    CUDA_SAFE_CALL(cudaMallocArray(texHomogeneityVerticalArray, &homogeneityChannelDesc, width, height));
    CUDA_SAFE_CALL(cudaBindTextureToArray(texHomogeneityVerticalRef, *texHomogeneityVerticalArray, homogeneityChannelDesc));


    // INTERPOLATED IMAGES
    // horizontal
    CUDA_SAFE_CALL(cudaMalloc(horizontal, width*height*sizeof(uchar4)));
    // vertical
    CUDA_SAFE_CALL(cudaMalloc(vertical,   width*height*sizeof(uchar4)));


    // OUTPUT IMAGE
    CUDA_SAFE_CALL(cudaMalloc(device_output,       width*height*sizeof(uchar4)));
    CUDA_SAFE_CALL(cudaMalloc(device_output_final, width*height*sizeof(uchar3)));
    CUDA_SAFE_CALL(cudaMallocHost(host_output,     width*height*sizeof(uchar3)));
}


extern "C"
__host__ void cuda_ahda_destroy(cudaArray_t inputTexArray,

                               cudaArray *texCielabHorizontalArray,
                               cudaArray *texCielabVerticalArray,

                               float4 * cielabHorizontal,
                               float4 * cielabVertical,


                               cudaArray *texHomogeneityHorizontalArray,
                               cudaArray *texHomogeneityVerticalArray,

                               unsigned char * homogeneityHorizontal,
                               unsigned char * homogeneityVertical,

                               uchar4 * horizontal,
                               uchar4 * vertical,

                               uchar4 * device_output,
                               uchar3 * device_output_final,
                               uchar3 * host_output,
                               unsigned char * host_inputData,

                               cudaStream_t  stream,
                               cudaEvent_t start, cudaEvent_t stop)
{

    // INPUT
    CUDA_SAFE_CALL(cudaFreeHost(host_inputData));

    CUDA_SAFE_CALL(cudaUnbindTexture(texRefInputNew));
    CUDA_SAFE_CALL(cudaFreeArray((cudaArray*)inputTexArray));

    // CIELAB SOURCE
    CUDA_SAFE_CALL(cudaFree(cielabHorizontal));
    CUDA_SAFE_CALL(cudaFree(cielabVertical));

    // CIELAB TARGET TEXTURES
    CUDA_SAFE_CALL(cudaUnbindTexture(texCielabHorizontalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texCielabHorizontalArray));

    CUDA_SAFE_CALL(cudaUnbindTexture(texCielabVerticalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texCielabVerticalArray));

    // HOMOGENEITY SOURCE
    CUDA_SAFE_CALL(cudaFree(homogeneityVertical));
    CUDA_SAFE_CALL(cudaFree(homogeneityHorizontal));

    // HOMOGENEITY TARGET TEXTURES
    CUDA_SAFE_CALL(cudaUnbindTexture(texHomogeneityHorizontalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texHomogeneityHorizontalArray));

    CUDA_SAFE_CALL(cudaUnbindTexture(texHomogeneityVerticalRef));
    CUDA_SAFE_CALL(cudaFreeArray(texHomogeneityVerticalArray));

    // INTERPOLATED IMAGES
    CUDA_SAFE_CALL(cudaFree(horizontal));
    CUDA_SAFE_CALL(cudaFree(vertical));

    // OUTPUT
    CUDA_SAFE_CALL(cudaFree(device_output));
    CUDA_SAFE_CALL(cudaFree(device_output_final));
    CUDA_SAFE_CALL(cudaFreeHost(host_output));

    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    cudaStreamDestroy(stream);
}


extern "C"
__host__ void cuda_ahda_process(cudaArray_t inputTexArray,

                               cudaArray *texCielabHorizontalArray,
                               cudaArray *texCielabVerticalArray,

                               float4 * cielabHorizontal,
                               float4 * cielabVertical,

                               cudaArray *texHomogeneityHorizontalArray,
                               cudaArray *texHomogeneityVerticalArray,

                               unsigned char * homogeneityHorizontal,
                               unsigned char * homogeneityVertical,

                               uchar4 * horizontal,
                               uchar4 * vertical,

                               uchar4 * device_output,
                               uchar3 * device_output_final,
                               uchar3 * host_output,
                               int width,
                               int height,
                               cudaStream_t stream,
                               unsigned char * host_inputData,
                               cudaEvent_t start,
                               cudaEvent_t stop)
{
    dim3 threadsPerBlock(32, 32);
    dim3 numBlocks(ceil((double)width/(double)threadsPerBlock.x),ceil((double)height/(double)threadsPerBlock.y));
    dim3 numBlocks_half(ceil((double)width/(double)threadsPerBlock.x/2.0),ceil((double)height/(double)threadsPerBlock.y/2.0));

    // copy input data into array accessed via texture
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(inputTexArray, 0, 0, host_inputData, width * height, cudaMemcpyHostToDevice,stream));

    // kernels
    cudaEventRecord(start);

    ahda_interpolateCombined<<<numBlocks_half,threadsPerBlock,0,stream>>>(vertical,horizontal,width,height);

    // horizontal/vertical image memory -> texture (uchar)
    convertToCielab<<<numBlocks,threadsPerBlock,0,stream>>>(vertical,horizontal,cielabHorizontal,cielabVertical,width,height);

    // copy cielab data into arrays accessed via texture
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texCielabHorizontalArray, 0,0,cielabHorizontal, width*height*sizeof(float4), cudaMemcpyDeviceToDevice,stream));
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texCielabVerticalArray,   0,0,cielabVertical,   width*height*sizeof(float4), cudaMemcpyDeviceToDevice,stream));

    calculateHomogenity<<<numBlocks,threadsPerBlock,0,stream>>>(homogeneityVertical,homogeneityHorizontal,width,height);

    // copy homogeneity data into arrays accessed via texture
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texHomogeneityVerticalArray,  0,0,homogeneityVertical,  width*height, cudaMemcpyDeviceToDevice,stream));
    CUDA_SAFE_CALL(cudaMemcpyToArrayAsync(texHomogeneityHorizontalArray,0,0,homogeneityHorizontal,width*height, cudaMemcpyDeviceToDevice,stream));

    // in original data is from CIELAB not RGB
    chooseDirection<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,vertical,horizontal,width,height);

    // remove artifacts (optional)
    removeArtifacts<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,width,height);
    removeArtifacts<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,width,height);
    removeArtifacts<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,width,height);

    convertUchar4to3<<<numBlocks,threadsPerBlock,0,stream>>>(device_output,device_output_final,  width,  height);
    cudaEventRecord(stop);


    CUDA_SAFE_CALL(cudaMemcpyAsync(host_output, device_output_final, width*height*sizeof(uchar3), cudaMemcpyDeviceToHost,stream));
}



