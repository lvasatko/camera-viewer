#include "cameradebayerwidget.h"
#include "ui_cameradebayerwidget.h"

#include <iostream>

#include <QTime>

#include <QFileDialog>

CameraDebayerWidget::CameraDebayerWidget(QSize frameSize, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CameraDebayerWidget),mSize(frameSize)
{
    ui->setupUi(this);

    qRegisterMetaType<VideoFrame*>("VideoFrame *");


    fpsTimer.setInterval(1000);
    connect(&fpsTimer,&QTimer::timeout,[this]() {
        if(mFrameToDebayerCounter == 0 &&  mDebayeredFrameCounter == 0) {
            fpsTimer.stop();
            running = false;
            this->enableGUI(!running);
            qDebug() << "Demosaicing finished\\\\\\\ " ;
            ui->startStopBtn->setText("START");
            return;
        }
        qDebug() << "FPS: " << mDebayeredFrameCounter;
        mFrameToDebayerCounter = 0;
        mDebayeredFrameCounter = 0;
    });

    connect(&mShowTimer,SIGNAL(timeout()),this,SLOT(showImage()));
    mShowTimer.setInterval(1000/60.0);
    mShowTimer.start();
}

CameraDebayerWidget::~CameraDebayerWidget()
{
    mShowTimer.stop();

    if(mDemoFrameGrabber != nullptr) {

        QMetaObject::invokeMethod(mDemoFrameGrabber,"destroy",Qt::BlockingQueuedConnection);
        mDemoFrameGrabberThread.quit();
        if(!mDemoFrameGrabberThread.wait(2000))
            mDemoFrameGrabberThread.terminate();

        delete mDemoFrameGrabber;
    }


    mDebayerThread.quit();
    if(!mDebayerThread.wait(2000))
        mDebayerThread.terminate();

    if(mDebayerUnit != nullptr)
        delete mDebayerUnit;

    delete ui;
}

void CameraDebayerWidget::init()
{
    mFrameToDebayerCounter = 0;
    mDebayeredFrameCounter = 0;

    mDebayerThread.quit();
    if(!mDebayerThread.wait(2000))
        mDebayerThread.terminate();

    if(mDebayerUnit != nullptr)
        delete mDebayerUnit;

    if(mDemoFrameGrabber != nullptr) {
        QMetaObject::invokeMethod(mDemoFrameGrabber,"destroy",Qt::BlockingQueuedConnection);
        mDemoFrameGrabberThread.quit();
        if(!mDemoFrameGrabberThread.wait(2000))
            mDemoFrameGrabberThread.terminate();

        delete mDemoFrameGrabber;
    }




    ui->startStopBtn->setText("LOADING");




    mDemoFrameGrabber = new DemoFrameGrabber();
    mFrameCount = ui->repeatSpinBox->value();
    mDemoFrameGrabber->setFrameCount(mFrameCount);

    mDemoFrameGrabber->moveToThread(&mDemoFrameGrabberThread);
    mDemoFrameGrabberThread.start();
    QMetaObject::invokeMethod(mDemoFrameGrabber,"init");
    QMetaObject::invokeMethod(mDemoFrameGrabber,"setFps",Q_ARG(int,mFps));
    connect(mDemoFrameGrabber,SIGNAL(frameToDebayer(VideoFrame*)),this,SLOT(newFrameToDebayer(VideoFrame *)));



    switch(mCurrentInputType)
    {
    case IMAGE:
        QMetaObject::invokeMethod(mDemoFrameGrabber,"loadImage",Qt::BlockingQueuedConnection,Q_ARG(QString,mImageFilename));
        break;
    case MJPEG:
        QMetaObject::invokeMethod(mDemoFrameGrabber,"loadMJPEG",Qt::BlockingQueuedConnection,Q_ARG(QString,mMJPEGdataFilename),Q_ARG(QString,mMJPEGindexFilename),Q_ARG(int,0),Q_ARG(int,mFrameCount),Q_ARG(QSize,mSize));
        break;
    }



    mDebayerUnit = new Debayer(mCudaDeviceNumber);
    mDebayerUnit->moveToThread(&mDebayerThread);
    mDebayerThread.start();   
    QMetaObject::invokeMethod(mDebayerUnit,"init",Qt::BlockingQueuedConnection,Q_ARG(int, mMethodType),Q_ARG(int, mStreamCount),Q_ARG(uint, mSize.width()),Q_ARG(uint, mSize.height()));

    ui->startStopBtn->setText("STOP");

    connect(mDebayerUnit,SIGNAL(newDebayeredFrame(VideoFrame *)),this,SLOT(newDebayeredFrame(VideoFrame *)));


    fpsTimer.start();
}

void CameraDebayerWidget::newDebayeredFrame(VideoFrame *frame)
{
    mDebayeredFrameCounter++;

    if(mutex.tryLock()) {
        if(mCurrentDebayeredFrame != nullptr)
            delete mCurrentDebayeredFrame;

        mCurrentDebayeredFrame = frame;

        mutex.unlock();
    }
    else
        delete frame;
}

void CameraDebayerWidget::showImage()
{
    mutex.lock();
    if(mCurrentDebayeredFrame != nullptr)
    {
        ui->openGLWidget->newFrame(mCurrentDebayeredFrame);

        mCurrentDebayeredFrame = nullptr;
    }
    mutex.unlock();
}

void CameraDebayerWidget::setCudaDevice(int cudaDevice)
{
    mCudaDeviceNumber = cudaDevice;
}

void CameraDebayerWidget::newFrameToDebayer(VideoFrame *frame)
{
    mFrameToDebayerCounter++;

    if(storring)
        delete frame;

    QMetaObject::invokeMethod(mDebayerUnit,"enqueueFrame",Q_ARG(VideoFrame *,frame));
}


void CameraDebayerWidget::on_startStopBtn_clicked()
{
    if(running) {

        mFrameToDebayerCounter = 0;
        mDebayeredFrameCounter = 0;

        mDebayerThread.quit();
        if(!mDebayerThread.wait(2000))
            mDebayerThread.terminate();

        if(mDebayerUnit != nullptr) {
            delete mDebayerUnit;
            mDebayerUnit = nullptr;
        }


        if(mDemoFrameGrabber != nullptr) {
            QMetaObject::invokeMethod(mDemoFrameGrabber,"destroy",Qt::BlockingQueuedConnection);
            mDemoFrameGrabberThread.quit();
            if(!mDemoFrameGrabberThread.wait(2000))
                mDemoFrameGrabberThread.terminate();

            delete mDemoFrameGrabber;

            mDemoFrameGrabber = nullptr;
        }

        fpsTimer.stop();
        running = false;
        this->enableGUI(!running);
        qDebug() << "Demosaicing finished\\\\\\\ " ;
        ui->startStopBtn->setText("START");

        return;
    }


    this->init();

    if(mDemoFrameGrabber != nullptr)
        QMetaObject::invokeMethod(mDemoFrameGrabber,"start");

    qDebug() << "Demosaicing started\\\\\\\\";
    running = true;
    enableGUI(!running);
}

void CameraDebayerWidget::on_fpsSpinBox_valueChanged(int arg1)
{
    mFps = arg1;
    if(mDemoFrameGrabber != nullptr)
        QMetaObject::invokeMethod(mDemoFrameGrabber,"setFps",Q_ARG(int,arg1));
}

void CameraDebayerWidget::on_repeatSpinBox_valueChanged(int arg1)
{
    mFrameCount = arg1;
}

void CameraDebayerWidget::on_methodComboBox_currentIndexChanged(int index)
{
    mMethodType = (Debayer::Methods)index;
}

void CameraDebayerWidget::on_streamCountSpinBox_valueChanged(int arg1)
{
    mStreamCount = arg1;
}

void CameraDebayerWidget::on_loadBtn_clicked()
{
    switch(mCurrentInputType)
    {
    case IMAGE: {
        QString filename = QFileDialog::getOpenFileName(this,"Choose image",QApplication::applicationDirPath());

        if(filename.isEmpty())
            return;

        mImageFilename = filename;
        break;
    }

    case MJPEG: {
        QString mjpegFile = QFileDialog::getOpenFileName(this,"Choose mjpeg file",QApplication::applicationDirPath(),"*.mjpeg");

        if(mjpegFile.isEmpty())
            return;

        mMJPEGdataFilename  = mjpegFile;

        QString indexFile = mjpegFile;
        indexFile = indexFile.replace("video","index");
        mMJPEGindexFilename = indexFile.replace("mjpeg","bin");
        break;
    }
    }
}

void CameraDebayerWidget::on_inputTypeComboBox_currentIndexChanged(int index)
{
    mCurrentInputType = (InputType)index;
    switch (mCurrentInputType) {
    case IMAGE:
        ui->loadBtn->setText("Load IMAGE");
        break;
    case MJPEG:
        ui->loadBtn->setText("Load MJPEG");
        break;
    default:
        break;
    }
}

void CameraDebayerWidget::enableGUI(bool enable)
{
    ui->fpsSpinBox->setEnabled(enable);
    ui->inputTypeComboBox->setEnabled(enable);
    ui->streamCountSpinBox->setEnabled(enable);
    ui->repeatSpinBox->setEnabled(enable);
    ui->methodComboBox->setEnabled(enable);
    ui->loadBtn->setEnabled(enable);
}
