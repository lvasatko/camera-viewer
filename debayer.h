#ifndef DEBAYER_H
#define DEBAYER_H

#include <QObject>
#include <QMutex>
#include <QList>
#include <QImage>
#include <QQueue>
#include <QTimer>
#include <QElapsedTimer>
#include <QThread>

#include "videoframe.h"
#include "worker.h"

class DebayerMethod;
class Debayer;
/////
/// \brief The Debayer class
///
class Debayer : public QObject
{
    Q_OBJECT
public:

    /////
    /// \brief The Methods enum
    /// list of implemented methods
    enum Methods{
        Dummy = 0,
        Dfpd,
        Hqli,
        Ahda,
        Bilinear,
        Ahda_edge
    };

    ////
    /// \brief Debayer
    /// \param cudaDeviceNumber number of cuda device used on demosaicing
    /// \param parent from Qt QObject design
    ///
    explicit Debayer(int cudaDeviceNumber = 0, QObject *parent = 0);

    ~Debayer();

public slots:
    /////
    /// \brief init method for usage of already implemented methods
    /// \param method from Methods enum
    /// \param streamCount number of streams created for give method
    /// \param width of input frame in pixels
    /// \param height of input frame in pixels
    ///
    void init(int method, int streamCount,uint width, uint height);
    //////
    /// \brief init method for usage of new methods implemented as subclasses of class DebayerMethod
    /// \param methodObjecs objects sublassed from DebayerMethod class
    /// \param width of input frame in pixels
    /// \param height of input frame in pixels
    ///
    void init(QList<DebayerMethod *> methodObjecs, uint width, uint height);

    ////////
    /// \brief enqueueFrame for processing, if any Worker is idle it starts processing VideoFrame immediatly
    /// \param frame
    ///
    void enqueueFrame(VideoFrame *frame);
    ////
    /// \brief dequeueFrame function called from Worker object to get next VideoFrame if processing has finished
    /// \return pointer to VideoFrame , if null no frame waits for processing
    ///
    VideoFrame *dequeueFrame();
signals:
    /////
    /// \brief newDebayeredFrame signal sends out already processed VideoFrame
    ///
    void newDebayeredFrame(VideoFrame *);

private:
    QList<Worker *>   m_workers;

    QMutex m_framesToProcessMutex;
    QQueue<VideoFrame *> m_framesToProcess;

    QMutex m_framesProcessedMutex;
    QQueue<VideoFrame *> m_framesProcessed;

    int cudaDevice;
    QSize size;
};

#endif // DEBAYER_H
