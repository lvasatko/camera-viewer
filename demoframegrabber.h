﻿#ifndef DEMOFRAMEGRABBER_H
#define DEMOFRAMEGRABBER_H

#include <QObject>
#include <QTimer>

#include "videoframe.h"
#include "convertertobayer.h"

#include <QRgb>
#include <QMutex>
#include <QSize>


//////
/// \brief The DemoFrameGrabber class
/// class simulates frame grabber
class DemoFrameGrabber : public QObject
{
    Q_OBJECT
public:
    //////
    /// \brief DemoFrameGrabber
    /// \param parent
    ///
    explicit DemoFrameGrabber( QObject *parent = 0);
    ~DemoFrameGrabber();

    ////
    /// \brief frameCount
    /// \return number of frames which will be sended to demosaicing
    ///

    int frameCount() const;
    ////
    /// \brief setFrameCount
    /// \param frameCount number of frames which will be sended to demosaicing
    ///
    void setFrameCount(int frameCount);
    /////
    /// \brief fps
    /// \return fps used on output, if fps == 0, all frames are sended immediately
    ///
    int fps() const;


signals:
    ////
    /// \brief frameToDebayer
    /// \param frame to demosaicing
    ///
    void frameToDebayer(VideoFrame * frame);

public slots:
    /////
    /// \brief setFps
    /// \param fps
    /// sets output fps, if fps == 0, all frames are sended immediately
    void setFps(int fps);

    ////
    /// \brief start
    /// start sending frames to demosaicing, if fps == 0, send all frames, if fps > 0 send frames in fps interval
    void start();
    ////
    /// \brief loadImage
    /// \param pathToImageFile
    /// load image file and convert it into CFA type given in constructor
    void loadImage(QString pathToImageFile);
    //////
    /// \brief loadMJPEG
    /// \param mjpegFilename
    /// \param indexFilename
    /// \param startFrame
    /// \param frameCount
    /// \param frameResolution
    /// function loads MJPEG file and index file which contains frame sizes stored on 4 bytes
    void loadMJPEG(QString mjpegFilename, QString indexFilename, int startFrame, int frameCount, QSize frameResolution);

    ////
    /// \brief init
    /// function initialized timer for simulating fps
    void init();

    /////
    /// \brief destroy
    ///  destroys previously created timer for simulating fps
    void destroy();

private slots:
    void sendFrame();

private:
    int m_currentFrameIndex{0};
    int m_frameCount {1000};

    int m_fps{0};
    QTimer * m_fpsTimer{nullptr};

    int m_debazerType {};
    ConverterToBayer * m_converter{nullptr};

    QList<VideoFrame *> m_frames;
    VideoFrame * m_frame{nullptr};
};

#endif // DEMOFRAMEGRABBER_H
