#include "modedialog.h"
#include "ui_modedialog.h"
#include <QDebug>

ModeDialog::ModeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ModeDialog)
{
    ui->setupUi(this);
}

ModeDialog::~ModeDialog()
{
    delete ui;
}

QString ModeDialog::printDevProp(cudaDeviceProp devProp)
{
    QString result;
    result += QString("Major revision number:\t\t\t%1\n").arg(devProp.major);
    result += QString("Minor revision number:\t\t\t%1\n").arg(devProp.minor);
    result += QString("Name:\t\t%1\n").arg(QString(devProp.name));
    result += QString("Total global memory:\t\t%1\n").arg(devProp.totalGlobalMem);
    result += QString("Total shared memory per block:\t%1\n").arg(devProp.sharedMemPerBlock);
    result += QString("Total registers per block:\t%1\n").arg(devProp.regsPerBlock);
    result += QString("Warp size:\t\t\t%1\n").arg(devProp.warpSize);
    result += QString("Maximum memory pitch:\t%1\n").arg(devProp.memPitch);
    result += QString("Maximum threads per block:\t%1\n").arg(devProp.maxThreadsPerBlock);
    for (int i = 0; i < 3; ++i)
        result += QString("Maximum dimension %1 of block:\t%2\n").arg(i).arg(devProp.maxThreadsDim[i]);
    for (int i = 0; i < 3; ++i)
        result += QString("Maximum dimension %1 of grid:\t%2\n").arg(i).arg(devProp.maxGridSize[i]);
    result += QString("Clock rate:\t\t\t%1\n").arg(devProp.clockRate);
    result += QString("Total constant memory:\t%1\n").arg(devProp.totalConstMem);
    result += QString("Texture alignment:\t\t%1\n").arg(devProp.textureAlignment);
    result += QString("Concurrent copy and execution:\t%1\n").arg((devProp.deviceOverlap ? "Yes" : "No"));
    result += QString("Number of multiprocessors:\t%1\n").arg(devProp.multiProcessorCount);
    result += QString("Kernel execution timeout:\t%1\n").arg((devProp.kernelExecTimeoutEnabled ?"Yes" : "No"));
    return result;
}



int ModeDialog::exec()
{
    // load cuda devices

    cudaError_t error;

    int count;
    if((error = cudaGetDeviceCount(&count)) != cudaSuccess) {
        qDebug() << "Cuda error: " << cudaGetErrorString(error);
    }

    cudaDeviceProp devProp;

    QString info;

    for (int var = 0; var < count; ++var)
    {
        if((error = cudaGetDeviceProperties(&devProp,var)) != cudaSuccess) {
            qDebug() << "Cuda error: " << cudaGetErrorString(error);
        }
        else {
            info += "////////////////////\n";
            info += "Info for device: " + QString::number(var) + "\n";
            info += printDevProp(devProp) + "\n";
        }

    }

    ui->imageDebayerBtn->setEnabled(count != 0);

    ui->spinBox->setMaximum(count - 1);
    ui->textEdit->setText(info);



    return QDialog::exec();
}

void ModeDialog::on_imageDebayerBtn_clicked()
{
    this->accept();
}

int ModeDialog::getCudeDeviceNumber()
{
    return ui->spinBox->value();
}

QSize ModeDialog::frameResolution()
{
    return QSize(ui->widthSpinBox->value(),ui->heightSpinBox->value());
}

void ModeDialog::on_dataDebayerBtn_clicked()
{
    this->reject();
}
