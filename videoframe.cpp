#include "videoframe.h"


VideoFrame::VideoFrame(int frameID, int width, int height, int bytesPerLine, QObject *parent) : QObject(parent), m_frameID(frameID),m_width(width),m_height(height),m_bytesPerLine(bytesPerLine)
{
    m_data = malloc(height*bytesPerLine);
}

VideoFrame::VideoFrame(int frameID, int width, int height, int bytesPerLine, void *data, QObject *parent) : QObject(parent), m_frameID(frameID),m_width(width),m_height(height),m_bytesPerLine(bytesPerLine), m_data(data)
{

}

VideoFrame::~VideoFrame()
{
    free(m_data);
}

int VideoFrame::frameID() const
{
    return m_frameID;
}

void VideoFrame::setFrameID(int frameID)
{
    m_frameID = frameID;
}
uint VideoFrame::width() const
{
    return m_width;
}

void VideoFrame::setWidth(uint width)
{
    m_width = width;
}
uint VideoFrame::height() const
{
    return m_height;
}

void VideoFrame::setHeight(uint height)
{
    m_height = height;
}
int VideoFrame::bytesPerLine() const
{
    return m_bytesPerLine;
}

void VideoFrame::setBytesPerLine(int bytesPerLine)
{
    m_bytesPerLine = bytesPerLine;
}
void *VideoFrame::data() const
{
    return m_data;
}

void VideoFrame::setData(void *data)
{
    m_data = data;
}

int VideoFrame::type() const
{
    return m_type;
}

void VideoFrame::setType(int type)
{
    m_type = type;
}
