#ifndef CONVERTERTODEBAYER_H
#define CONVERTERTODEBAYER_H

#include <QObject>
#include <cuda_runtime.h>

class ConverterToBayer : public QObject
{
    Q_OBJECT
public:
    explicit ConverterToBayer(int width, int height, QObject *parent = 0);

    ~ConverterToBayer();
signals:

public slots:
    void convertFrameToGRBG(unsigned char *input, unsigned char *output);
    void init();

private:
    int m_width;
    int m_height;
    unsigned char * m_device_outputData;
    cudaArray *     m_inputTexArray;
    cudaStream_t m_stream;
};

#endif // CONVERTERTODEBAYER_H
