#include "glwidget.h"
#include <QDebug>

GLWidget::GLWidget(QWidget *parent) : QOpenGLWidget(parent)
{

}

GLWidget::~GLWidget()
{
    glDeleteTextures(1,&texid);
}

void GLWidget::paintGL()
{
    glViewport(0, 0, this->size().width(), this->size().height()); // use a screen size of WIDTH x HEIGHT
    glEnable(GL_TEXTURE_2D);     // Enable 2D texturing

    glMatrixMode(GL_PROJECTION);     // Make a simple 2D projection on the entire window
    glLoadIdentity();
    glOrtho(0.0, this->size().width(), this->size().height(), 0.0, 0.0, 100.0);

    glMatrixMode(GL_MODELVIEW);    // Set the matrix mode to object modeling


    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texid);
    /* Draw a quad */
    glBegin(GL_QUADS);
    glTexCoord2i(0, 0); glVertex2i(0,   0);
    glTexCoord2i(0, 1); glVertex2i(0,  this->size().height());
    glTexCoord2i(1, 1); glVertex2i(this->size().width(), this->size().height());
    glTexCoord2i(1, 0); glVertex2i(this->size().width(), 0);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);
}

void GLWidget::resizeGL()
{
    glViewport(0, 0, this->size().width(), this->size().height());
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    glOrtho(0.0, this->size().width(), this->size().height(), 0.0, 0.0, 100.0);
    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::initializeGL()
{
    glViewport(0, 0, this->size().width(), this->size().height()); // use a screen size of WIDTH x HEIGHT
    glEnable(GL_TEXTURE_2D);     // Enable 2D texturing

    glMatrixMode(GL_PROJECTION);     // Make a simple 2D projection on the entire window
    glLoadIdentity();
    glOrtho(0.0, this->size().width(), this->size().height(), 0.0, 0.0, 100.0);

    glMatrixMode(GL_MODELVIEW);    // Set the matrix mode to object modeling

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear the window


    glGenTextures(1, &texid); /* Texture name generation */
    glBindTexture(GL_TEXTURE_2D, texid); /* Binding of texture name */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void GLWidget::newFrame(VideoFrame *newFrame)
{

    glBindTexture(GL_TEXTURE_2D, texid); /* Binding of texture name */

//    QImage image((uchar*)newFrame->data(),newFrame->width(),newFrame->height(),QImage::Format_RGB888);
//    image.save("test-png" + QString::number(counter++) + ".png");

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGB,
                 newFrame->width(),
                 newFrame->height(),
                 0,
                 GL_RGB,
                 GL_UNSIGNED_BYTE,
                 newFrame->data());

    glBindTexture(GL_TEXTURE_2D, 0);

    update();

    delete newFrame;
}

