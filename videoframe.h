#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include <QObject>

//////
/// \brief The VideoFrame class
/// contains frame data and its metadata
class VideoFrame : public QObject
{
    Q_OBJECT

public:
    ////
    /// \brief The Type enum
    /// is used to identify different CFA formats, NON_DEBAYER_TYPE means that data is not in CFA format - for example RGBA, RGB
    enum Type{
        NON_DEBAYER_TYPE,
        GRBG,
        RGGB,
        BGGR,
        GBRG
    };
    /////
    /// \brief VideoFrame
    /// \param frameID
    /// \param width
    /// \param height
    /// \param bytesPerLine
    /// \param parent
    /// this constructor allocates memory for given parameters
    VideoFrame(int frameID,int width, int height,int bytesPerLine,QObject *parent = 0);

    ////
    /// \brief VideoFrame
    /// \param frameID
    /// \param width
    /// \param height
    /// \param bytesPerLine
    /// \param data
    /// \param parent
    /// this contructor taks ownership of given data, no allocation of new memory or memcpy
    VideoFrame(int frameID,int width, int height,int bytesPerLine, void * data,QObject *parent = 0);
    ~VideoFrame();

    //// list of setter and getters
    int frameID() const;
    void setFrameID(int frameID);

    uint width() const;
    void setWidth(uint width);

    uint height() const;
    void setHeight(uint height);

    int bytesPerLine() const;
    void setBytesPerLine(int bytesPerLine);

    void *data() const;
    void setData(void *data);

    int type() const;
    void setType(int type);

private:
    int m_type {NON_DEBAYER_TYPE};
    int m_frameID;
    uint m_width;
    uint m_height;
    int m_bytesPerLine;
    void *m_data = {NULL};


};

#endif // VIDEOFRAME_H
