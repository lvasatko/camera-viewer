#include "viewerwindow.h"
#include "ui_viewerwindow.h"

ViewerWindow::ViewerWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::ViewerWindow)
{
    ui->setupUi(this);
}

bool ViewerWindow::parseArguments(int argc, char *argv[])
{
    // if no argument is given - start GUI
    if(argc == 1) {
        return false;
    }

    for (int index = 0; index < argc; ++index) {
        QString argument (argv[index]);
        if(argument == "-h" || argument == "--help") {
            printHelp();
        }
    }


    return true;
}

void ViewerWindow::createWidgets()
{

}

void ViewerWindow::showDialog()
{

    ModeDialog dialog(this);

    if(dialog.exec() == QDialog::Rejected) {
        QTimer::singleShot(100,this,SLOT(close()));
        return;
    }

    cameraWidget = new CameraDebayerWidget(dialog.frameResolution(),this);
    ui->stackedWidget->addWidget(cameraWidget);
    ui->stackedWidget->setCurrentWidget(cameraWidget);
    cameraWidget->setCudaDevice(dialog.getCudeDeviceNumber());

}

ViewerWindow::~ViewerWindow()
{
    delete ui;
}

void ViewerWindow::printHelp()
{

}


