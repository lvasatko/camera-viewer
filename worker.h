#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QDebug>
#include <QMutex>
#include <QThread>

#include "include/Methods/debayermethod.h"

class VideoFrame;
class Debayer;

////
/// \brief The Worker class
/// controls demosaicing in private thread
class Worker : public QObject
{
    Q_OBJECT
public:
    /////
    /// \brief Worker
    /// \param debayerManager pointer to Debayer object for getting new VideoFrames
    /// \param method pointer to method object
    /// \param cudaDevice cuda device number
    ///
    explicit Worker(Debayer * debayerManager, DebayerMethod *method,  int cudaDevice = 0);
    ~Worker();

    /////
    /// \brief processing
    /// \return true if worker is processing VideoFrame
    ///
    bool processing();
    /////
    /// \brief setProcessing
    /// \param processing new value of processing flag
    ///
    void setProcessing(bool processing);

    /////
    /// \brief stop
    /// function stops processing after current VideoFrame processing finishes
    void stop();

    /////
    /// \brief stopWorking
    /// \return true if worker has to stop processing
    ///
    bool stopWorking();

public slots:
    ///
    /// \brief processFrame
    /// \param frame for demosaicing process
    ///
    void processFrame(VideoFrame * frame);

    ///
    /// \brief init
    /// \param width of frame in pixels
    /// \param height of frame in pixels
    /// init function which calls init method of give DebayerMethod object
    void init(uint width, uint height);

signals:
    /////
    /// \brief storeProcessedFrame
    /// \param frame which has been processed
    ///
    void storeProcessedFrame(VideoFrame * frame);

private:

    Debayer * m_debayerManager;
    int m_cudaDevice;

    bool m_initialized{false};

    QMutex m_processingMutex;
    bool m_processing {true};

    QMutex m_stopMutex;
    bool m_stopping{false};

    DebayerMethod * m_method {nullptr};

    QThread * m_workerThread {nullptr};
};

#endif // WORKER_H
