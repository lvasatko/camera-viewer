#include "demoframegrabber.h"
#include <QImage>

#include <QThread>
#include <QDir>
#include <QFileInfo>
#include <QtMath>
#include <iostream>

#include <QDebug>

extern "C"
void cuda_detectEdges(unsigned char * inputData,int width, int height);


DemoFrameGrabber::DemoFrameGrabber(QObject *parent) : QObject(parent)
{

}

DemoFrameGrabber::~DemoFrameGrabber()
{
    if(m_frame != nullptr)
        delete m_frame;

    if(m_converter != nullptr)
        delete m_converter;
}


void DemoFrameGrabber::loadMJPEG(QString mjpegFilename, QString indexFilename, int startFrame, int frameCount, QSize frameResolution)
{
    qDebug() << mjpegFilename << " " << indexFilename;

    QFile videoFile(mjpegFilename);
    QFile indexFile(indexFilename);

    if(!videoFile.open(QIODevice::ReadOnly))
    {
        std::cerr << "VideoLoaderThread: Could not open file " << videoFile.fileName().toStdString() << " for reading.\t\t[FAIL]" << std::endl;
        return;
    }
    qint64 videoFileSize = videoFile.size();

    if(!indexFile.open(QIODevice::ReadOnly)) {
        std::cerr << "VideoLoaderThread: Could not open file " << indexFile.fileName().toStdString() << " for reading.\t\t[FAIL]" << std::endl;
        return;
    }
    qint64 indexFileSize = indexFile.size();


    qint64 pos = 0;

    if(pos > indexFileSize / (qint64)sizeof(quint32))
        pos = qFloor((double)indexFileSize / (double)sizeof(quint32));

    int count = indexFileSize / (qint64)sizeof(quint32);

    if(startFrame < pos || startFrame > count)
        startFrame = 0;
    else
        pos = startFrame;

    if(startFrame + frameCount >= count) {
        frameCount = count - startFrame - 1;
    }


    m_converter = new ConverterToBayer(frameResolution.width(), frameResolution.height());
    m_converter->init();


    for(int i = 1; i <= frameCount; i++)
    {

        quint32 frameOffset = 0;
        quint32 nextOffset = 0;
        size_t frameSize = 0;
        if(!indexFile.seek(pos * sizeof(quint32)))
            break;
        if(indexFile.read((char *)&frameOffset, sizeof(quint32)) != sizeof(quint32))
            break;

        if((pos + 1) * (int)sizeof(quint32) >= indexFileSize)
            frameSize = videoFileSize - frameOffset;
        else
        {
            if(!indexFile.seek((pos + 1) * sizeof(quint32)))
                break;
            if(indexFile.read((char *)&nextOffset, sizeof(quint32)) != sizeof(quint32))
                break;
            frameSize = nextOffset - frameOffset;
        }

        if(!videoFile.seek(frameOffset))
            break;
        void *frameData = malloc(frameSize);
        if(frameData == NULL)
        {
            std::cerr << "[FAIL]\tFree memory is too low, cannot load more frames." << std::endl;
            break;
        }

        if(videoFile.read((char *)frameData, frameSize) != (qint64)frameSize)
        {
            std::cerr << "[FAIL]\tCannot read whole frame. Video loading aborted." << std::endl;
            break;
        }

        QImage image(frameResolution.width(), frameResolution.height(),QImage::Format_RGBX8888);
        image.loadFromData((const uchar *)frameData,frameSize,"JPG");
        free(frameData);


        unsigned char * outputData = (unsigned char *)malloc(frameResolution.width()*frameResolution.height());

        // GPU
        m_converter->convertFrameToGRBG(image.bits(),outputData);

        VideoFrame * newFrame = new VideoFrame(0,frameResolution.width(),frameResolution.height(),frameResolution.width());
        memcpy(newFrame->data(),outputData,frameResolution.width()*frameResolution.height());
        free(outputData);


        m_frames.append(newFrame);

        pos += 1;
    }
}

void DemoFrameGrabber::init()
{
    m_fpsTimer = new QTimer();
    connect(m_fpsTimer,SIGNAL(timeout()),this,SLOT(sendFrame()));
}

void DemoFrameGrabber::destroy()
{
    if(m_fpsTimer !=nullptr) {
        m_fpsTimer->stop();
        delete m_fpsTimer;
    }
}

void DemoFrameGrabber::loadImage(QString pathToImageFile)
{
    if(m_frame != nullptr) {
        delete m_frame;
        m_frame = nullptr;
    }

    QImage m_currentImage(pathToImageFile);

    if(m_currentImage.isNull())
        return;

    cuda_detectEdges(m_currentImage.bits(),m_currentImage.height(),m_currentImage.width());

    m_frame = new VideoFrame(0,m_currentImage.width(),m_currentImage.height(),m_currentImage.width());


    m_converter = new ConverterToBayer(m_currentImage.width(), m_currentImage.height());
    m_converter->init();

    // GPU
    m_converter->convertFrameToGRBG(m_currentImage.bits(),(unsigned char *)m_frame->data());

    if(m_frame == nullptr)
        return;

    for (int var = 0; var < m_frameCount; ++var) {
        VideoFrame * newFrame = new VideoFrame(m_frame->frameID(),m_frame->width(),m_frame->height(),m_frame->bytesPerLine());
        memcpy(newFrame->data(),m_frame->data(),m_frame->height()*m_frame->bytesPerLine());

        m_frames.append(newFrame);
    }
}


void DemoFrameGrabber::start()
{
    if(m_fps == 0) {
        for (VideoFrame * frame : m_frames) {
            emit frameToDebayer(frame);
        }
    }
    else {
        m_fpsTimer->start();
    }
}


void DemoFrameGrabber::sendFrame()
{
    if(m_currentFrameIndex >= m_frames.size()) {
        m_fpsTimer->stop();
        return;
    }

    emit frameToDebayer(m_frames[m_currentFrameIndex++]);

}

int DemoFrameGrabber::fps() const
{
    return m_fps;
}

void DemoFrameGrabber::setFps(int fps)
{
    m_fps = fps;
    if(m_fpsTimer != nullptr && fps > 0)
        m_fpsTimer->setInterval(1000/fps);
    else if(m_fpsTimer != nullptr && fps == 0)
        m_fpsTimer->stop();
}

int DemoFrameGrabber::frameCount() const
{
    return m_frameCount;
}

void DemoFrameGrabber::setFrameCount(int frameCount)
{
    m_frameCount = frameCount;
}




